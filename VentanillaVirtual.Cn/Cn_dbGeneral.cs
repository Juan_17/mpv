﻿//using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Configuration;

namespace VentanillaVirtual.Cn
{
    public class Cn_dbGeneral
    {
        //OracleConnection CnOracle;
        SqlConnection CnSql;

        //public OracleConnection Cn_Oracle()
        //{
        //    CnOracle = new OracleConnection();
        //    string lstCn = ConfigurationManager.ConnectionStrings["CnVVIRTUAL"].ConnectionString;
        //    CnOracle.ConnectionString = lstCn;
        //    return CnOracle;
        //}

        public SqlConnection Cn_SqlServer()
        {
            CnSql = new SqlConnection();
            string lstCn = ConfigurationManager.ConnectionStrings["CnVVIRTUAL"].ConnectionString;
            CnSql.ConnectionString = lstCn;
            return CnSql;
        }

        public SqlConnection Cn_SqlServer_SID()
        {
            CnSql = new SqlConnection();
            string lstCn = ConfigurationManager.ConnectionStrings["CnTRAMITE"].ConnectionString;
            CnSql.ConnectionString = lstCn;
            return CnSql;
        }

    }
}
