﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class M_MAES_DIRE_Be
    {
        public int vDIRE_P_inCODDIR { get; set; }
        public int vPERS_P_inCODPER { get; set; }
        public int vUBIG_P_inCODUBI { get; set; }
        public string vDIRE_chDESDIR { get; set; }
        public int vDIRE_btVIGDIR { get; set; }
        public string vSEGU_P_chCODUSU { get; set; }
        public string vSEGU_chFECINI { get; set; }
        public string vSEGU_chFECMOD { get; set; }
        public int vSEGU_btFLAELI { get; set; }
        public string vCALLE { get; set; }
        public string vVIA { get; set; }
        public string vDIRE_chNRO { get; set; }
        public string vDIRE_chINT { get; set; }
        public string vDIRE_chMAN { get; set; }
        public string vDIRE_chLOTE { get; set; }
        public string vDIRE_chURB { get; set; }
        public string vDIRE_chREFE { get; set; }
        public int vSEGU_inUSUCRE { get; set; }
        public int vSEGU_inUSUMOD { get; set; }
        public string vSEGU_chMODFEC { get; set; }
        public string vSEGU_chCREFEC { get; set; }
        public string vSEGU_chUSUPC { get; set; }
        public string vSEGU_chUSUMAC { get; set; }
        public string vSEGU_chUSUIP { get; set; }
    }
}
