﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class TUPA_Be
    {

        public int ASUN_P_inCODASU { get; set; }
        public string ASUN_chTUPAPART { get; set; }
        public string BUS_ASUN_chDESASU { get; set; }
        public int ASUN_inTIEDIA { get; set; }
        public decimal ASUN_reTUPADERPAG { get; set; }
        public string CALI_chNOMCOR { get; set; }
        public int OFIC_P_inCODOFI { get; set; }
        public string OFIC_chDESOFISEC { get; set; }
        public string ASUN_chTUPASUBP { get; set; }

    }
}
