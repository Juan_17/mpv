﻿using System;


namespace VentanillaVirtual.Entidad
{
    public class ARCHIVOADJ_Be
    {

        public int CODARCHIVO { get; set; }
        public string DESCARCHIVO { get; set; }
        public int IDSOLICITUD { get; set; }
        public string TXTURLARCHIVO { get; set; }
        public DateTime? FECREG { get; set; }
        public string TXTIP { get; set; }
        public string ESTADO { get; set; }


        public string TIPOPROCESO { get; set; }
        public int CORRELA { get; set; }


    }
}
