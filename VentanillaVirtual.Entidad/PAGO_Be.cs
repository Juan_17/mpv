﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class PAGO_Be
    {
        public int IDSOLICITUD { get; set; }
        public string FPAGO { get; set; }
        public string FEPAGO { get; set; }
        public int ID_CUENTA { get; set; }
        public decimal MONTO { get; set; }
 
    }
}
