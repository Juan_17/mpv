﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class RESPUESTAOBS_Be
    {

        public int IDRESPUESTAOBS { get; set; }
        public int IDOBSERVACION { get; set; }
        public int IDSOLICITUD { get; set; }
        public string TXTRESPUESTA { get; set; }
        public string TXTURLARCHIVO { get; set; }
        public string FECREG { get; set; }
        public string TXTIP { get; set; }
        public string ESTADO { get; set; }


        public string TIPOPROCESO { get; set; }
        public int CORRELA { get; set; }
        public int ID_CONDICION { get; set; }

    }
}
