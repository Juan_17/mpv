﻿
namespace VentanillaVirtual.Entidad
{
    public class UBIGEO_Be
    {

        public string proCODUBIGEO { get; set; }
        public string proTXTDEPARTAMENTO { get; set; }
        public string proTXTPROVINCIA { get; set; }
        public string proTXTDISTRITO { get; set; }
        public string proCODUSUARIOREG { get; set; }
        public string proFECREG { get; set; }
        public string proCODUSUARIOMOD { get; set; }
        public string proFECMOD { get; set; }


    }
}
