﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class M_MAES_DOCU_Be
    {
        public int DOCU_P_inCODDOC { get; set; }
        public int OFIC_P_inCODOFI { get; set; }
        public int TIPO_P_inCODTIP { get; set; }
        public string DOCU_chANODOC { get; set; }
        public string DOCU_chNRODOC { get; set; }
        public string DOCU_chFECDOC { get; set; }
        public int DOCU_inNROFOL { get; set; }
        public int ASUN_P_inCODASU { get; set; }
        public int PERS_P_inCODPER { get; set; }
        public int DIRE_P_inCODDIR { get; set; }
        public string DOCU_chCODCON { get; set; }
        public string DOCU_chOBSDOC { get; set; }
        public int ESTA_P_inCODEST { get; set; }
        public string SEGU_P_chCODUSU { get; set; }
        public string SEGU_chFECINI { get; set; }
        public string SEGU_chHORA { get; set; }
        public string SEGU_chFECMOD { get; set; }
        public int SEGU_btFLAELI { get; set; }
        public string MED_chCOD { get; set; }
        public int SEGU_inCREUSU { get; set; }
        public int SEGU_inMODUSU { get; set; }
        public string SEGU_chCREFEC { get; set; }
        public string SEGU_chMODFEC { get; set; }

        public string BAND_chCODEST { get; set; }
        public string DOCU_chReg { get; set; }
        public string DOCU_chSIGJER { get; set; }
        public string DOCU_chSIGOFI { get; set; }
        public string DOCU_chDESASU { get; set; }

    }
}
