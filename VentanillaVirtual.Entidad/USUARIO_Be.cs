﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class USUARIO_Be
    {

        public string IDUSUARIO { get; set; }
        public string TIPDOCIDE { get; set; }
        public string NRODOCIDE { get; set; }
        public string RAZONSOCIAL { get; set; }
        public string EMAIL { get; set; }
        public string TIPOACCESO { get; set; }
        public string FOTOUSUARIO { get; set; }
        public string USUARIO { get; set; }
        public string CLAVE { get; set; }
        public string ESTADO { get; set; }
        public string IDOFICINA { get; set; }
        public string OFICINA { get; set; }
        public string PERS_P_inCODPER { get; set; }
        public string DIRE_P_inCODDIR { get; set; }
        

    }
}
