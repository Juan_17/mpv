﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class OBSERVACION_Be
    {
        public int IDOBSERVACION { get; set; }
        public string TXTOBSERVACION { get; set; }
        public int IDSOLICITUD { get; set; }
        public string FECREG { get; set; }
        public string TXTIP { get; set; }
        public string ESTADO { get; set; }



        public string TIPOPROCESO { get; set; }
        public int CORRELA { get; set; }
        public int ID_CONDICION { get; set; }
        
    }
}
