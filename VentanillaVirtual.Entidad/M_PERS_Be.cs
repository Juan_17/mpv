﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class M_PERS_Be
    {
        public int vPERS_P_inCODPER { get; set; }
        public string vVARI_P_chCODTIP_TIPPER { get; set; }
        public string vVARI_P_chCODCAR_TIPPER { get; set; }
        public string vPERS_chNOMPER { get; set; }
        public string vPERS_chAPEPAT { get; set; }
        public string vPERS_chAPEMAT { get; set; }
        public string vPERS_chRAZSOC { get; set; }
        public string vVARI_P_chCODTIP_TIPDOC { get; set; }
        public string vVARI_P_chCODCAR_TIPDOC { get; set; }
        public string vPERS_chNRODOC { get; set; }
        public string vPERS_chTELPER { get; set; }
        public string vPERS_chCELPER { get; set; }
        public string vPERS_chMEIPER { get; set; }
        public int vSEGU_btFLAELI { get; set; }
        public string vPERS_chCONTRIB { get; set; }
        public string vPERS_chCONTRIB_COD { get; set; }
        public string vPERS_chNOMCOMP { get; set; }
        public string vPERS_chFECNAC { get; set; }
        public string vPERS_chSEX { get; set; }
        public string vSEGU_chUSUPC { get; set; }
        public string vSEGU_chUSUMAC { get; set; }
        public string vSEGU_chUSUIP { get; set; }
        public int vSEGU_inUSUCRE { get; set; }
        public int vSEGU_inUSUMOD { get; set; }
        public string vSEGU_chCREUSU { get; set; }
        public string vSEGU_chMODUSU { get; set; }
        public string vPERS_CIVIL { get; set; }
        public string vPERS_OCUPACION { get; set; }
        public string vPERS_NACIONALIDAD { get; set; }
        public string vPERS_LUGNAC { get; set; }
    }
}
