﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class DIRECCION_Be
    {
        public int DIRE_P_inID { get; set; }
        public string PAD_P_inIDPER { get; set; }
        public int VIA1_P_inID { get; set; }
        public string UBI1_chDESC { get; set; }
        public int VIA2_P_inID { get; set; }
        public string UBI2_chDESC { get; set; }
        public int VIA3_P_inID { get; set; }
        public string UBI3_chDESC { get; set; }
        public int VIA4_P_inID { get; set; }
        public string UBI4_chDESC { get; set; }
        public int VIA5_P_inID { get; set; }
        public string UBI5_chDESC { get; set; }
        public int VIA6_P_inID { get; set; }
        public string UBI6_chDESC { get; set; }
        public int UBIG_P_inID { get; set; }
        public int SEGU_inFLAELI { get; set; }
        public int SEGU_inUSUCRE { get; set; }
        public string SEGU_chFECCRE { get; set; }
        public int SEGU_inUSUMOD { get; set; }
        public string SEGU_chFECMOD { get; set; }
        public string CALLE_P_chID { get; set; }
        public string DIRE_chDESDIR { get; set; }
        public int DIRE_P_inCODDIR { get; set; }
        public int PERS_P_inCODPER { get; set; }

    }
}
