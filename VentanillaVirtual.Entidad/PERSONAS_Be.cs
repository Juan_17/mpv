﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class PERSONAS_Be
    {
        public string PAD_P_chIDPER { get; set; }
        public int TIPPER_P_inIDPER { get; set; }
        public int TIPDOC_P_inIDDOC { get; set; }
        public string PAD_chNRODOC { get; set; }
        public string PAD_chNOM { get; set; }
        public string PAD_chAPEPAT { get; set; }
        public string PAD_chAPEMAT { get; set; }
        public string PAD_chRAZSOC { get; set; }
        public string PAD_chNOMCOM { get; set; }
        public string PAD_chFECNAC { get; set; }
        public string PAD_chSEXO { get; set; }
        public string PAD_chTELFIJ1 { get; set; }
        public string PAD_chTELFIJ2 { get; set; }
        public string PAD_chNROCEL1 { get; set; }
        public string PAD_chNROCEL2 { get; set; }
        public string PAD_chMAIL { get; set; }
        public int PAD_inFLAELI { get; set; }
        public string PAD_chFECCRE { get; set; }
        public string PAD_chFECMOD { get; set; }
        public string PAD_chAPLFUE { get; set; }
        public string vNroDoc { get; set; }
        public int IDTipoPersona { get; set; }
        public int IDTipoDocumento { get; set; }
        public int PERS_P_inCODPER { get; set; }
        public string PERS_chNRODOC { get; set; }
        public int SEGU_btFLAELI { get; set; }

    }
}
