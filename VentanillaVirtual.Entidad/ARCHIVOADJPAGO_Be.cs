﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VentanillaVirtual.Entidad
{
    public class ARCHIVOADJPAGO_Be
    {
        public int CODARCHIVO { get; set; }
        public string DESCARCHIVO { get; set; }
        public int IDSOLICITUD { get; set; }
        public string TXTURLARCHIVO { get; set; }
        public DateTime? FECREG { get; set; }
        public string TXTIP { get; set; }
        public string ESTADO { get; set; }


        public string TIPOPROCESO { get; set; }
        public int CORRELA { get; set; }
    }
}
