﻿

namespace VentanillaVirtual.Entidad
{
    public class SESION_Be
    {
        public int IDSESION { get; set; }
        public string TXTLOGIN { get; set; }
        public string TXTCLAVE { get; set; }
        public string IPLOCAL { get; set; }
        public string TXTESTADO { get; set; }
        public string FECREGISTRO { get; set; }
        public string IDACCESO { get; set; }

        public string TXTESTADOFINAL { get; set; }
        public string FECESTADOFINAL { get; set; }

    }
}
