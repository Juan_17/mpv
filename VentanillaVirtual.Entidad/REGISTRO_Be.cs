﻿
namespace VentanillaVirtual.Entidad
{
    public class REGISTRO_Be
    {

        public int IDREGISTRO { get; set; }
        public int IDTIPODOCIDE { get; set; }
        public string NRODOCIDE { get; set; }
        public string TXTAPEPAT { get; set; }
        public string TXTAPEMAT { get; set; }
        public string TXTNOMRAZONSOCIAL { get; set; }
        public string TXTFIJOFONO { get; set; }
        public string TXTCELULAR { get; set; }
        public string TXTCORREO { get; set; }
        public string TXTFECNAC { get; set; }
        public string CODDEPDIR { get; set; }
        public string CODPROVDIR { get; set; }
        public string CODDISTDIR { get; set; }
        public string CODVIADIR { get; set; }
        public string TXTVIA { get; set; }
        public string CODOP1DIR { get; set; }
        public string TXTOP1DIR { get; set; }
        public string CODOP2DIR { get; set; }
        public string TXTOP2DIR { get; set; }
        public string CODOP3DIR { get; set; }
        public string TXTOP3DIR { get; set; }
        public string DIRECCION { get; set; }
        public int PERS_P_inCODPER { get; set; }
        public int DIRE_P_inCODDIR { get; set; }
        public string TXTMENSAJE { get; set; }

    }
}
