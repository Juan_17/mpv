﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class TUPA_REQUISITO_Be
    {

        public int ASUN_P_inCODASU { get; set; }
        public string ASUN_chTUPAPART { get; set; }
        public string ASUN_chDESASU { get; set; }
        public string CALI_chNOMCOM { get; set; }
        public int ASUN_inTIEDIA { get; set; }
        public string OFIC_chDESOFI { get; set; }
        public string ASUN_chTUPACONC { get; set; }
        public string DPLA_chDESC { get; set; }
        public string DPLA_chDESCOR { get; set; }
        public decimal ASUN_reTUPADERPAG { get; set; }
        public string REQ_chDES { get; set; }
        public decimal REQ_inDERPAG { get; set; }


    }
}
