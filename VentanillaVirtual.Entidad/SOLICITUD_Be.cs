﻿using System;

namespace VentanillaVirtual.Entidad
{
    public class SOLICITUD_Be
    {

        public int IDSOLICITUD { get; set; }
        public int IDREGISTRO { get; set; }
        public string TXTASUNTO { get; set; }
        public string TXTOBSERVACION { get; set; }
        public string TXTFOLIOS { get; set; }
        public int IDTUPA { get; set; }
        public string FECREG { get; set; }
        public string ESTADO { get; set; }
        public string MENSAJE { get; set; }
        public string FECHA { get; set; }
        public int ID_CONDICION { get; set; }
        public string ASUN_chDESASU { get; set; }
        public string DESC_CONDICION { get; set; }
        public string ADMINISTRADO { get; set; }
        public string AREA { get; set; }
        public int FLAGPAGO { get; set; }
        public int TIPO_P_inCODTIP { get; set; }
        public int OPC { get; set; }
        public int PERS_P_inCODPER { get; set; }
        public int DIRE_P_inCODDIR { get; set; }
        public int OFIC_P_inCODOFI { get; set; }
        public int DOCU_P_inCODDOC { get; set; }


        public int IDTIPOSOLICITUD { get; set; }
        public int CORRELA { get; set; }
        public string NROSOLICITUD { get; set; }
        public string FIJO { get; set; }
        public string CORREO { get; set; }
        public string CELULAR { get; set; }
        public string NRODOCIDE { get; set; }

        public string FPAGO { get; set; }
        public string FEPAGO { get; set; }
        public int ID_CUENTA { get; set; }
        public decimal MONTO { get; set; }
        public string NROCTA { get; set; }
        public string DIRECCION { get; set; }

        public string NROEXPEDIENTE { get; set; }
        public string NRO_OBSERVACIONES { get; set; }
        public string NRO_RESPUESTAS { get; set; }
        public string FECHAEXPEDIENTE { get; set; }


        public string NRO_DOCADJ { get; set; }
        public string DOCADMINISTRADO { get; set; }


    }
}
