﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class M_MAES_DIRE_Br
    {
        public int Piud_M_MAES_DIRE(M_MAES_DIRE_Be M_MAES_DIREBe)
        {
            M_MAES_DIRE_Das M_MAES_DIREDas = new M_MAES_DIRE_Das();

            return M_MAES_DIREDas.Piud_Direccion(M_MAES_DIREBe);

        }
    }
}
