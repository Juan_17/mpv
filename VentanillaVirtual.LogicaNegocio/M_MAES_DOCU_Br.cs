﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class M_MAES_DOCU_Br
    {
        public string Piud_M_Maes_Docu(M_MAES_DOCU_Be M_MAES_DOCUBe)
        {
            M_MAES_DOCU_Dau M_MAES_DOCUDas = new M_MAES_DOCU_Dau();
            return M_MAES_DOCUDas.Piud_Documento(M_MAES_DOCUBe);

        }
    }
}
