﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;


namespace VentanillaVirtual.LogicaNegocio
{
    public class ARCHIVODJPAGO_Br
    {
        public List<ARCHIVOADJPAGO_Be> Ps_ARCHIVOADJ(string strIdSolicitud)

        {
            return new ARCHIVOADJPAGO_Dau().Ps_ARCHIVOADJ(strIdSolicitud);
        }

        public string Pid_ARCHIVOADJ(ARCHIVOADJPAGO_Be ARCHIVOADJBe)
        {
            return new ARCHIVOADJPAGO_Dau().Pid_ARCHIVOADJ(ARCHIVOADJBe);
        }

        public string Pid_ACTUALIZAPAGO(ARCHIVOADJPAGO_Be ARCHIVOADJBe)
        {
            return new ARCHIVOADJPAGO_Dau().Pid_ACTUALIZAESTPAGO(ARCHIVOADJBe);
        }

    }
}
