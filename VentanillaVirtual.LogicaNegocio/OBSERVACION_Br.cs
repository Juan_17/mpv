﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class OBSERVACION_Br
    {

        public List<OBSERVACION_Be> Ps_OBSERVACION(string strIdSolicitud)

        {
            return new OBSERVACION_Dau().Ps_OBSERVACION(strIdSolicitud);
        }

        public string Pid_OBSERVACION(OBSERVACION_Be OBSERVACIONBe)
        {
            return new OBSERVACION_Dau().Pid_OBSERVACION(OBSERVACIONBe);
        }


    }
}
