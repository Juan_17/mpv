﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class CUENTA_Br
    {
        public List<CUENTA_Be> listarNroCuenta(CUENTA_Be CUENTABe)
        {

            CUENTA_Das CUENTADas = new CUENTA_Das();
            return CUENTADas.fs_listarNROCUENTA(CUENTABe);

        }
    }
}
