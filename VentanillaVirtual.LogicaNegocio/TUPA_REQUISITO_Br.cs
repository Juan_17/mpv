﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class TUPA_REQUISITO_Br
    {

        public DataTable PS_LISTATUPA_REQUISITO(TUPA_REQUISITO_Be TUPA_REQUISITOBe)
        {

            return new TUPA_REQUISITO_Das().PS_LISTATUPA_REQUISITO(TUPA_REQUISITOBe);

        }

    }
}
