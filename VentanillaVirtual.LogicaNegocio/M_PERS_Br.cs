﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class M_PERS_Br
    {
        public int Piud_M_PERS(M_PERS_Be M_PERSBe)
        {
            M_PERS_Das M_PERSDas = new M_PERS_Das();
            return M_PERSDas.Piud_Personas(M_PERSBe);

        }
    }
}
