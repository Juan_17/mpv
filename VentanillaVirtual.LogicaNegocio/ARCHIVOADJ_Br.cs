﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class ARCHIVOADJ_Br
    {
        public List<ARCHIVOADJ_Be> Ps_ARCHIVOADJ(string strIdSolicitud)

        {
            return new ARCHIVOADJ_Dau().Ps_ARCHIVOADJ(strIdSolicitud);
        }

        public string Pid_ARCHIVOADJ(ARCHIVOADJ_Be ARCHIVOADJBe)
        {
            return new ARCHIVOADJ_Dau().Pid_ARCHIVOADJ(ARCHIVOADJBe);
        }

     

    }
}
