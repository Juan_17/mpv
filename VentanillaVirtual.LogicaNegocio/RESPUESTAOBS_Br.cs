﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class RESPUESTAOBS_Br
    {


        public List<RESPUESTAOBS_Be> Ps_RESPUESTAOBS(string strIdSolicitud, string strIdObservacion)

        {
            return new RESPUESTAOBS_Dau().Ps_RESPUESTAOBS(strIdSolicitud, strIdObservacion);
        }

        public string Pid_RESPUESTAOBS(RESPUESTAOBS_Be RESPUESTAOBSBe)
        {
            return new RESPUESTAOBS_Dau().Pid_RESPUESTAOBS(RESPUESTAOBSBe);
        }


    }
}
