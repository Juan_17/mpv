﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class DIRECCION_Br
    {
        public string Piud_Direccion(DIRECCION_Be DIRECCIONBe)
        {
            DIRECCION_Das DIRECCIONDas = new DIRECCION_Das();
            return DIRECCIONDas.Piud_Direccion(DIRECCIONBe);

        }
        public List<DIRECCION_Be> listarDireccion(DIRECCION_Be DIRECCIONBe)
        {

            DIRECCION_Das DIRECCIONDas = new DIRECCION_Das();
            return DIRECCIONDas.fs_listarDireccion(DIRECCIONBe);

        }
    }
}
