﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class SOLICITUD_Br
    {

        public string Pid_SOLICITUD(SOLICITUD_Be SOLICITUDBe)
        {
            try
            {

                return new SOLICITUD_Dau().Pid_SOLICITUD(SOLICITUDBe);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string Pu_SOLICITUD_SID(SOLICITUD_Be SOLICITUDBe)
        {
            try
            {

                return new SOLICITUD_Dau().Pu_SOLICITUD_SID(SOLICITUDBe);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<SOLICITUD_Be> Ps_SOLICITUD(SOLICITUD_Be SOLICITUDBe)
        {

            return new SOLICITUD_Dau().Ps_SOLICITUD(SOLICITUDBe);

        }

        public List<SOLICITUD_Be> Ps_SOLICITUD_TIPO_AREA(SOLICITUD_Be SOLICITUDBe)
        {

            return new SOLICITUD_Dau().Ps_SOLICITUD_TIPO_AREA(SOLICITUDBe);

        }

        public DataTable Ps_IMPCONSTANCIA(string strIDSOLICITUD)
        {

            return new SOLICITUD_Dau().Ps_IMPCONSTANCIA(strIDSOLICITUD);

        }



        //DOCUMENTO SIMPLES

        public List<SOLICITUD_Be> Ps_DOCUMENTOSIMPLE(string strTIPOSOLI,string strIDREGISTRO, string strTIPOBUSQ, string strBUSQ, string strORIGEN, string strArea)
        {
            return new SOLICITUD_Dau().Ps_DOCUMENTOSIMPLE(strTIPOSOLI, strIDREGISTRO, strTIPOBUSQ, strBUSQ, strORIGEN, strArea);
        }

        //FIN DOCUMENTO SIMPLE

        //SOLICTUD EXPEDIENTE

        public List<SOLICITUD_Be> Ps_SEXPEDIENTE(string strTIPOSOLI, string strIDREGISTRO, string strTIPOBUSQ, string strBUSQ, string strOrigen, string strArea)
        {
            return new SOLICITUD_Dau().Ps_SEXPEDIENTE(strTIPOSOLI, strIDREGISTRO, strTIPOBUSQ, strBUSQ, strOrigen, strArea);
        }

        //FIN SOLICTUD EXPEDIENTE

        public string PU_DATOSREGISTRO(int IDREGISTRO, int PERS_P_inCODPER, int DIRE_P_inCODDIR)
        {
            try
            {

                return new SOLICITUD_Dau().PU_DATOSREGISTRO(IDREGISTRO, PERS_P_inCODPER, DIRE_P_inCODDIR);

            }
            catch (Exception ex)
            {
                throw;
            }
        }



    }
}
