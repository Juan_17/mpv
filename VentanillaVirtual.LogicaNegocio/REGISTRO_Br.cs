﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;


namespace VentanillaVirtual.LogicaNegocio
{
    public class REGISTRO_Br
    {

        public int Pi_REGISTRO(REGISTRO_Be REGISTROBe)
        {
            try
            {
                return new REGISTRO_Dau().Pi_REGISTRO(REGISTROBe);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int PS_REGISTRO(REGISTRO_Be REGISTROBe)
        {
            try
            {
                return new REGISTRO_Dau().PS_REGISTRO(REGISTROBe);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<REGISTRO_Be> listarAdministrado(REGISTRO_Be REGISTROBe)
        {

            REGISTRO_Dau PERSONASDas = new REGISTRO_Dau();
            return PERSONASDas.fs_listarAdministrado(REGISTROBe);

        }

    }
}
