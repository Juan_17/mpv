﻿using System;
using System.Data;
using VentanillaVirtual.AccesoDatos;
using VentanillaVirtual.Entidad;
using System.Collections.Generic;

namespace VentanillaVirtual.LogicaNegocio
{
    public class PERSONAS_Br
    {
        public List<PERSONAS_Be> listarPersonas(PERSONAS_Be PERSONASBe)
        {

            PERSONAS_Das PERSONASDas = new PERSONAS_Das();
            return PERSONASDas.fs_listarPersonas(PERSONASBe);

        }

        public List<PERSONAS_Be> listarAdministrado(PERSONAS_Be PERSONASBe)
        {

            PERSONAS_Das PERSONASDas = new PERSONAS_Das();
            return PERSONASDas.fs_listarAdministrado(PERSONASBe);

        }

        public string Piud_Personas(PERSONAS_Be PERSONASBe)
        {
            PERSONAS_Das PERSONASDas = new PERSONAS_Das();
            return PERSONASDas.Piud_Personas(PERSONASBe);

        }

    }
}
