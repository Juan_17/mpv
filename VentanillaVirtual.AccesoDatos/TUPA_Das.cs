﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class TUPA_Das
    {

        SqlConnection cnx;
        public TUPA_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }


        public DataTable Ps_LISTATUPA(TUPA_Be vTUPABe)
        {
            DataTable dt;
            dt = new DataTable();

            List<TUPA_Be> listado = new List<TUPA_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.SP_TUPA", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("ASUN_chTUPASUBP ", DbType.String)).Value = vTUPABe.ASUN_chTUPASUBP;
                    com.Parameters.Add(new SqlParameter("BUS_ASUN_chDESASU", DbType.String)).Value = vTUPABe.BUS_ASUN_chDESASU;
                    com.Parameters.Add(new SqlParameter("OFIC_P_inCODOFI", DbType.Int32)).Value = vTUPABe.OFIC_P_inCODOFI;
                    cnx.Open();

                    SqlDataReader dr = com.ExecuteReader();
                    dt.Load(dr);
                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }

            }


        }




    }
}
