﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class ARCHIVOADJ_Dau
    {

        SqlConnection cnx;
        public ARCHIVOADJ_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public List<ARCHIVOADJ_Be> Ps_ARCHIVOADJ(string strIdSolicitud)
        {

            List<ARCHIVOADJ_Be> listado = new List<ARCHIVOADJ_Be>();

            using (SqlCommand cmd = new SqlCommand("vvirtual.dbo.PS_ARCHIVOADJ", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("IDSOLICITUD", DbType.Int32)).Value = strIdSolicitud;
                    cnx.Open();
                    SqlDataReader rs = cmd.ExecuteReader();
                    ARCHIVOADJ_Be ARCHIVOADJBe;

                    Int32 vCorrela = 0;

                    while (rs.Read())
                    {
                        vCorrela=vCorrela + 1;
                        ARCHIVOADJBe = new ARCHIVOADJ_Be();
                        ARCHIVOADJBe.CORRELA = vCorrela;
                        ARCHIVOADJBe.CODARCHIVO = Int32.Parse(rs["CODARCHIVO"].ToString());
                        ARCHIVOADJBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        ARCHIVOADJBe.DESCARCHIVO = rs["DESCARCHIVO"].ToString();
                        ARCHIVOADJBe.TXTURLARCHIVO = rs["TXTURLARCHIVO"].ToString();
                        listado.Add(ARCHIVOADJBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }

        public string Pid_ARCHIVOADJ(ARCHIVOADJ_Be ARCHIVOADJBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("vvirtual.dbo.PID_ARCHIVOADJ", cnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("iCODARCHIVO", DbType.Int32)).Value = ARCHIVOADJBe.CODARCHIVO;
                cmd.Parameters.Add(new SqlParameter("iDESCARCHIVO", DbType.String)).Value = ((ARCHIVOADJBe.DESCARCHIVO != null) ? ARCHIVOADJBe.DESCARCHIVO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.String)).Value = ARCHIVOADJBe.IDSOLICITUD;
                cmd.Parameters.Add(new SqlParameter("iTXTURLARCHIVO", DbType.String)).Value = ((ARCHIVOADJBe.TXTURLARCHIVO != null) ? ARCHIVOADJBe.TXTURLARCHIVO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTXTIP", DbType.String)).Value = ((ARCHIVOADJBe.TXTIP != null) ? ARCHIVOADJBe.TXTIP : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTIPOPROCESO", DbType.String)).Value = ((ARCHIVOADJBe.TIPOPROCESO != null) ? ARCHIVOADJBe.TIPOPROCESO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("oRESULT", DbType.String)).Direction = ParameterDirection.Output;

                cnx.Open();
                cmd.ExecuteNonQuery();
                string lsResultado = cmd.Parameters["oRESULT"].Value.ToString();
                cnx.Close();
                return lsResultado;

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


    }
}
