﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class USUARIO_Das
    {

        SqlConnection cnx;

        public USUARIO_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public USUARIO_Be Ps_VALIDARLOGIN(string strUSUARIO, string strCLAVE)
        {
            USUARIO_Be USUARIOBe = null;

            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_VALIDALOGIN", cnx))
            {

                try
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vUSUARIO", DbType.String)).Value = strUSUARIO;
                    com.Parameters.Add(new SqlParameter("vCLAVE", DbType.String)).Value = strCLAVE;
                    //com.Parameters.Add(new SqlParameter("vANIO", DbType.String)).Value = strANIO;
                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    while (rs.Read())
                    {

                        USUARIOBe = new USUARIO_Be();

                        USUARIOBe.IDUSUARIO = rs["IDUSUARIO"].ToString();
                        USUARIOBe.TIPDOCIDE = rs["TIPDOCIDE"].ToString();
                        USUARIOBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                        USUARIOBe.RAZONSOCIAL = rs["RAZONSOCIAL"].ToString();
                        USUARIOBe.EMAIL = rs["EMAIL"].ToString();
                        USUARIOBe.TIPOACCESO = rs["TIPOACCESO"].ToString();
                        USUARIOBe.FOTOUSUARIO = rs["FOTOUSUARIO"].ToString();
                        USUARIOBe.USUARIO = rs["USUARIO"].ToString();
                        USUARIOBe.CLAVE = rs["CLAVE"].ToString();
                        USUARIOBe.ESTADO = rs["ESTADO"].ToString();
                        USUARIOBe.IDOFICINA= rs["AREAASIG"].ToString();
                        USUARIOBe.OFICINA = rs["OFICINA"].ToString();
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    cnx.Close();
                    Console.WriteLine(e.ToString());
                }

            }


            return USUARIOBe;
        }


        //public USUARIO_Be Ps_USUARIO(string strIdContri)
        //{
        //    USUARIO_Be USUARIOBe = null;

        //    using (SqlCommand com = new SqlCommand("SATMUNXP_WEB.PWEB.PS_CONTRIBUYENTE", cnx))
        //    {

        //        try
        //        {
        //            com.CommandType = CommandType.StoredProcedure;
        //            com.CommandTimeout = 10 * 60;
        //            com.Parameters.Add(new SqlParameter("vIDCONTRIBUYENTE", DbType.String)).Value = strIdContri;
        //            //com.Parameters.Add("pcrPAGOWEB", OracleDbType.RefCursor).Direction = System.Data.ParameterDirection.Output;
        //            cnx.Open();
        //            SqlDataReader rs = com.ExecuteReader();

        //            while (rs.Read())
        //            {

        //                CONTRIBUYENTEBe = new CONTRIBUYENTE_Be();

        //                CONTRIBUYENTEBe.IDCONTRIBUYENTE = rs["IDCONTRIBUYENTE"].ToString();
        //                CONTRIBUYENTEBe.TIPDOCIDE = rs["TIPDOCIDE"].ToString();
        //                CONTRIBUYENTEBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
        //                CONTRIBUYENTEBe.RAZONSOCIAL = rs["RAZONSOCIAL"].ToString();
        //                CONTRIBUYENTEBe.TELEFONOFIJO = rs["TELEFONOFIJO"].ToString();
        //                CONTRIBUYENTEBe.TELEFONOMOVIL = rs["TELEFONOMOVIL"].ToString();
        //                CONTRIBUYENTEBe.EMAIL = rs["EMAIL"].ToString();
        //                CONTRIBUYENTEBe.TIPOCONTRI = rs["TIPOCONTRI"].ToString();
        //                CONTRIBUYENTEBe.CONDICIONCONTRI = rs["CONDICIONCONTRI"].ToString();
        //                CONTRIBUYENTEBe.PREDIOS = Int32.Parse(rs["PREDIOS"].ToString());
        //                CONTRIBUYENTEBe.DIREFISCAL = rs["DIREFISCAL"].ToString();
        //                CONTRIBUYENTEBe.TIPOBENEFICIO = rs["TIPOBENEFICIO"].ToString();
        //                CONTRIBUYENTEBe.FOTOCONTRIBUYENTE = rs["FOTOCONTRIBUYENTE"].ToString();
        //                //CONTRIBUYENTEBe.USUARIO = rs["USUARIO"].ToString();
        //                // CONTRIBUYENTEBe.CLAVE = rs["CLAVE"].ToString();
        //                CONTRIBUYENTEBe.ESTADO = rs["ESTADO"].ToString();
        //                CONTRIBUYENTEBe.FECREGISTRO = rs["FECREGISTRO"].ToString();
        //                CONTRIBUYENTEBe.TIPOCONTRIBUYENTE = rs["TIPOCONTRIBUYENTE"].ToString();
        //                CONTRIBUYENTEBe.ACTUALIZADO = rs["ACTUALIZADO"].ToString(); // 0 NO EXISTE
        //            }

        //            cnx.Close();
        //        }
        //        catch (Exception e)
        //        {
        //            cnx.Close();
        //            Console.WriteLine(e.ToString());
        //        }

        //    }


        //    return CONTRIBUYENTEBe;
        //}


    }
}
