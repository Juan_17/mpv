﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class SESION_Dau
    {

        SqlConnection cnx;

        public SESION_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }


        public int Pi_SESION(SESION_Be SESIONBe)
        {

            using (SqlCommand com = new SqlCommand("VVIRTUAL.DBO.PI_SESION", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vLOGIN", DbType.String)).Value = ((SESIONBe.TXTLOGIN != null) ? SESIONBe.TXTLOGIN : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vCLAVE", DbType.String)).Value = ((SESIONBe.TXTCLAVE != null) ? SESIONBe.TXTCLAVE : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vIPLOCAL", DbType.String)).Value = ((SESIONBe.IPLOCAL != null) ? SESIONBe.IPLOCAL : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vESTADO", DbType.String)).Value = ((SESIONBe.TXTESTADO != null) ? SESIONBe.TXTESTADO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vFECREGISTRO", DbType.String)).Value = ((SESIONBe.FECREGISTRO != null) ? SESIONBe.FECREGISTRO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vIDACCESO", DbType.String)).Value = ((SESIONBe.IDACCESO != null) ? SESIONBe.IDACCESO : (object)DBNull.Value);// SESIONBe.IDCONTRIBUYENTE;

                    com.Parameters.Add(new SqlParameter("vIDSESION", DbType.Int32));
                    com.Parameters["vIDSESION"].Direction = ParameterDirection.InputOutput;
                    com.Parameters["vIDSESION"].Value = 0;

                    cnx.Open();
                    com.ExecuteNonQuery();

                    //return Convert.ToInt32(com.Parameters["vIDSESION"].Value);
                    return int.Parse(com.Parameters["vIDSESION"].Value.ToString());

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }


        public int Pu_SESION(SESION_Be SESIONBe)
        {

            using (SqlCommand com = new SqlCommand("VVIRTUAL.DBO.PU_SESION", cnx))
            {

                try
                {

                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vIDSESION", DbType.String)).Value = ((SESIONBe.TXTLOGIN != null) ? SESIONBe.IDSESION : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("vESTADOFINAL", DbType.String)).Value = ((SESIONBe.TXTCLAVE != null) ? SESIONBe.TXTESTADOFINAL : (object)DBNull.Value);

                    //com.Parameters.Add(new OracleParameter("vIDSESION", OracleDbType.Int32));
                    //com.Parameters["vIDSESION"].Direction = ParameterDirection.InputOutput;
                    //com.Parameters["vIDSESION"].Value = 0;

                    cnx.Open();
                    com.ExecuteNonQuery();
                    return 0;
                    //return Convert.ToInt32(cmd.Parameters["vIDPAGO"].Value);
                    //return int.Parse(com.Parameters["vIDSESION"].Value.ToString());

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }


    }
}
