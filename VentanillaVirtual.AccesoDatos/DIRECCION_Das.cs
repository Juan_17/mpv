﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;


namespace VentanillaVirtual.AccesoDatos
{
    public class DIRECCION_Das
    {
     
        SqlConnection cnx;
        public DIRECCION_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer_SID();
        }

        public string Piud_Direccion(DIRECCION_Be vDIRECCIONBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_i_WEB_T_DIRE_PADR_Registrar", cnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("IDVia1", DbType.Int32)).Value = vDIRECCIONBe.VIA1_P_inID;
                cmd.Parameters.Add(new SqlParameter("IDVia2", DbType.Int32)).Value = vDIRECCIONBe.VIA2_P_inID;
                cmd.Parameters.Add(new SqlParameter("IDVia3", DbType.Int32)).Value = vDIRECCIONBe.VIA3_P_inID;
                cmd.Parameters.Add(new SqlParameter("IDVia4", DbType.Int32)).Value = vDIRECCIONBe.VIA4_P_inID;
                cmd.Parameters.Add(new SqlParameter("DescVia1", DbType.String)).Value = ((vDIRECCIONBe.UBI1_chDESC != null) ? vDIRECCIONBe.UBI1_chDESC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("DescVia2", DbType.String)).Value = ((vDIRECCIONBe.UBI2_chDESC != null) ? vDIRECCIONBe.UBI2_chDESC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("DescVia3", DbType.String)).Value = ((vDIRECCIONBe.UBI3_chDESC != null) ? vDIRECCIONBe.UBI3_chDESC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("DescVia4", DbType.String)).Value = ((vDIRECCIONBe.UBI4_chDESC != null) ? vDIRECCIONBe.UBI4_chDESC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("IDDistrito", DbType.Int32)).Value = vDIRECCIONBe.UBIG_P_inID;
                cmd.Parameters.Add(new SqlParameter("IDPersona", DbType.String)).Value = ((vDIRECCIONBe.PAD_P_inIDPER != null) ? vDIRECCIONBe.PAD_P_inIDPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Eliminado", DbType.Int32)).Value = vDIRECCIONBe.SEGU_inFLAELI;
                cmd.Parameters.Add(new SqlParameter("FechaCreacion", DbType.String)).Value = ((vDIRECCIONBe.SEGU_chFECCRE != null) ? vDIRECCIONBe.SEGU_chFECCRE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("FechaModificacion", DbType.String)).Value = ((vDIRECCIONBe.SEGU_chFECMOD != null) ? vDIRECCIONBe.SEGU_chFECMOD : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vIdCalle", DbType.String)).Value = ((vDIRECCIONBe.CALLE_P_chID != null) ? vDIRECCIONBe.CALLE_P_chID : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chDESDIR", DbType.String)).Value = ((vDIRECCIONBe.DIRE_chDESDIR != null) ? vDIRECCIONBe.DIRE_chDESDIR : (object)DBNull.Value);
                cnx.Open();
                //cmd.ExecuteNonQuery();

                cmd.Parameters.AddWithValue("@result", 0);
                //tell it that this parameter is a return value, not a regular inbound parameter
                cmd.Parameters["@result"].Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();
                //nor retrieve the value in the return parameter.
                string id = "";
                int idDireccion = 0;
                idDireccion = (Int32)cmd.Parameters["@result"].Value;
                id = Convert.ToString(idDireccion);

                cnx.Close();
                return id;


            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


        public List<DIRECCION_Be> fs_listarDireccion(DIRECCION_Be vDIRECCIONBe)
        {

            List<DIRECCION_Be> listado = new List<DIRECCION_Be>();

            using (SqlCommand cmd = new SqlCommand("sp_s_M_MAES_DIRE_DomiciliosPorPersona", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.Add(new SqlParameter("vPERS_P_inCODPER", DbType.Int32)).Value = vDIRECCIONBe.PERS_P_inCODPER;
                    cnx.Open();

                    SqlDataReader rs = cmd.ExecuteReader();
                    DIRECCION_Be DIRECCIONBe;

                    while (rs.Read())
                    {

                        DIRECCIONBe = new DIRECCION_Be();
                        DIRECCIONBe.DIRE_P_inCODDIR = Int32.Parse(rs["DIRE_P_inCODDIR"].ToString());
                        DIRECCIONBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString());
                        listado.Add(DIRECCIONBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }

    }
}
