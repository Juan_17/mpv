﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class REGISTRO_Dau
    {
        SqlConnection cnx;
        public REGISTRO_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }


        public int Pi_REGISTRO(REGISTRO_Be REGISTROBe)
        {
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PI_REGISTRO", cnx))
            {
                try
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("@IDTIPODOCIDE", DbType.String)).Value = ((REGISTROBe.IDTIPODOCIDE != null) ? REGISTROBe.IDTIPODOCIDE : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@NRODOCIDE", DbType.String)).Value = ((REGISTROBe.NRODOCIDE != null) ? REGISTROBe.NRODOCIDE : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTAPEPAT", DbType.String)).Value = ((REGISTROBe.TXTAPEPAT != null) ? REGISTROBe.TXTAPEPAT : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTAPEMAT", DbType.String)).Value = ((REGISTROBe.TXTAPEMAT != null) ? REGISTROBe.TXTAPEMAT : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTNOMRAZONSOCIAL", DbType.String)).Value = ((REGISTROBe.TXTNOMRAZONSOCIAL != null) ? REGISTROBe.TXTNOMRAZONSOCIAL : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTFIJOFONO", DbType.String)).Value = ((REGISTROBe.TXTFIJOFONO != null) ? REGISTROBe.TXTFIJOFONO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTCELULAR", DbType.String)).Value = ((REGISTROBe.TXTCELULAR != null) ? REGISTROBe.TXTCELULAR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTCORREO", DbType.String)).Value = ((REGISTROBe.TXTCORREO != null) ? REGISTROBe.TXTCORREO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTFECNAC", DbType.String)).Value = ((REGISTROBe.TXTFECNAC != null) ? REGISTROBe.TXTFECNAC : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODDEPDIR", DbType.String)).Value = ((REGISTROBe.CODDEPDIR != null) ? REGISTROBe.CODDEPDIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODPROVDIR", DbType.String)).Value = ((REGISTROBe.CODPROVDIR != null) ? REGISTROBe.CODPROVDIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODDISTDIR", DbType.String)).Value = ((REGISTROBe.CODDISTDIR != null) ? REGISTROBe.CODDISTDIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODVIADIR", DbType.String)).Value = ((REGISTROBe.CODVIADIR != null) ? REGISTROBe.CODVIADIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTVIA", DbType.String)).Value = ((REGISTROBe.TXTVIA != null) ? REGISTROBe.TXTVIA : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODOP1DIR", DbType.String)).Value = ((REGISTROBe.CODOP1DIR != null) ? REGISTROBe.CODOP1DIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTOP1DIR", DbType.String)).Value = ((REGISTROBe.TXTOP1DIR != null) ? REGISTROBe.TXTOP1DIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODOP2DIR", DbType.String)).Value = ((REGISTROBe.CODOP2DIR != null) ? REGISTROBe.CODOP2DIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTOP2DIR", DbType.String)).Value = ((REGISTROBe.TXTOP2DIR != null) ? REGISTROBe.TXTOP2DIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@CODOP3DIR", DbType.String)).Value = ((REGISTROBe.CODOP3DIR != null) ? REGISTROBe.CODOP3DIR : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@TXTOP3DIR", DbType.String)).Value = ((REGISTROBe.TXTOP3DIR != null) ? REGISTROBe.TXTOP3DIR : (object)DBNull.Value);

                    com.Parameters.Add("@ID", SqlDbType.VarChar, 100);
                    com.Parameters["@ID"].Direction = ParameterDirection.Output;

                    //com.Parameters.Add(new SqlParameter("@ID", DbType.Int32));
                    //com.Parameters["@ID"].Direction = ParameterDirection.InputOutput;
                    //com.Parameters["@ID"].Value = 0;

                    cnx.Open();
                    com.ExecuteNonQuery();

                    //return Convert.ToInt32(com.Parameters["vIDSESION"].Value);
                    return int.Parse(com.Parameters["@ID"].Value.ToString());

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }


        public int PS_REGISTRO(REGISTRO_Be REGISTROBe)
        {
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_REGISTRO", cnx))
            {
                try
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("@NRODOCIDE", DbType.String)).Value = ((REGISTROBe.NRODOCIDE != null) ? REGISTROBe.NRODOCIDE : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("@ID", DbType.Int32));
                    com.Parameters["@ID"].Direction = ParameterDirection.InputOutput;
                    com.Parameters["@ID"].Value = 0;

                    cnx.Open();
                    com.ExecuteNonQuery();

                    //return Convert.ToInt32(com.Parameters["vIDSESION"].Value);
                    return int.Parse(com.Parameters["@ID"].Value.ToString());

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }




        public List<REGISTRO_Be> fs_listarAdministrado(REGISTRO_Be vPERSONASBe)
        {

            List<REGISTRO_Be> listado = new List<REGISTRO_Be>();

            using (SqlCommand cmd = new SqlCommand("VVIRTUAL.dbo.PS_S_REGISTRO", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VNRODOCIDE", DbType.String)).Value = vPERSONASBe.NRODOCIDE;
                    
                    cnx.Open();

                    SqlDataReader rs = cmd.ExecuteReader();
                    REGISTRO_Be PERSONASBe;

                    while (rs.Read())
                    {

                        PERSONASBe = new REGISTRO_Be();
                        PERSONASBe.IDREGISTRO = Convert.ToInt32(rs["IDREGISTRO"].ToString());
                        PERSONASBe.IDTIPODOCIDE = Convert.ToInt32(rs["IDTIPODOCIDE"].ToString());
                        PERSONASBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                        PERSONASBe.TXTAPEPAT = rs["TXTAPEPAT"].ToString();
                        PERSONASBe.TXTAPEMAT = rs["TXTAPEMAT"].ToString();
                        PERSONASBe.TXTNOMRAZONSOCIAL = rs["TXTNOMRAZONSOCIAL"].ToString();
                        PERSONASBe.TXTFIJOFONO = rs["TXTFIJOFONO"].ToString();
                        PERSONASBe.TXTCELULAR = rs["TXTCELULAR"].ToString();
                        PERSONASBe.TXTCORREO = rs["TXTCORREO"].ToString();
                        PERSONASBe.TXTFECNAC = rs["TXTFECNAC"].ToString();
                        PERSONASBe.CODDEPDIR = rs["CODDEPDIR"].ToString();
                        PERSONASBe.CODPROVDIR = rs["CODPROVDIR"].ToString();
                        PERSONASBe.CODDISTDIR = rs["CODDISTDIR"].ToString();
                        PERSONASBe.CODVIADIR = rs["CODVIADIR"].ToString();
                        PERSONASBe.TXTVIA = rs["TXTVIA"].ToString();
                        PERSONASBe.CODOP1DIR = rs["CODOP1DIR"].ToString();
                        PERSONASBe.TXTOP1DIR = rs["TXTOP1DIR"].ToString();
                        PERSONASBe.CODOP2DIR = rs["CODOP2DIR"].ToString();
                        PERSONASBe.TXTOP2DIR = rs["TXTOP2DIR"].ToString();
                        PERSONASBe.CODOP3DIR = rs["CODOP3DIR"].ToString();
                        PERSONASBe.TXTOP3DIR = rs["TXTOP3DIR"].ToString();
                        PERSONASBe.DIRECCION = rs["DIRECCION"].ToString();
                        PERSONASBe.PERS_P_inCODPER = Convert.ToInt32(rs["PERS_P_inCODPER"].ToString());
                        PERSONASBe.DIRE_P_inCODDIR = Convert.ToInt32(rs["DIRE_P_inCODDIR"].ToString());
                        listado.Add(PERSONASBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }



    }


}
