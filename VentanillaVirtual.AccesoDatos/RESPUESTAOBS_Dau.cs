﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class RESPUESTAOBS_Dau
    {
        SqlConnection cnx;
        public RESPUESTAOBS_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public List<RESPUESTAOBS_Be> Ps_RESPUESTAOBS(string strIdSolicitud, string strIdObservacion)
        {

            List<RESPUESTAOBS_Be> listado = new List<RESPUESTAOBS_Be>();

            using (SqlCommand cmd = new SqlCommand("vvirtual.dbo.PS_RESPUESTAOBS", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = strIdSolicitud;
                    cmd.Parameters.Add(new SqlParameter("iIDOBSERVACION", DbType.Int32)).Value = strIdObservacion;
                    cnx.Open();
                    SqlDataReader rs = cmd.ExecuteReader();
                    RESPUESTAOBS_Be RESPUESTAOBSBe;

                    Int32 vCorrela = 0;

                    while (rs.Read())
                    {
                        vCorrela = vCorrela + 1;
                        RESPUESTAOBSBe = new RESPUESTAOBS_Be();
                        RESPUESTAOBSBe.CORRELA = vCorrela;
                        RESPUESTAOBSBe.IDRESPUESTAOBS = Int32.Parse(rs["IDRESPUESTAOBS"].ToString());
                        RESPUESTAOBSBe.IDOBSERVACION = Int32.Parse(rs["IDOBSERVACION"].ToString());
                        RESPUESTAOBSBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        RESPUESTAOBSBe.TXTRESPUESTA = rs["TXTRESPUESTA"].ToString();
                        RESPUESTAOBSBe.TXTURLARCHIVO = rs["TXTURLARCHIVO"].ToString();
                        RESPUESTAOBSBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());
                        
                        listado.Add(RESPUESTAOBSBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }


        public string Pid_RESPUESTAOBS(RESPUESTAOBS_Be RESPUESTAOBSBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("vvirtual.dbo.PID_RESPUESTAOBS", cnx);
                cmd.CommandType = CommandType.StoredProcedure;

             
                cmd.Parameters.Add(new SqlParameter("iIDRESPUESTAOBS", DbType.Int32)).Value = RESPUESTAOBSBe.IDRESPUESTAOBS;
                cmd.Parameters.Add(new SqlParameter("iIDOBSERVACION", DbType.Int32)).Value = RESPUESTAOBSBe.IDOBSERVACION;
                cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = RESPUESTAOBSBe.IDSOLICITUD;
                cmd.Parameters.Add(new SqlParameter("iTXTRESPUESTA", DbType.String)).Value = ((RESPUESTAOBSBe.TXTRESPUESTA != null) ? RESPUESTAOBSBe.TXTRESPUESTA : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTXTURLARCHIVO", DbType.String)).Value = ((RESPUESTAOBSBe.TXTURLARCHIVO != null) ? RESPUESTAOBSBe.TXTURLARCHIVO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTXTIP", DbType.String)).Value = ((RESPUESTAOBSBe.TXTIP != null) ? RESPUESTAOBSBe.TXTIP : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTIPOPROCESO", DbType.String)).Value = ((RESPUESTAOBSBe.TIPOPROCESO != null) ? RESPUESTAOBSBe.TIPOPROCESO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("oRESULT", DbType.String)).Direction = ParameterDirection.Output;

                cnx.Open();
                cmd.ExecuteNonQuery();
                string lsResultado = cmd.Parameters["oRESULT"].Value.ToString();
                cnx.Close();
                return lsResultado;

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
