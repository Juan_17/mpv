﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class M_MAES_DIRE_Das
    {
        SqlConnection cn;
        public M_MAES_DIRE_Das()
        {
            cn = new Cn_dbGeneral().Cn_SqlServer();
        }
        public int Piud_Direccion(M_MAES_DIRE_Be vM_MAES_DIREBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("PID_M_DIRECCION", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("vDIRE_P_inCODDIR", DbType.Int32));
                cmd.Parameters["vDIRE_P_inCODDIR"].Direction = ParameterDirection.InputOutput;
                cmd.Parameters["vDIRE_P_inCODDIR"].Value = 0;
                cmd.Parameters.Add(new SqlParameter("vPERS_P_inCODPER", DbType.Int32)).Value = vM_MAES_DIREBe.vPERS_P_inCODPER;
                cmd.Parameters.Add(new SqlParameter("vUBIG_P_inCODUBI", DbType.Int32)).Value = vM_MAES_DIREBe.vUBIG_P_inCODUBI;
                cmd.Parameters.Add(new SqlParameter("vDIRE_chDESDIR", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chDESDIR != null) ? vM_MAES_DIREBe.vDIRE_chDESDIR : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_btVIGDIR", DbType.Int32)).Value = vM_MAES_DIREBe.vDIRE_btVIGDIR;
                cmd.Parameters.Add(new SqlParameter("vSEGU_P_chCODUSU", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_P_chCODUSU != null) ? vM_MAES_DIREBe.vSEGU_P_chCODUSU : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chFECINI", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chFECINI != null) ? vM_MAES_DIREBe.vSEGU_chFECINI : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chFECMOD", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chFECMOD != null) ? vM_MAES_DIREBe.vSEGU_chFECMOD : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_btFLAELI", DbType.Int32)).Value = vM_MAES_DIREBe.vSEGU_btFLAELI;
                cmd.Parameters.Add(new SqlParameter("vCALLE", DbType.String)).Value = ((vM_MAES_DIREBe.vCALLE != null) ? vM_MAES_DIREBe.vCALLE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vVIA", DbType.String)).Value = ((vM_MAES_DIREBe.vVIA != null) ? vM_MAES_DIREBe.vVIA : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chNRO", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chNRO != null) ? vM_MAES_DIREBe.vDIRE_chNRO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chINT", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chINT != null) ? vM_MAES_DIREBe.vDIRE_chINT : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chMAN", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chMAN != null) ? vM_MAES_DIREBe.vDIRE_chMAN : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chLOTE", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chLOTE != null) ? vM_MAES_DIREBe.vDIRE_chLOTE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chURB", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chURB != null) ? vM_MAES_DIREBe.vDIRE_chURB : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vDIRE_chREFE", DbType.String)).Value = ((vM_MAES_DIREBe.vDIRE_chREFE != null) ? vM_MAES_DIREBe.vDIRE_chREFE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_inUSUCRE", DbType.Int32)).Value = vM_MAES_DIREBe.vSEGU_inUSUCRE;
                cmd.Parameters.Add(new SqlParameter("vSEGU_inUSUMOD", DbType.Int32)).Value = vM_MAES_DIREBe.vSEGU_inUSUMOD;
                cmd.Parameters.Add(new SqlParameter("vSEGU_chMODFEC", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chMODFEC != null) ? vM_MAES_DIREBe.vSEGU_chMODFEC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chCREFEC", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chCREFEC != null) ? vM_MAES_DIREBe.vSEGU_chCREFEC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUPC", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chUSUPC != null) ? vM_MAES_DIREBe.vSEGU_chUSUPC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUMAC", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chUSUMAC != null) ? vM_MAES_DIREBe.vSEGU_chUSUMAC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUIP", DbType.String)).Value = ((vM_MAES_DIREBe.vSEGU_chUSUIP != null) ? vM_MAES_DIREBe.vSEGU_chUSUIP : (object)DBNull.Value);
                cn.Open();
                cmd.ExecuteNonQuery();

                int cvDIRE_P_inCODDIR = 0;
                cvDIRE_P_inCODDIR = int.Parse(cmd.Parameters["vDIRE_P_inCODDIR"].Value.ToString());
                int lsResultado = cvDIRE_P_inCODDIR;
                cn.Close();
                return lsResultado;
                //return "1";

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


    }
}
