﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;


namespace VentanillaVirtual.AccesoDatos
{
    public class PAGO_Dau
    {
        SqlConnection cnx;

        public PAGO_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public string Pid_PAGOS(PAGO_Be PAGOBe)
        {

            using (SqlCommand com = new SqlCommand("VVIRTUAL.DBO.SP_U_GENERA_PAGO", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = PAGOBe.IDSOLICITUD;
                    com.Parameters.Add(new SqlParameter("iMONTO", DbType.Decimal)).Value = PAGOBe.MONTO;
                    com.Parameters.Add(new SqlParameter("iFEPAGO", DbType.String)).Value = ((PAGOBe.FEPAGO != null) ? PAGOBe.FEPAGO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("iID_CUENTA", DbType.Int32)).Value = PAGOBe.ID_CUENTA;
                    cnx.Open();
                    com.ExecuteNonQuery();

                    return "1";

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }


    }
}
