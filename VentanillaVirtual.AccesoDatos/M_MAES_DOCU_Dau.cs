﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;


namespace VentanillaVirtual.AccesoDatos
{
    public class M_MAES_DOCU_Dau
    {
        SqlConnection cnx;
        public M_MAES_DOCU_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public string Piud_Documento(M_MAES_DOCU_Be vM_MAES_DOCUBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("PID_M_MAES_DOCU", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("iDOCU_P_inCODDOC", DbType.Int32)).Value = vM_MAES_DOCUBe.DOCU_P_inCODDOC; //OUTPUT,

                cmd.Parameters.Add(new SqlParameter("iDOCU_P_inCODDOC", DbType.Int32));
                cmd.Parameters["iDOCU_P_inCODDOC"].Direction = ParameterDirection.InputOutput;
                cmd.Parameters["iDOCU_P_inCODDOC"].Value = 0;


                cmd.Parameters.Add(new SqlParameter("iOFIC_P_inCODOFI", DbType.Int32)).Value = vM_MAES_DOCUBe.OFIC_P_inCODOFI;
                cmd.Parameters.Add(new SqlParameter("iTIPO_P_inCODTIP", DbType.Int32)).Value = vM_MAES_DOCUBe.TIPO_P_inCODTIP;
                cmd.Parameters.Add(new SqlParameter("iDOCU_chANODOC", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chANODOC != null) ? vM_MAES_DOCUBe.DOCU_chANODOC : (object)DBNull.Value);
                //cmd.Parameters.Add(new SqlParameter("iDOCU_chNRODOC", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chNRODOC != null) ? vM_MAES_DOCUBe.DOCU_chNRODOC : (object)DBNull.Value); //OUTPUT,

                //cmd.Parameters.Add(new SqlParameter("iDOCU_chNRODOC", DbType.String)).Direction = ParameterDirection.Output;
                //cmd.Parameters["iDOCU_chNRODOC"].Value = "0";

                cmd.Parameters.Add("iDOCU_chNRODOC", SqlDbType.VarChar, 10);
                cmd.Parameters["iDOCU_chNRODOC"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add(new SqlParameter("iDOCU_chFECDOC", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chFECDOC != null) ? vM_MAES_DOCUBe.DOCU_chFECDOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_inNROFOL", DbType.Int32)).Value = vM_MAES_DOCUBe.DOCU_inNROFOL;
                cmd.Parameters.Add(new SqlParameter("iASUN_P_inCODASU", DbType.Int32)).Value = vM_MAES_DOCUBe.ASUN_P_inCODASU;
                cmd.Parameters.Add(new SqlParameter("iPERS_P_inCODPER", DbType.Int32)).Value = vM_MAES_DOCUBe.PERS_P_inCODPER;
                cmd.Parameters.Add(new SqlParameter("iDIRE_P_inCODDIR", DbType.Int32)).Value = vM_MAES_DOCUBe.DIRE_P_inCODDIR;
                cmd.Parameters.Add(new SqlParameter("iDOCU_chCODCON", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chCODCON != null) ? vM_MAES_DOCUBe.DOCU_chCODCON : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_chOBSDOC", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chOBSDOC != null) ? vM_MAES_DOCUBe.DOCU_chOBSDOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iESTA_P_inCODEST", DbType.Int32)).Value = vM_MAES_DOCUBe.ESTA_P_inCODEST;
                cmd.Parameters.Add(new SqlParameter("iSEGU_P_chCODUSU", DbType.String)).Value = vM_MAES_DOCUBe.SEGU_P_chCODUSU;
                cmd.Parameters.Add(new SqlParameter("iSEGU_chFECINI", DbType.String)).Value = vM_MAES_DOCUBe.SEGU_chFECINI;
                cmd.Parameters.Add(new SqlParameter("iSEGU_chFECMOD", DbType.String)).Value = vM_MAES_DOCUBe.SEGU_chFECMOD;
                cmd.Parameters.Add(new SqlParameter("iBAND_chCODEST", DbType.String)).Value = vM_MAES_DOCUBe.BAND_chCODEST;
                cmd.Parameters.Add(new SqlParameter("iMED_chCOD", DbType.String)).Value = vM_MAES_DOCUBe.MED_chCOD;
                cmd.Parameters.Add(new SqlParameter("iDOCU_chReg", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chReg != null) ? vM_MAES_DOCUBe.DOCU_chReg : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_chSIGJER", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chSIGJER != null) ? vM_MAES_DOCUBe.DOCU_chSIGJER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_chSIGOFI", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chSIGOFI != null) ? vM_MAES_DOCUBe.DOCU_chSIGOFI : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_chDESASU", DbType.String)).Value = ((vM_MAES_DOCUBe.DOCU_chDESASU != null) ? vM_MAES_DOCUBe.DOCU_chDESASU : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iDOCU_chFECPLA", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("iDOCU_chESRSTA", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("iDOCU_chINFRSTA", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_inCREUSU", DbType.Int32)).Value = vM_MAES_DOCUBe.SEGU_inCREUSU;
                cmd.Parameters.Add(new SqlParameter("vSEGU_inMODUSU", DbType.Int32)).Value = vM_MAES_DOCUBe.SEGU_inMODUSU;
                cmd.Parameters.Add(new SqlParameter("vSEGU_chCREFEC", DbType.String)).Value = ((vM_MAES_DOCUBe.SEGU_chCREFEC != null) ? vM_MAES_DOCUBe.SEGU_chCREFEC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chMODFEC", DbType.String)).Value = ((vM_MAES_DOCUBe.SEGU_chMODFEC != null) ? vM_MAES_DOCUBe.SEGU_chMODFEC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPAGO_inNROCOM", DbType.Int32)).Value = 0;
                cmd.Parameters.Add(new SqlParameter("vPAGO_chNOMNROPAG", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chIP", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chMAC", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSURED", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chPC", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chIPCRE", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chMACCRE", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUREDCRE", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vSEGU_chPCCRE", DbType.String)).Value = "";
                cmd.Parameters.Add(new SqlParameter("vPERS_PRESENTADO", DbType.String)).Value = "";

                //return int.Parse(com.Parameters["vRESULTADO"].Value.ToString());
                cnx.Open();
                cmd.ExecuteNonQuery();
                string vDOCU_chNRODOC = "";

                int vDOCU_P_inCODDOC = 0;
                vDOCU_P_inCODDOC = int.Parse(cmd.Parameters["iDOCU_P_inCODDOC"].Value.ToString());
                vDOCU_chNRODOC = Convert.ToString(cmd.Parameters["iDOCU_chNRODOC"].Value.ToString());

                string lsResultado = Convert.ToString(vDOCU_P_inCODDOC);
                cnx.Close();
                return lsResultado;
                //return "1";

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


    }
}
