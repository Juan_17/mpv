﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class PERSONAS_Das
    {

 

        SqlConnection cnx;
        public PERSONAS_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer_SID();
        }

        public List<PERSONAS_Be> fs_listarPersonas(PERSONAS_Be vPERSONASBe)
        {

            List<PERSONAS_Be> listado = new List<PERSONAS_Be>();

            using (SqlCommand cmd = new SqlCommand("sp_s_WEB_T_MAES_AUXPADR_Valida", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.Add(new SqlParameter("NroDoc", DbType.String)).Value = vPERSONASBe.vNroDoc;
                    cnx.Open();

                    SqlDataReader rs = cmd.ExecuteReader();
                    PERSONAS_Be PERSONASBe;

                    while (rs.Read())
                    {

                        PERSONASBe = new PERSONAS_Be();


                        PERSONASBe.PAD_P_chIDPER = rs["PAD_P_chIDPER"].ToString();
                        PERSONASBe.TIPPER_P_inIDPER = Int32.Parse(rs["TIPPER_P_inIDPER"].ToString());
                        PERSONASBe.TIPDOC_P_inIDDOC = Int32.Parse(rs["TIPDOC_P_inIDDOC"].ToString());
                        PERSONASBe.PAD_chNRODOC = rs["PAD_chNRODOC"].ToString();
                        PERSONASBe.PAD_chNOM = rs["PAD_chNOM"].ToString();
                        PERSONASBe.PAD_chAPEPAT = rs["PAD_chAPEPAT"].ToString();
                        PERSONASBe.PAD_chAPEMAT = rs["PAD_chAPEMAT"].ToString();
                        PERSONASBe.PAD_chRAZSOC = rs["PAD_chRAZSOC"].ToString();
                        PERSONASBe.PAD_chNOMCOM = rs["PAD_chNOMCOM"].ToString();
                        PERSONASBe.PAD_chFECNAC = rs["PAD_chFECNAC"].ToString();
                        PERSONASBe.PAD_chSEXO = rs["PAD_chSEXO"].ToString();
                        PERSONASBe.PAD_chTELFIJ1 = rs["PAD_chTELFIJ1"].ToString();
                        PERSONASBe.PAD_chTELFIJ2 = rs["PAD_chTELFIJ2"].ToString();
                        PERSONASBe.PAD_chNROCEL1 = rs["PAD_chNROCEL1"].ToString();
                        PERSONASBe.PAD_chNROCEL2 = rs["PAD_chNROCEL2"].ToString();
                        PERSONASBe.PAD_chMAIL = rs["PAD_chMAIL"].ToString();
                        PERSONASBe.PAD_inFLAELI = Int32.Parse(rs["PAD_inFLAELI"].ToString());
                        PERSONASBe.PAD_chFECCRE = rs["PAD_chFECCRE"].ToString();
                        PERSONASBe.PAD_chFECMOD = rs["PAD_chFECMOD"].ToString();
                        PERSONASBe.PAD_chAPLFUE = rs["PAD_chAPLFUE"].ToString();
                        listado.Add(PERSONASBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }


        public List<PERSONAS_Be> fs_listarAdministrado(PERSONAS_Be vPERSONASBe)
        {

            List<PERSONAS_Be> listado = new List<PERSONAS_Be>();

            using (SqlCommand cmd = new SqlCommand("sp_s_M_MAES_PERS_porcodigo_V1", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.Add(new SqlParameter("vPERS_P_inCODPER", DbType.Int32)).Value = 0;
                    cmd.Parameters.Add(new SqlParameter("vPERS_chNRODOC", DbType.String)).Value = vPERSONASBe.PERS_chNRODOC;
                    cmd.Parameters.Add(new SqlParameter("vSEGU_btFLAELI", DbType.Int32)).Value = vPERSONASBe.SEGU_btFLAELI;
                    cnx.Open();

                    SqlDataReader rs = cmd.ExecuteReader();
                    PERSONAS_Be PERSONASBe;

                    while (rs.Read())
                    {

                        PERSONASBe = new PERSONAS_Be();
                        PERSONASBe.PAD_P_chIDPER = rs["PERS_chNRODOC"].ToString();
                        PERSONASBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString());
                        listado.Add(PERSONASBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }


        public string Piud_Personas(PERSONAS_Be vPERSONASBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_i_WEB_T_MAES_AUXPADR_Registrar", cnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("IDPersona", DbType.String)).Value = ((vPERSONASBe.PAD_P_chIDPER != null) ? vPERSONASBe.PAD_P_chIDPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("IDTipPersona", DbType.Int32)).Value = vPERSONASBe.TIPPER_P_inIDPER;
                cmd.Parameters.Add(new SqlParameter("IDTipDoc", DbType.Int32)).Value = vPERSONASBe.TIPDOC_P_inIDDOC;
                cmd.Parameters.Add(new SqlParameter("Nombre", DbType.String)).Value = ((vPERSONASBe.PAD_chNOM != null) ? vPERSONASBe.PAD_chNOM : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("ApePaterno", DbType.String)).Value = ((vPERSONASBe.PAD_chAPEPAT != null) ? vPERSONASBe.PAD_chAPEPAT : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("ApeMaterno", DbType.String)).Value = ((vPERSONASBe.PAD_chAPEMAT != null) ? vPERSONASBe.PAD_chAPEMAT : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("RazonSocial", DbType.String)).Value = ((vPERSONASBe.PAD_chRAZSOC != null) ? vPERSONASBe.PAD_chRAZSOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("NombreCompleto", DbType.String)).Value = ((vPERSONASBe.PAD_chNOMCOM != null) ? vPERSONASBe.PAD_chNOMCOM : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("FechaNacimiento ", DbType.String)).Value = ((vPERSONASBe.PAD_chFECNAC != null) ? vPERSONASBe.PAD_chFECNAC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Sexo", DbType.String)).Value = ((vPERSONASBe.PAD_chSEXO != null) ? vPERSONASBe.PAD_chSEXO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Telefono1", DbType.String)).Value = ((vPERSONASBe.PAD_chTELFIJ1 != null) ? vPERSONASBe.PAD_chTELFIJ1 : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Telefono2", DbType.String)).Value = ((vPERSONASBe.PAD_chTELFIJ2 != null) ? vPERSONASBe.PAD_chTELFIJ2 : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Celular1", DbType.String)).Value = ((vPERSONASBe.PAD_chNROCEL1 != null) ? vPERSONASBe.PAD_chNROCEL1 : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Celular2", DbType.String)).Value = ((vPERSONASBe.PAD_chNROCEL2 != null) ? vPERSONASBe.PAD_chNROCEL2 : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Email", DbType.String)).Value = ((vPERSONASBe.PAD_chMAIL != null) ? vPERSONASBe.PAD_chMAIL : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Eliminado", DbType.Int32)).Value = vPERSONASBe.PAD_inFLAELI;
                cmd.Parameters.Add(new SqlParameter("FechaCreacion", DbType.String)).Value = ((vPERSONASBe.PAD_chFECCRE != null) ? vPERSONASBe.PAD_chFECCRE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("FechaModificacion", DbType.String)).Value = ((vPERSONASBe.PAD_chFECMOD != null) ? vPERSONASBe.PAD_chFECMOD : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("Aplicacion", DbType.String)).Value = ((vPERSONASBe.PAD_chAPLFUE != null) ? vPERSONASBe.PAD_chAPLFUE : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("NroDoc", DbType.String)).Value = ((vPERSONASBe.PAD_chNRODOC != null) ? vPERSONASBe.PAD_chNRODOC : (object)DBNull.Value);
                //cmd.Parameters.Add(new SqlParameter("oRESULT", DbType.String)).Direction = ParameterDirection.Output;
                cnx.Open();
                cmd.ExecuteNonQuery();
                //string lsResultado = cmd.Parameters["oRESULT"].Value.ToString();
                cnx.Close();
                //return lsResultado;
                return "1";

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


    }
}
