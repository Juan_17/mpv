﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class OBSERVACION_Dau
    {

        SqlConnection cnx;
        public OBSERVACION_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public List<OBSERVACION_Be> Ps_OBSERVACION(string strIdSolicitud)
        {

            List<OBSERVACION_Be> listado = new List<OBSERVACION_Be>();

            using (SqlCommand cmd = new SqlCommand("vvirtual.dbo.PS_OBSERVACION", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = strIdSolicitud;
                    cnx.Open();
                    SqlDataReader rs = cmd.ExecuteReader();
                    OBSERVACION_Be OBSERVACIONBe;

                    Int32 vCorrela = 0;

                    while (rs.Read())
                    {
                        vCorrela = vCorrela + 1;
                        OBSERVACIONBe = new OBSERVACION_Be();
                        OBSERVACIONBe.CORRELA = vCorrela;
                        OBSERVACIONBe.IDOBSERVACION = Int32.Parse(rs["IDOBSERVACION"].ToString());
                        OBSERVACIONBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        OBSERVACIONBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();
                        OBSERVACIONBe.FECREG = rs["FECREG"].ToString();
                        OBSERVACIONBe.ESTADO = rs["ESTADO"].ToString();
                        OBSERVACIONBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());
                        listado.Add(OBSERVACIONBe);
                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }

        public string Pid_OBSERVACION(OBSERVACION_Be OBSERVACIONBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("vvirtual.dbo.PID_OBSERVACION", cnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("iIDOBSERVACION", DbType.Int32)).Value = OBSERVACIONBe.IDOBSERVACION;
                cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.String)).Value = OBSERVACIONBe.IDSOLICITUD;
                cmd.Parameters.Add(new SqlParameter("iTXTOBSERVACION", DbType.String)).Value = OBSERVACIONBe.TXTOBSERVACION;
                cmd.Parameters.Add(new SqlParameter("iTXTIP", DbType.String)).Value = ((OBSERVACIONBe.TXTIP != null) ? OBSERVACIONBe.TXTIP : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("iTIPOPROCESO", DbType.String)).Value = ((OBSERVACIONBe.TIPOPROCESO != null) ? OBSERVACIONBe.TIPOPROCESO : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("oRESULT", DbType.String)).Direction = ParameterDirection.Output;

                cnx.Open();
                cmd.ExecuteNonQuery();
                string lsResultado = cmd.Parameters["oRESULT"].Value.ToString();
                cnx.Close();
                return lsResultado;

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }



    }
}
