﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class OFICINA_Das
    {

        SqlConnection cnx;
        public OFICINA_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public List<OFICINA_Be> Ps_LISTAOFICINA(OFICINA_Be vOFICINABe)
        {

            List<OFICINA_Be> listado = new List<OFICINA_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_OFICINA", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vOFIC_P_inCODOFI", DbType.Int32)).Value = vOFICINABe.OFIC_P_inCODOFI;
                    com.Parameters.Add(new SqlParameter("vSEGU_btFLAELI", DbType.String)).Value = vOFICINABe.SEGU_btFLAELI;
                    com.Parameters.Add(new SqlParameter("vGRUO_P_chID", DbType.String)).Value = "0101";

                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    OFICINA_Be OFICINABe ;

                    while (rs.Read())
                    {

                        OFICINABe = new OFICINA_Be();
                        OFICINABe.OFIC_P_inCODOFI = Int32.Parse(rs["OFIC_P_inCODOFI"].ToString());
                        OFICINABe.OFIC_chDESOFI = rs["OFIC_chDESOFI"].ToString();
                        listado.Add(OFICINABe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            return listado;
        }


    }
}
