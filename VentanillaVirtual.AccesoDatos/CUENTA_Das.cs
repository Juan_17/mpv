﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;


namespace VentanillaVirtual.AccesoDatos
{
    public class CUENTA_Das
    {
        SqlConnection cnx;
        public CUENTA_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }

        public List<CUENTA_Be> fs_listarNROCUENTA(CUENTA_Be vCALLESBe)
        {

            List<CUENTA_Be> listado = new List<CUENTA_Be>();

            using (SqlCommand cmd = new SqlCommand("SP_S_CUENTA", cnx))
            {

                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cnx.Open();

                    SqlDataReader rs = cmd.ExecuteReader();
                    CUENTA_Be CUENTABe;

                    while (rs.Read())
                    {

                        CUENTABe = new CUENTA_Be();

                        CUENTABe.ID_CUENTA = Convert.ToInt32(rs["ID_CUENTA"].ToString());
                        CUENTABe.NROCTA = rs["NROCTA"].ToString();

                        listado.Add(CUENTABe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }


            return listado;
        }


    }
}
