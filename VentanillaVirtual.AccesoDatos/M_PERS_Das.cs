﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;


namespace VentanillaVirtual.AccesoDatos
{
    public class M_PERS_Das
    {
        SqlConnection cn;
        public M_PERS_Das()
        {
            cn = new Cn_dbGeneral().Cn_SqlServer();
        }

        public int Piud_Personas(M_PERS_Be vM_PERSBe)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("PID_M_PERSONAS", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                //cmd.Parameters.Add(new SqlParameter("vPERS_P_inCODPER", DbType.Int32)).Value = vM_PERSBe.vPERS_P_inCODPER;
                cmd.Parameters.Add(new SqlParameter("vPERS_P_inCODPER", DbType.Int32));
                cmd.Parameters["vPERS_P_inCODPER"].Direction = ParameterDirection.InputOutput;
                cmd.Parameters["vPERS_P_inCODPER"].Value = 0;
                cmd.Parameters.Add(new SqlParameter("vVARI_P_chCODTIP_TIPPER", DbType.String)).Value = ((vM_PERSBe.vVARI_P_chCODTIP_TIPPER != null) ? vM_PERSBe.vVARI_P_chCODTIP_TIPPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vVARI_P_chCODCAR_TIPPER", DbType.String)).Value = ((vM_PERSBe.vVARI_P_chCODCAR_TIPPER != null) ? vM_PERSBe.vVARI_P_chCODCAR_TIPPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chNOMPER", DbType.String)).Value = ((vM_PERSBe.vPERS_chNOMPER != null) ? vM_PERSBe.vPERS_chNOMPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chAPEPAT", DbType.String)).Value = ((vM_PERSBe.vPERS_chAPEPAT != null) ? vM_PERSBe.vPERS_chAPEPAT : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chAPEMAT", DbType.String)).Value = ((vM_PERSBe.vPERS_chAPEMAT != null) ? vM_PERSBe.vPERS_chAPEMAT : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chRAZSOC", DbType.String)).Value = ((vM_PERSBe.vPERS_chRAZSOC != null) ? vM_PERSBe.vPERS_chRAZSOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vVARI_P_chCODTIP_TIPDOC", DbType.String)).Value = ((vM_PERSBe.vVARI_P_chCODTIP_TIPDOC != null) ? vM_PERSBe.vVARI_P_chCODTIP_TIPDOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vVARI_P_chCODCAR_TIPDOC", DbType.String)).Value = ((vM_PERSBe.vVARI_P_chCODCAR_TIPDOC != null) ? vM_PERSBe.vVARI_P_chCODCAR_TIPDOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chNRODOC", DbType.String)).Value = ((vM_PERSBe.vPERS_chNRODOC != null) ? vM_PERSBe.vPERS_chNRODOC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chTELPER", DbType.String)).Value = ((vM_PERSBe.vPERS_chTELPER != null) ? vM_PERSBe.vPERS_chTELPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chCELPER", DbType.String)).Value = ((vM_PERSBe.vPERS_chCELPER != null) ? vM_PERSBe.vPERS_chCELPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chMEIPER", DbType.String)).Value = ((vM_PERSBe.vPERS_chMEIPER != null) ? vM_PERSBe.vPERS_chMEIPER : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_btFLAELI", DbType.Int32)).Value = vM_PERSBe.vSEGU_btFLAELI;
                cmd.Parameters.Add(new SqlParameter("vPERS_chCONTRIB", DbType.String)).Value = ((vM_PERSBe.vPERS_chCONTRIB != null) ? vM_PERSBe.vPERS_chCONTRIB : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chCONTRIB_COD", DbType.String)).Value = ((vM_PERSBe.vPERS_chCONTRIB_COD != null) ? vM_PERSBe.vPERS_chCONTRIB_COD : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chNOMCOMP", DbType.String)).Value = ((vM_PERSBe.vPERS_chNOMCOMP != null) ? vM_PERSBe.vPERS_chNOMCOMP : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chFECNAC", DbType.String)).Value = ((vM_PERSBe.vPERS_chFECNAC != null) ? vM_PERSBe.vPERS_chFECNAC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_chSEX", DbType.String)).Value = ((vM_PERSBe.vPERS_chSEX != null) ? vM_PERSBe.vPERS_chSEX : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUPC", DbType.String)).Value = ((vM_PERSBe.vSEGU_chUSUPC != null) ? vM_PERSBe.vSEGU_chUSUPC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUMAC", DbType.String)).Value = ((vM_PERSBe.vSEGU_chUSUMAC != null) ? vM_PERSBe.vSEGU_chUSUMAC : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chUSUIP", DbType.String)).Value = ((vM_PERSBe.vSEGU_chUSUIP != null) ? vM_PERSBe.vSEGU_chUSUIP : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_inUSUCRE", DbType.Int32)).Value = vM_PERSBe.vSEGU_inUSUCRE;
                cmd.Parameters.Add(new SqlParameter("vSEGU_inUSUMOD", DbType.Int32)).Value = vM_PERSBe.vSEGU_inUSUMOD;
                cmd.Parameters.Add(new SqlParameter("vSEGU_chCREUSU", DbType.String)).Value = ((vM_PERSBe.vSEGU_chCREUSU != null) ? vM_PERSBe.vSEGU_chCREUSU : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vSEGU_chMODUSU", DbType.String)).Value = ((vM_PERSBe.vSEGU_chMODUSU != null) ? vM_PERSBe.vSEGU_chMODUSU : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_CIVIL", DbType.String)).Value = ((vM_PERSBe.vPERS_CIVIL != null) ? vM_PERSBe.vPERS_CIVIL : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_OCUPACION", DbType.String)).Value = ((vM_PERSBe.vPERS_OCUPACION != null) ? vM_PERSBe.vPERS_OCUPACION : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_NACIONALIDAD", DbType.String)).Value = ((vM_PERSBe.vPERS_NACIONALIDAD != null) ? vM_PERSBe.vPERS_NACIONALIDAD : (object)DBNull.Value);
                cmd.Parameters.Add(new SqlParameter("vPERS_LUGNAC", DbType.String)).Value = ((vM_PERSBe.vPERS_LUGNAC != null) ? vM_PERSBe.vPERS_LUGNAC : (object)DBNull.Value);
                cn.Open();
                cmd.ExecuteNonQuery();

                int cvPERS_P_inCODPER = 0;
                cvPERS_P_inCODPER = int.Parse(cmd.Parameters["vPERS_P_inCODPER"].Value.ToString());
                int lsResultado = cvPERS_P_inCODPER;
                cn.Close();
                return lsResultado;
                //return "1";

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
