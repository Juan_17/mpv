﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class SOLICITUD_Dau
    {

        SqlConnection cnx;

        public SOLICITUD_Dau()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }


        public string Pid_SOLICITUD(SOLICITUD_Be SOLICITUDBe)
        {

            using (SqlCommand com = new SqlCommand("VVIRTUAL.DBO.PIDU_SOLICITUD", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = SOLICITUDBe.IDSOLICITUD;
                    com.Parameters.Add(new SqlParameter("iIDREGISTRO", DbType.Int32)).Value = SOLICITUDBe.IDREGISTRO;
                    com.Parameters.Add(new SqlParameter("iTXTASUNTO", DbType.String)).Value = ((SOLICITUDBe.TXTASUNTO != null) ? SOLICITUDBe.TXTASUNTO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("iOBSERVACION", DbType.String)).Value = ((SOLICITUDBe.TXTOBSERVACION != null) ? SOLICITUDBe.TXTOBSERVACION : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("iFOLIOS", DbType.String)).Value = ((SOLICITUDBe.TXTFOLIOS != null) ? SOLICITUDBe.TXTFOLIOS : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("iIDTUPA", DbType.Int32)).Value = SOLICITUDBe.IDTUPA;
                    com.Parameters.Add(new SqlParameter("iESTADO", DbType.String)).Value = ((SOLICITUDBe.ESTADO != null) ? SOLICITUDBe.ESTADO : (object)DBNull.Value);
                    com.Parameters.Add(new SqlParameter("iTIPO_P_inCODTIP", DbType.Int32)).Value = SOLICITUDBe.TIPO_P_inCODTIP;
                    com.Parameters.Add(new SqlParameter("iOPC", DbType.Int32)).Value = SOLICITUDBe.OPC;
                    com.Parameters.Add(new SqlParameter("iIDTIPOSOLICITUD", DbType.Int32)).Value = SOLICITUDBe.IDTIPOSOLICITUD;
                    //com.Parameters.Add(new SqlParameter("oRESULT", DbType.String)).Direction = ParameterDirection.Output;
                    com.Parameters.Add("oRESULT", SqlDbType.VarChar, 100);
                    com.Parameters["oRESULT"].Direction = ParameterDirection.Output;





                    cnx.Open();
                    com.ExecuteNonQuery();

                  
                    return com.Parameters["oRESULT"].Value.ToString();

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }

        public string Pu_SOLICITUD_SID(SOLICITUD_Be PASEBe)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("vvirtual.dbo.SP_U_DOCUMENTO_SID", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("iIDSOLICITUD", DbType.Int32)).Value = PASEBe.IDSOLICITUD;
                cmd.Parameters.Add(new SqlParameter("iDOCU_P_inCODDOC", DbType.Int32)).Value = PASEBe.DOCU_P_inCODDOC; //PASEBe.IDREGISTRO;
                cmd.Parameters.Add("iDOCU_chNRODOC", SqlDbType.VarChar, 10);
                cmd.Parameters["iDOCU_chNRODOC"].Direction = ParameterDirection.Output;

                cnx.Open();
                cmd.ExecuteNonQuery();

                String vDOCU_chNRODOC;
                vDOCU_chNRODOC = Convert.ToString(cmd.Parameters["iDOCU_chNRODOC"].Value.ToString());

                cnx.Close();
                //return "1";
                return Convert.ToString(vDOCU_chNRODOC);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public List<SOLICITUD_Be> Ps_SOLICITUD(SOLICITUD_Be vSOLICITUDBe)
        {

            List<SOLICITUD_Be> listado = new List<SOLICITUD_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_SOLICITUD", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vIDREGISTRO", DbType.Int32)).Value = vSOLICITUDBe.IDREGISTRO;
                  
                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    SOLICITUD_Be SOLICITUDBe;

                    Int32 iCorrela = 0;

                    while (rs.Read())
                    {
                        iCorrela = iCorrela + 1;
                        SOLICITUDBe = new SOLICITUD_Be();

                        SOLICITUDBe.CORRELA = iCorrela;
                        SOLICITUDBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        SOLICITUDBe.NROSOLICITUD= rs["NROSOLICITUD"].ToString();
                        SOLICITUDBe.TXTASUNTO = rs["TXTASUNTO"].ToString();
                        SOLICITUDBe.ASUN_chDESASU = rs["ASUN_chDESASU"].ToString();  
                        SOLICITUDBe.AREA = rs["AREA"].ToString(); 
                        SOLICITUDBe.TXTFOLIOS = rs["TXTFOLIOS"].ToString(); 
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString(); 
                        SOLICITUDBe.FECHA = rs["FECHA"].ToString(); 
                        SOLICITUDBe.IDREGISTRO = Int32.Parse(rs["IDREGISTRO"].ToString());
                        SOLICITUDBe.FLAGPAGO = Int32.Parse(rs["FLAGPAGO"].ToString()); 
                        SOLICITUDBe.TIPO_P_inCODTIP = Int32.Parse(rs["TIPO_P_inCODTIP"].ToString());
                        SOLICITUDBe.IDTUPA = Int32.Parse(rs["IDTUPA"].ToString());
                        SOLICITUDBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString()); 
                        SOLICITUDBe.DIRE_P_inCODDIR = Int32.Parse(rs["DIRE_P_inCODDIR"].ToString()); 
                        SOLICITUDBe.OFIC_P_inCODOFI = Int32.Parse(rs["OFIC_P_inCODOFI"].ToString());
                        SOLICITUDBe.DOCU_P_inCODDOC = Int32.Parse(rs["DOCU_P_inCODDOC"].ToString());
                        SOLICITUDBe.IDTIPOSOLICITUD = Int32.Parse(rs["IDTIPOSOLICITUD"].ToString());
                        SOLICITUDBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());

                        SOLICITUDBe.FPAGO = rs["FPAGO"].ToString();
                        SOLICITUDBe.MONTO = Convert.ToDecimal(rs["MONTO"].ToString());
                        SOLICITUDBe.FEPAGO = rs["FEPAGO"].ToString();
                        SOLICITUDBe.ID_CUENTA = Int32.Parse(rs["ID_CUENTA"].ToString());
                        SOLICITUDBe.NROCTA = rs["NROCTA"].ToString();


                        SOLICITUDBe.FIJO = rs["TXTFIJOFONO"].ToString();
                        SOLICITUDBe.CORREO = rs["TXTCORREO"].ToString();
                        SOLICITUDBe.CELULAR = rs["TXTCELULAR"].ToString();
                        SOLICITUDBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                  

                        SOLICITUDBe.NROEXPEDIENTE = rs["NROEXPEDIENTE"].ToString();
                        SOLICITUDBe.FECHAEXPEDIENTE = rs["FECHAEXPEDIENTE"].ToString();
                        SOLICITUDBe.NRO_OBSERVACIONES = rs["NRO_OBSERVACIONES"].ToString();
                        SOLICITUDBe.NRO_RESPUESTAS = rs["NRO_RESPUESTAS"].ToString();




                        listado.Add(SOLICITUDBe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            return listado;
        }

        public List<SOLICITUD_Be> Ps_SOLICITUD_TIPO_AREA(SOLICITUD_Be vSOLICITUDBe)
        {

            List<SOLICITUD_Be> listado = new List<SOLICITUD_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_SOLICITUD_AREA", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vIDTIPOSOLICITUD", DbType.Int32)).Value = vSOLICITUDBe.IDTIPOSOLICITUD;
                    com.Parameters.Add(new SqlParameter("vOFIC_P_inCODOFI", DbType.Int32)).Value = vSOLICITUDBe.OFIC_P_inCODOFI;

                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    SOLICITUD_Be SOLICITUDBe;

                    Int32 iCorrela = 0;

                    while (rs.Read())
                    {
                        iCorrela = iCorrela + 1;
                        SOLICITUDBe = new SOLICITUD_Be();

                        SOLICITUDBe.CORRELA = iCorrela;
                        SOLICITUDBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        SOLICITUDBe.NROSOLICITUD = rs["NROSOLICITUD"].ToString();
                        SOLICITUDBe.TXTASUNTO = rs["TXTASUNTO"].ToString();
                        SOLICITUDBe.ASUN_chDESASU = rs["ASUN_chDESASU"].ToString();
                        SOLICITUDBe.AREA = rs["AREA"].ToString();
                        SOLICITUDBe.TXTFOLIOS = rs["TXTFOLIOS"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();
                        SOLICITUDBe.FECHA = rs["FECHA"].ToString();
                        SOLICITUDBe.IDREGISTRO = Int32.Parse(rs["IDREGISTRO"].ToString());
                        SOLICITUDBe.FLAGPAGO = Int32.Parse(rs["FLAGPAGO"].ToString());
                        SOLICITUDBe.TIPO_P_inCODTIP = Int32.Parse(rs["TIPO_P_inCODTIP"].ToString());
                        SOLICITUDBe.IDTUPA = Int32.Parse(rs["IDTUPA"].ToString());
                        SOLICITUDBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString());
                        SOLICITUDBe.DIRE_P_inCODDIR = Int32.Parse(rs["DIRE_P_inCODDIR"].ToString());
                        SOLICITUDBe.OFIC_P_inCODOFI = Int32.Parse(rs["OFIC_P_inCODOFI"].ToString());
                        SOLICITUDBe.DOCU_P_inCODDOC = Int32.Parse(rs["DOCU_P_inCODDOC"].ToString());
                        SOLICITUDBe.IDTIPOSOLICITUD = Int32.Parse(rs["IDTIPOSOLICITUD"].ToString());
                        SOLICITUDBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());
                        SOLICITUDBe.ADMINISTRADO = rs["ADMINISTRADO"].ToString();
                        SOLICITUDBe.FIJO = rs["TXTFIJOFONO"].ToString();
                        SOLICITUDBe.CORREO = rs["TXTCORREO"].ToString();
                        SOLICITUDBe.CELULAR = rs["TXTCELULAR"].ToString();
                        SOLICITUDBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();
                        
                        SOLICITUDBe.FPAGO = rs["FPAGO"].ToString();
                        SOLICITUDBe.FEPAGO = rs["FEPAGO"].ToString();
                        SOLICITUDBe.ID_CUENTA = Int32.Parse(rs["ID_CUENTA"].ToString());
                        SOLICITUDBe.MONTO = Convert.ToDecimal( rs["MONTO"].ToString());

                        listado.Add(SOLICITUDBe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            return listado;
        }

        public DataTable Ps_IMPCONSTANCIA(string strIDSOLICITUD)
        {
            DataTable dt;
            try
            {
                dt = new DataTable();

                using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_IMPCONSTANCIA", cnx))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("vIDSOLICITUD", DbType.Int32)).Value = strIDSOLICITUD;
                    cnx.Open();
                    SqlDataReader dr = com.ExecuteReader();
                    dt.Load(dr);
                    return dt;

                }

            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cnx.Close();
            }
        }









        //DOCUMENTO SIMPLES

        public List<SOLICITUD_Be> Ps_DOCUMENTOSIMPLE(string strTIPOSOLI, string strIDREGISTRO, string strTIPOBUSQ, string strBUSQ, string strORIGEN, string strArea)
        {

            List<SOLICITUD_Be> listado = new List<SOLICITUD_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_DOCUMENTOSIMPLE", cnx))

            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("iIDTIPOSOLICITUD", DbType.Int32)).Value = strTIPOSOLI;
                    com.Parameters.Add(new SqlParameter("iIDREGISTRO", DbType.Int32)).Value = ((strIDREGISTRO != null) ? strIDREGISTRO : (object) 0); 
                    com.Parameters.Add(new SqlParameter("iIDTIPOBUSQ", DbType.String)).Value = strTIPOBUSQ;
                    com.Parameters.Add(new SqlParameter("iBUSQUEDAD", DbType.String)).Value = strBUSQ;
                    com.Parameters.Add(new SqlParameter("iORIGEN", DbType.String)).Value = ((strORIGEN != null) ? strORIGEN : (object)0);
                    com.Parameters.Add(new SqlParameter("iAREA", DbType.Int32)).Value = ((strArea != null) ? strArea : (object)0);

                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    SOLICITUD_Be SOLICITUDBe;

                    Int32 iCorrela = 0;

                    while (rs.Read())
                    {
                        iCorrela = iCorrela + 1;
                        SOLICITUDBe = new SOLICITUD_Be();

                        SOLICITUDBe.CORRELA = iCorrela;
                        SOLICITUDBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        SOLICITUDBe.NROSOLICITUD = rs["NROSOLICITUD"].ToString();
                        SOLICITUDBe.TXTASUNTO = rs["TXTASUNTO"].ToString();
                        SOLICITUDBe.ASUN_chDESASU = rs["ASUN_chDESASU"].ToString();
                        SOLICITUDBe.AREA = rs["AREA"].ToString();
                        SOLICITUDBe.TXTFOLIOS = rs["TXTFOLIOS"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();
                        SOLICITUDBe.FECHA = rs["FECHA"].ToString();
                        SOLICITUDBe.IDREGISTRO = Int32.Parse(rs["IDREGISTRO"].ToString());
                        SOLICITUDBe.FLAGPAGO = Int32.Parse(rs["FLAGPAGO"].ToString());
                        SOLICITUDBe.TIPO_P_inCODTIP = Int32.Parse(rs["TIPO_P_inCODTIP"].ToString());
                        SOLICITUDBe.IDTUPA = Int32.Parse(rs["IDTUPA"].ToString());
                        SOLICITUDBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString());
                        SOLICITUDBe.DIRE_P_inCODDIR = Int32.Parse(rs["DIRE_P_inCODDIR"].ToString());
                        SOLICITUDBe.OFIC_P_inCODOFI = Int32.Parse(rs["OFIC_P_inCODOFI"].ToString());
                        SOLICITUDBe.DOCU_P_inCODDOC = Int32.Parse(rs["DOCU_P_inCODDOC"].ToString());
                        SOLICITUDBe.IDTIPOSOLICITUD = Int32.Parse(rs["IDTIPOSOLICITUD"].ToString());
                        SOLICITUDBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());
                        SOLICITUDBe.ADMINISTRADO = rs["ADMINISTRADO"].ToString();
                        SOLICITUDBe.FIJO = rs["TXTFIJOFONO"].ToString();
                        SOLICITUDBe.CORREO = rs["TXTCORREO"].ToString();
                        SOLICITUDBe.CELULAR = rs["TXTCELULAR"].ToString();
                        SOLICITUDBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();

                        SOLICITUDBe.NROEXPEDIENTE = rs["NROEXPEDIENTE"].ToString();
                        SOLICITUDBe.FECHAEXPEDIENTE = rs["FECHAEXPEDIENTE"].ToString();
                        SOLICITUDBe.NRO_OBSERVACIONES = rs["NRO_OBSERVACIONES"].ToString();
                        SOLICITUDBe.NRO_RESPUESTAS = rs["NRO_RESPUESTAS"].ToString();
                    
                        SOLICITUDBe.NROCTA = rs["NROCTA"].ToString();
                        SOLICITUDBe.FPAGO = rs["FPAGO"].ToString();
                        SOLICITUDBe.FEPAGO = rs["FEPAGO"].ToString();
                        SOLICITUDBe.ID_CUENTA = Int32.Parse(rs["ID_CUENTA"].ToString());
                        SOLICITUDBe.MONTO = Convert.ToDecimal(rs["MONTO"].ToString());
                        SOLICITUDBe.DIRECCION = rs["DIRECCION"].ToString();
                        SOLICITUDBe.DOCADMINISTRADO = rs["DOCADMINISTRADO"].ToString();
                        SOLICITUDBe.NRO_DOCADJ = rs["NRO_DOCADJ"].ToString();


                        listado.Add(SOLICITUDBe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            return listado;
        }

        //FIN DOCUMENTO SIMPLE


        //SOLICTUD EXPEDIENTE

        public List<SOLICITUD_Be> Ps_SEXPEDIENTE(string strTIPOSOLI, string strIDREGISTRO, string strTIPOBUSQ, string strBUSQ, string strORIGEN, string strArea)
        {

            List<SOLICITUD_Be> listado = new List<SOLICITUD_Be>();
            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_SEXPEDIENTE", cnx))

            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("iIDTIPOSOLICITUD", DbType.Int32)).Value = strTIPOSOLI;
                    com.Parameters.Add(new SqlParameter("iIDREGISTRO", DbType.Int32)).Value = ((strIDREGISTRO != null) ? strIDREGISTRO : (object)0);
                    com.Parameters.Add(new SqlParameter("iIDTIPOBUSQ", DbType.String)).Value = strTIPOBUSQ;
                    com.Parameters.Add(new SqlParameter("iBUSQUEDAD", DbType.String)).Value = strBUSQ;
                    com.Parameters.Add(new SqlParameter("iORIGEN", DbType.String)).Value = ((strORIGEN != null) ? strORIGEN : (object)0);
                    com.Parameters.Add(new SqlParameter("iAREA", DbType.Int32)).Value = ((strArea != null) ? strArea : (object)0);
                    
                    cnx.Open();
                    SqlDataReader rs = com.ExecuteReader();

                    SOLICITUD_Be SOLICITUDBe;

                    Int32 iCorrela = 0;

                    while (rs.Read())
                    {
                        iCorrela = iCorrela + 1;
                        SOLICITUDBe = new SOLICITUD_Be();

                        SOLICITUDBe.CORRELA = iCorrela;
                        SOLICITUDBe.IDSOLICITUD = Int32.Parse(rs["IDSOLICITUD"].ToString());
                        SOLICITUDBe.NROSOLICITUD = rs["NROSOLICITUD"].ToString();
                        SOLICITUDBe.TXTASUNTO = rs["TXTASUNTO"].ToString();
                        SOLICITUDBe.ASUN_chDESASU = rs["ASUN_chDESASU"].ToString();
                        SOLICITUDBe.AREA = rs["AREA"].ToString();
                        SOLICITUDBe.TXTFOLIOS = rs["TXTFOLIOS"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();
                        SOLICITUDBe.FECHA = rs["FECHA"].ToString();
                        SOLICITUDBe.IDREGISTRO = Int32.Parse(rs["IDREGISTRO"].ToString());
                        SOLICITUDBe.FLAGPAGO = Int32.Parse(rs["FLAGPAGO"].ToString());
                        SOLICITUDBe.TIPO_P_inCODTIP = Int32.Parse(rs["TIPO_P_inCODTIP"].ToString());
                        SOLICITUDBe.IDTUPA = Int32.Parse(rs["IDTUPA"].ToString());
                        SOLICITUDBe.PERS_P_inCODPER = Int32.Parse(rs["PERS_P_inCODPER"].ToString());
                        SOLICITUDBe.DIRE_P_inCODDIR = Int32.Parse(rs["DIRE_P_inCODDIR"].ToString());
                        SOLICITUDBe.OFIC_P_inCODOFI = Int32.Parse(rs["OFIC_P_inCODOFI"].ToString());
                        SOLICITUDBe.DOCU_P_inCODDOC = Int32.Parse(rs["DOCU_P_inCODDOC"].ToString());
                        SOLICITUDBe.IDTIPOSOLICITUD = Int32.Parse(rs["IDTIPOSOLICITUD"].ToString());
                        SOLICITUDBe.ID_CONDICION = Int32.Parse(rs["ID_CONDICION"].ToString());
                        SOLICITUDBe.ADMINISTRADO = rs["ADMINISTRADO"].ToString();
                        SOLICITUDBe.FIJO = rs["TXTFIJOFONO"].ToString();
                        SOLICITUDBe.CORREO = rs["TXTCORREO"].ToString();
                        SOLICITUDBe.CELULAR = rs["TXTCELULAR"].ToString();
                        SOLICITUDBe.NRODOCIDE = rs["NRODOCIDE"].ToString();
                        SOLICITUDBe.TXTOBSERVACION = rs["TXTOBSERVACION"].ToString();

                        SOLICITUDBe.NROEXPEDIENTE = rs["NROEXPEDIENTE"].ToString();
                        SOLICITUDBe.FECHAEXPEDIENTE = rs["FECHAEXPEDIENTE"].ToString();
                        
                        SOLICITUDBe.NRO_OBSERVACIONES = rs["NRO_OBSERVACIONES"].ToString();
                        SOLICITUDBe.NRO_RESPUESTAS = rs["NRO_RESPUESTAS"].ToString();

                        SOLICITUDBe.NROCTA = rs["NROCTA"].ToString();
                        SOLICITUDBe.FPAGO = rs["FPAGO"].ToString();
                        SOLICITUDBe.FEPAGO = rs["FEPAGO"].ToString();
                        SOLICITUDBe.ID_CUENTA = Int32.Parse(rs["ID_CUENTA"].ToString());
                        SOLICITUDBe.MONTO = Convert.ToDecimal(rs["MONTO"].ToString());
                        SOLICITUDBe.DIRECCION = rs["DIRECCION"].ToString();


                        SOLICITUDBe.DOCADMINISTRADO = rs["DOCADMINISTRADO"].ToString();
                        SOLICITUDBe.NRO_DOCADJ = rs["NRO_DOCADJ"].ToString();


                        listado.Add(SOLICITUDBe);

                    }

                    cnx.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            return listado;
        }

        //FIN SOLICTUD EXPEDIENTE


        public string PU_DATOSREGISTRO(int IDREGISTRO, int PERS_P_inCODPER, int DIRE_P_inCODDIR)
        {

            using (SqlCommand com = new SqlCommand("VVIRTUAL.DBO.PIDU_REGISTRO", cnx))
            {

                try
                {

                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;
                    com.Parameters.Add(new SqlParameter("iIDREGISTRO", DbType.Int32)).Value = IDREGISTRO;
                    com.Parameters.Add(new SqlParameter("iPERS_P_inCODPER", DbType.Int32)).Value = PERS_P_inCODPER;
                    com.Parameters.Add(new SqlParameter("iDIRE_P_inCODDIR", DbType.Int32)).Value = DIRE_P_inCODDIR;

                    com.Parameters.Add("oRESULT", SqlDbType.VarChar, 100);
                    com.Parameters["oRESULT"].Direction = ParameterDirection.Output;
                    cnx.Open();
                    com.ExecuteNonQuery();
                    return com.Parameters["oRESULT"].Value.ToString();

                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }
            }

        }


    }
}
