﻿using System.Data.SqlClient;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.Cn;
using System.Collections.Generic;
using System.Data;
using System;

namespace VentanillaVirtual.AccesoDatos
{
    public class TUPA_REQUISITO_Das
    {

        SqlConnection cnx;
        public TUPA_REQUISITO_Das()
        {
            cnx = new Cn_dbGeneral().Cn_SqlServer();
        }


        public DataTable PS_LISTATUPA_REQUISITO(TUPA_REQUISITO_Be vTUPA_REQUISITOBe)
        {

            DataTable dt;
            dt = new DataTable();

            List<TUPA_REQUISITO_Be> listado = new List<TUPA_REQUISITO_Be>();

            using (SqlCommand com = new SqlCommand("VVIRTUAL.dbo.PS_TUPA_REQUISITO", cnx))
            {

                try
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 10 * 60;

                    com.Parameters.Add(new SqlParameter("ASUN_P_inCODASU ", DbType.String)).Value = vTUPA_REQUISITOBe.ASUN_P_inCODASU;
                    cnx.Open();

                    SqlDataReader dr = com.ExecuteReader();
                    dt.Load(dr);
                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnx.Close();
                }

            }


        }


    }
}
