﻿window.onload = function ()
{
   
    FnInicio("Inicio");
    ListadoTupa("", "", 0);
    
};

////////////***configuracion Inicio
function FnInicio(activar)
{

    if (activar == "Inicio") {
        document.getElementById("Div01").style.display = 'inline';
        document.getElementById("Div02").style.display = 'none';
    }
    else {
        //alert("FnInicio");
        document.getElementById("Div01").style.display = 'none';
        document.getElementById("Div02").style.display = 'inline';
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }

}


function ListadoTupa(strNproc, strAsunto, strOficina) {
    var vNproc = strNproc;
    var vAsunto = strAsunto;
    var vOficina = strOficina;
 
    $("#MGridP").dataTable().fnDestroy()

    $('#MGridP').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            iDisplayLength: 5,
            lengthChange: false,
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
         
            ajax:
                {
                    "url": "../Expediente/BuscarTUPA",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strNproc: vNproc, strAsunto: vAsunto, strOficina: vOficina }
                },

            columns:
                [

                     {
                         "render": function (data, type, row, meta) {

                             return '<input id="rd' + row.ASUN_P_inCODASU + '" type="radio"  name="defaultRole" onClick="ActivarCasilla(\'' + row.ASUN_P_inCODASU + '\',\'' + row.BUS_ASUN_chDESASU + '\');">';

                         }
                     },

                   // { data: 'ASUN_P_inCODASU', name: "n1", orderable: false, autoWidth: false, width: "2%" },
                    { data: 'ASUN_chTUPASUBP', name: "n2", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'OFIC_chDESOFISEC', name: "n5", orderable: false, autoWidth: false, width: "38%" },
                    { data: 'BUS_ASUN_chDESASU', name: "n3", orderable: false, autoWidth: false, width: "60%" }
                  //  { data: 'ASUN_inTIEDIA', name: "n6", orderable: false, autoWidth: false, width: "5%" }


                ],
           // rowReorder: {
          //      selector: 'td:nth-child(2)'
           // },
          //  responsive: true

        }

    );

  

    oTable = $('#MGridP').DataTable();


}

///buscar tupa 
$('#BtnBuscarP').click(function ()
{
    var v1 = document.getElementById('txtNroproc').value;
    var v2 = document.getElementById('txtProcedimiento').value;
    var v3 = document.getElementById('ddlOfic').value;
    ListadoTupa(v1, v2, v3);

});




function ListadoRequisitos()
{

    var CodTupa = document.getElementById("HCodTupa").value;

    if (CodTupa == "")
    {
        alert("No existe Codigo de TUPA");
        return;

    }

    document.getElementById("DivRequisito").style.display = 'inline';

    $("#MGridR").dataTable().fnDestroy()

    $('#MGridR').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
            ajax:
                {
                    "url": "/Expediente/BuscarRequisitoTupa",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strCodigo: CodTupa }
                },

            columns:
                [
                    //{ data: 'ASUN_P_inCODASU', name: "n1", orderable: false, autoWidth: false, width: "2%" },
                    { data: 'REQ_chDES', name: "n2", orderable: true, autoWidth: false, width: "100%" }
                    //{ data: 'BUS_ASUN_chDESASU', name: "n3", orderable: true, autoWidth: false, width: "7%" },
                    //{ data: 'OFIC_chDESOFISEC', name: "n5", orderable: false, autoWidth: false, width: "5%" },
                    //{ data: 'ASUN_inTIEDIA', name: "n6", orderable: false, autoWidth: false, width: "5%" }
                ]

        }

    );
    //Apply Custom search on jQuery DataTables here
    oTable = $('#MGridR').DataTable();
}





function FnvalidarFormulario()
{
    var info = "";

    var vHCodRegi = document.getElementById('HCodRegi').value;
    var vSolicitud = document.getElementById('txtSolicitud').value;
    var vObs = document.getElementById('txtObs').value;
    var vFolio = document.getElementById('txtFolio').value;
    var vHCodTupa = document.getElementById('HCodTupa').value;


    if (vHCodRegi == null || vHCodRegi.length == 0) {
        info = "No existe el usuario de envio de datos, vuelve a iniciar sesión !";
    }

    if (vSolicitud == null || vSolicitud.length == 0)
    {
        info = "Ingrese la Solicitud";
    }
    else if (vFolio == null || vFolio.length == 0)
    {
        info = "Ingrese los Folios !";
    }

    if (vHCodTupa == null || vHCodTupa.length == 0)
    {
       info = "Seleccione el Procedimiento para registrar la solicitud.";
    }

    return info;

}


function FnConfirmaGuardar() {

    var vValidado = FnvalidarFormulario();

    if (vValidado != "")
    {
        alert(vValidado);
        return false;
    }
    else {
        var type = "confirm";

        noty({
            text: "Desea Guardar el Registro ?",
            type: "confirm",
            layout: "top",
            timeout: 2000,
            modal: "true",
            buttons: (type != "confirm") ? false : [{
                addClass: "btn btn-primary",
                text: "Aceptar",
                onClick: function (c) {
                    FnEnviarDatos();
                    c.close();
                }
            }, {
                addClass: "btn btn-danger",
                text: "Cancelar",
                onClick: function (c) {
                    c.close();

                }
            }]
        });

    }

    return false;

}


function FnEnviarDatos()
{

    var vSolicitud = document.getElementById('txtSolicitud').value;
    var vObs = document.getElementById('txtObs').value;
    var vFolio = document.getElementById('txtFolio').value;
    var vHCodTupa = document.getElementById('HCodTupa').value;
    //var vCodDocu = document.getElementById('HCodDocu').value;
    var vHCodRegi = document.getElementById('HCodRegi').value;
    //var vHCodTipdoc = document.getElementById('HIdDoc').value;
   // var vHOpc = document.getElementById('HOpc').value;

    $.ajax({
        // contentType: "application/json; charset=utf-8",
        url: '../Expediente/RegistrarExpediente',
        cache: false,
        type: 'POST',
        dataType: 'json',
        //data: decodeURIComponent($.param(formData)),
        data: { StrSolicitud: vSolicitud, StrObs: vObs, StrFolio: vFolio, StrHCodTupa: vHCodTupa, StrHCodRegi: vHCodRegi },
        //  cache: false,
        beforeSend: function () {
           // $.LoadingOverlay("show");
        },
        complete: function () {
           // $.LoadingOverlay("hide");
        },
        success: function (Data) {
           // $.LoadingOverlay("hide");

            if (Data.proValor != "0")
            {
                document.getElementById('H2NroSolicitud').innerHTML = "Nro de Solicitud :  <strong>" + Data.proValor + "</strong>";
                FnInicio("Fin");
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
           // $.LoadingOverlay("hide");
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }

    });

}




function ActivarCasilla(CODASUNTO, ASUNTO)
{

    document.getElementById("HCodTupa").value = CODASUNTO;
    document.getElementById("txtTupa").value = ASUNTO;

}

$('#MGridP').on('click', 'tr', function () {

     // $(this).css('background-color', "#D6D5C3");
     //  alert('CLICK ROW');
    //$(this).css("background-color", "#ffffff");
      oTable = $('#MGrid').DataTable();

    //if ($(this).hasClass('selected')) {
    //    $(this).removeClass('selected');
    //}
    //else {
    //    oTable.$('tr.selected').removeClass('selected');
    //    $(this).addClass('selected');
    //}



});

function cerrar() {

    document.getElementById("HCodTupa").value = "";
    document.getElementById("txtAsunto").value = "";
}


