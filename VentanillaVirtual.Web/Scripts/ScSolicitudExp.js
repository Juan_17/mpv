﻿window.onload = function () {
    var IdRegistro = 1;
    FnListaSolicitud(IdRegistro);
    listarCuenta();
    document.getElementById("TxtMonto").value = "0.00";
    
};

function listarCuenta() {
    $.ajax({
        type: "POST",
        url: "/Pagos/Pd_ListarNROCUENTA",
        cache: false,
        success: function (data) {
            $('#ddlCuenta').html("");
            $('#ddlCuenta').append('<option value="0" selected="selected">Seleccione Cuenta</option>');
            $.each(data, function (index, value) {
                $('#ddlCuenta').append('<option value="' + value.ID_CUENTA + '">' + value.NROCTA + '</option>');
                // $('#ddlCuenta').selectpicker('refresh');
            });
            //$('#ddlCalle').selectpicker('refresh');
            //$('#ddlCalle').addClass("selectpicker").selectpicker('refresh');
        }
    });
}

function FnListaSolicitud(StrCodigo) {
    var vTipoSolicitud = StrCodigo;
    //alert(vCodigo);
    $("#DtSolicitudes").dataTable().fnDestroy()

    $('#DtSolicitudes').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
            lengthChange: false,
            ajax:
                {
                    "url": "../SolicitudExp/BuscarSolicitud",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strTIPOSOLICITUD: vTipoSolicitud}
                },

            columns:
                [
                    { data: 'NROSOLICITUD', name: "n1", orderable: true, autoWidth: false, width: "5%" },

                     //{
                     //    orderable: false, name: "n2", autoWidth: false, width: "1%", "render": function (data, type, row, meta) {
                     //        if (row.IDTIPOSOLICITUD == "2") {
                     //            return '<center><i class="icon-copy"  data-toggle="tooltip" data-placement="top" title="Documento Simple" ></i></center>';
                     //        }
                     //        else {
                     //            return '<center><i class="icon-paste"  data-toggle="tooltip" data-placement="top" title="Expediente" ></i></center>';
                     //        }
                     //    }
                     //},

                    { data: 'FECHA', name: "n3", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'ADMINISTRADO', name: "n3", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'TXTASUNTO', name: "n4", orderable: true, autoWidth: false, width: "30%" },
                    { data: 'ASUN_chDESASU', name: "n5", orderable: false, autoWidth: false, width: "30%" },
                    { data: 'TXTFOLIOS', name: "n6", orderable: false, autoWidth: false, width: "5%" },
                    {
                        orderable: false, name: "n7", autoWidth: false, width: "10%", "render": function (data, type, row, meta) {
                            //Eliminar
                            var vmensaje = 'Desea eliminar la Solicitud ' + row.NROSOLICITUD + ' ?';
                            var vconfirm = 'confirm';
                            var vtop = 'top';
                            var vtrue = 'true';
                            //Procesar
                            var vmensaje2 = 'Al enviar la solicitud sera atendido por el área correspondiente, desea Enviar la Solicitud ' + row.NROSOLICITUD + ' ahora ?';

                            var vSele = '<div class="btn-toolbar">';



                            if (row.ID_CONDICION == 2) {
                                vSele += '<button class="btn btn-xs" onclick="FnAdjuntar(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\');"><i class="icon-paper-clip" data-toggle="tooltip" data-placement="top" title="Adjuntar Doc."></i></button>';
                                //vSele += '<button class="btn btn-xs" onclick="FnConfirmaEliSolicitud(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Solicitud"></i></button>';

                   
                                
                                vSele += '<button class="btn btn-xs" onclick="FnGenerarExpediente(\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.IDTIPOSOLICITUD + '\',\'' + row.TXTFOLIOS + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.IDTUPA + '\',\'' + row.OFIC_P_inCODOFI + '\',\'' + row.ASUN_chDESASU + '\',\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.ADMINISTRADO + '\',\'' + row.FIJO + '\',\'' + row.CORREO + '\',\'' + row.CELULAR + '\',\'' + row.NRODOCIDE + '\',\'' + row.TXTASUNTO  + '\',\'' + row.TXTOBSERVACION+ '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Generar Expediente"></i></button>';


                                

                               // vSele += '<button class="btn btn-xs" onclick="FnConfirmaProcSolicitud(\'' + vmensaje2 + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Generar Expediente"></i></button>';
                                vSele += '<button class="btn btn-xs" onclick="FnGenerarPago(\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.ASUN_chDESASU + '\',\'' + row.TXTASUNTO + '\',\'' + row.MONTO + '\',\'' + row.FEPAGO + '\',\'' + row.ID_CUENTA + '\',\'' + row.FLAGPAGO + '\');"><i class="icon-usd" data-toggle="tooltip" data-placement="top" title="Generar Pago"></i></button>';
           
                            }
                            else {
       
                                 vSele += '<button class="btn btn-xs" onclick="FnFormatoConstancia(\'' + row.IDSOLICITUD + '\');"><i class="icon-file-text" data-toggle="tooltip" data-placement="top" title="Imprimir Constancia."></i></button>';

                            }



                            //vSele += '<button class="btn btn-xs" onclick="FnProcesarSid(\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.TIPO_P_inCODTIP + '\',\'' + row.TXTFOLIOS + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.IDTUPA + '\',\'' + row.OFIC_P_inCODOFI + '\',\'' + row.TXTASUNTO + '\',\'' + row.IDSOLICITUD + '\',\'' + row.DOCU_P_inCODDOC + '\');"><i class="fa fa-files-o" data-toggle="tooltip" data-placement="top" title="Generar Expediente"></i></button>';
                            return vSele;

                        }
                    },

                ],
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true

        }

    );
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtSolicitudes').DataTable();

}


function FnGenerarExpediente(vPERS_P_inCODPER, vDIRE_P_inCODDIR, vTIPO_P_inCODTIP, vFOLIO, vOBS, vIDASUNTO, vOFICINA, vASUNTO, vIDSOLICITUD, vNROSOLICITUD, vADMINISTRADO, vFIJO, vCORREO, vCELULAR, vNRODOCIDE, vPROCEDIMIENTO, vOBSERVACION) {

    document.getElementById("TxtCorrelaT").value = vNROSOLICITUD;

    document.getElementById("HIdSolicitudT").value = vIDSOLICITUD;
    document.getElementById("TxtAsuntoT").value = vPROCEDIMIENTO;
    document.getElementById("HIdPersonaT").value = vPERS_P_inCODPER;
    document.getElementById("HIdDireccionT").value = vDIRE_P_inCODDIR;
    document.getElementById("HIdTipdocT").value = vTIPO_P_inCODTIP;
    document.getElementById("HFolioT").value = vFOLIO;
    document.getElementById("HIdTupaT").value = vIDASUNTO;
    document.getElementById("HIdOficinaT").value = vOFICINA;
    document.getElementById("HAdministradoT").value = vADMINISTRADO;
    document.getElementById("HFIJOT").value = vFIJO;
    document.getElementById("HCORREOT").value = vCORREO;
    document.getElementById("HCELULART").value = vCELULAR;
    document.getElementById("HNRODOCIDET").value = vNRODOCIDE;

    document.getElementById("TxtProcedimientoT").value = vASUNTO;
    document.getElementById("HOBSSOLIT").value = vOBSERVACION;

    document.forms["FrmGenerarSid"].reset();


    document.forms["FrmGenerar"].reset();


    $('#ModAdjuntar').modal('show');

}

function FnGenerarPago(vIDSOLICITUD, vNROSOLICITUD, vPROCEDIMIENTO, vASUNTO, vMONTO, vFEMSION,vIDCUENTA, vESTPAGO) {

    if (vESTPAGO == "2") {
        document.getElementById("BtnGenerarPago").disabled = true;
    }
    if (vESTPAGO == "1") {
        document.getElementById("BtnGenerarPago").disabled = false;
    }
    if (vESTPAGO == "3") {
        document.getElementById("BtnGenerarPago").disabled = true;
        document.getElementById("BtnConfirmarPago").disabled = true;
    }
    document.getElementById("TxtCorrela1").value = vNROSOLICITUD;
    document.getElementById("HIdSolicitud1").value = vIDSOLICITUD;
    document.getElementById("TxtAsunto1").value = vPROCEDIMIENTO;
    document.getElementById("TxtProcedimiento1").value = vASUNTO;
    document.getElementById("TxtFechaEmi").value = vFEMSION;
    document.getElementById("TxtMonto").value = vMONTO;
    document.getElementById("ddlCuenta").value = vIDCUENTA;
    
    FnListaArcAdjuntoPago(vIDSOLICITUD);
    document.forms["FrmGenerar"].reset();
    $('#ModPago').modal('show');
    
}

document.getElementById('BtnGenerarPago').onclick = function () {
    var vIDSOLICITUD = document.getElementById("HIdSolicitud1").value;
    var vFECHAEMI= document.getElementById("TxtFechaEmi").value;
    var vMONTO = document.getElementById("TxtMonto").value;
    var vIDCUENTA = document.getElementById("ddlCuenta").value;
  
    
    var data = new FormData();
    data.append("iIDSOLICITUD", vIDSOLICITUD);
    data.append("iFECHAEMI", vFECHAEMI);
    data.append("iMONTO", vMONTO);
    data.append("iID_CUENTA", vIDCUENTA);

    $.ajax({
        url: "../Pagos/GenerarPago",
        cache: false,
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            //$.LoadingOverlay("show");
        },
        complete: function () {
            //$.LoadingOverlay("hide");
        },
        success: function (Data) {
            //$.LoadingOverlay("hide");

            if (Data.proValor != "0") {
                document.getElementById('HId1').value = Data.proValor;
                var IdRegistro = 1;
                FnListaSolicitud(IdRegistro);
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //$.LoadingOverlay("hide");
            //alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}


document.getElementById('BtnGenerarTrans').onclick = function () {



    var vOBS = document.getElementById("TxtObservacionT").value;
    var vIDSOLICITUD = document.getElementById("HIdSolicitudT").value;
    var vASUNTO = document.getElementById("TxtAsuntoT").value;
    var vPROCEDIMIENTO = document.getElementById("TxtProcedimientoT").value;
    var vPERS_P_inCODPER = document.getElementById("HIdPersonaT").value;
    var vDIRE_P_inCODDIR = document.getElementById("HIdDireccionT").value;
    var vTIPO_P_inCODTIP = document.getElementById("HIdTipdocT").value;
    var vFOLIO = document.getElementById("HFolioT").value;
    var vIDASUNTO = document.getElementById("HIdTupaT").value;
    var vOFICINA = document.getElementById("HIdOficinaT").value;
    var vADMINISTRADO = document.getElementById("HAdministradoT").value;
    var vFIJO = document.getElementById("HFIJOT").value;
    var vCORREO = document.getElementById("HCORREOT").value;
    var vCELULAR = document.getElementById("HCELULART").value;
    var vNRODOCIDE = document.getElementById("HNRODOCIDET").value;
    var vOBSSOLI = document.getElementById("HOBSSOLIT").value;
    

    var data = new FormData();
    data.append("iPERS_P_inCODPER", vPERS_P_inCODPER);
    data.append("iDIRE_P_inCODDIR", vDIRE_P_inCODDIR);
    data.append("iTIPO_P_inCODTIP", vTIPO_P_inCODTIP);
    data.append("iFOLIO", vFOLIO);
    data.append("iOBS", vOBS);
    data.append("iIDASUNTO", vIDASUNTO);
    data.append("iOFICINA", vOFICINA);
    data.append("iASUNTO", vASUNTO);
    data.append("iIDSOLICITUD", vIDSOLICITUD);
    data.append("iADMINISTRADO", vADMINISTRADO);
    data.append("iFIJO", vFIJO);
    data.append("iCORREO", vCORREO);
    data.append("iCELULAR", vCELULAR);
    data.append("iNRODOCIDE", vNRODOCIDE);
    data.append("iPROCEDIMIENTO", vPROCEDIMIENTO);
    data.append("iOBSSOLI", vOBSSOLI);
    

    $.ajax({
        url: "../SolicitudExp/ProcesarSID",
        cache: false,
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            //$.LoadingOverlay("show");
        },
        complete: function () {
            //$.LoadingOverlay("hide");
        },
        success: function (Data) {
            //$.LoadingOverlay("hide");

            if (Data.proValor != "0") {
                document.getElementById('HId').value = Data.proValor;
                var IdRegistro = 1;
                FnListaSolicitud(IdRegistro);
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //$.LoadingOverlay("hide");
            //alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
  

}

function FnListaArcAdjuntoPago(vIdSolicitud) {

    $("#DtPagos").dataTable().fnDestroy()

    $('#DtPagos').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Pagos/BuscarAdjArchivoPago",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    }
                 
                    //{
                    //    orderable: false, name: "n4", width: '5%',
                    //    mRender: function (data, type, row) {
                    //        var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.DESCARCHIVO + ' ?';
                    //        var vconfirm = 'confirm';
                    //        var vtop = 'top';
                    //        var vtrue = 'true';

                    //        var vSele = '<span class="btn-group"> <a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="Delete"  onclick="FnConfirmaEliArcAdjuntoPago(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.CODARCHIVO + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash"></i></a> </span>';


                    //        return vSele;
                    //    }
                    //}

                ]

        }

    );

}


document.getElementById('BtnConfirmarPago').onclick = function () {
    var IdSolicitud = document.getElementById("HIdSolicitud1").value;
    FnEnviarPago(IdSolicitud);
}

//---------------------------------------------------

function FnEnviarPago(vIdSolicitud) {



    $.ajax({
        type: 'POST',
        url: '../Pagos/ActualizarEstPago',
        data: { strIdSolicitud: vIdSolicitud, strOpc: "3" },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });



}
