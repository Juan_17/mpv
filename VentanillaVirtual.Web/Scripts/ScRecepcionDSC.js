﻿window.onload = function () {

    RadioSelect(null);
    var IdRegistro = document.getElementById("HCodRegi").value;
    FnListaDocSimple("2", IdRegistro);

};

function RadioSelect(myRadio) {


    document.getElementById("LbSeleccionado").innerHTML = $('input[name="OptionRadio"]:checked').parent().text();
    $('#TxtBuscar').val("");
    $('#TxtBuscar').focus();
    //alert('New value: ' + myRadio.value);

}

function FnListaDocSimple(StrTipo, StrIdRegistro){

    var vTipo = StrTipo;
    var vIdRegistro = StrIdRegistro;

    $("#DtDocSimple").dataTable().fnDestroy()

    $('#DtDocSimple').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
            lengthChange: false,
            ajax:
                {
                    "url": "../DocSimple/BuscarDocSimple",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strTipo: vTipo, strIdRegistro: vIdRegistro, strOrigen: 'DSC' }
                },

            columns:
                [
                    { data: 'NROSOLICITUD', name: "n1", orderable: true, autoWidth: false, width: "5%" },
                    { data: 'NROEXPEDIENTE', name: "n6", orderable: false, autoWidth: false, width: "5%" },
                    { data: 'FECHA', name: "n2", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'ADMINISTRADO', name: "n3", orderable: false, autoWidth: false, width: "20%" },
                    { data: 'TXTASUNTO', name: "n4", orderable: true, autoWidth: false, width: "30%" },
                    { data: 'TXTFOLIOS', name: "n5", orderable: false, autoWidth: false, width: "5%" },
                  
                    { data: 'NRO_OBSERVACIONES', name: "n7", orderable: false, autoWidth: false, width: "5%" },

                          {
                              orderable: false, name: "n8", autoWidth: false, width: "5%", "render": function (data, type, row, meta) {

                                  if (row.ID_CONDICION == '3') {
                                      vSele = '<span class="label label-warning">Recepción</span>';
                                  }
                                  else if (row.ID_CONDICION == '4') {
                                      vSele = '<span class="label label-danger">Observado</span>';
                                  }
                                  else if (row.ID_CONDICION == '5') {
                                      vSele = '<span class="label label-default">Generado</span>';
                                  }

                                  return vSele;

                              }
                          },

                    {
                        orderable: false, name: "n9", autoWidth: false, width: "10%", "render": function (data, type, row, meta) {
                            //Eliminar
                          //  var vmensaje = 'Desea eliminar la Solicitud ' + row.NROSOLICITUD + ' ?';
                          //  var vconfirm = 'confirm';
                          //  var vtop = 'top';
                         //   var vtrue = 'true';
                        
                            var vSele = '<div class="btn-toolbar">';

                            vSele += '<button class="btn btn-xs" onclick="FnAdjuntar(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\');"><i class="icon-paper-clip" style="color:GREEN" data-toggle="tooltip" data-placement="top" title="Documentos Adjuntos"></i></button>';

                            if (row.NRO_OBSERVACIONES > 0)
                            {
                                vSele += '<button class="btn btn-xs" onclick="FnObservacion(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\');"><i class="icon-calendar" style="color:NAVY" data-toggle="tooltip" data-placement="top" title="Observación"></i></button>';
                            }

                            if (row.FLAGPAGO == 1) {
                                vSele += '<button class="btn btn-xs" onclick="FnAdjuntarPago(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\',\'' + row.MONTO + '\',\'' + row.NROCTA + '\');"><i class="icon-usd" data-toggle="tooltip" data-placement="top" title="Cancelar Tramite"></i></button>';
                            }

                            vSele += '<button class="btn btn-xs" onclick="FnFormatoConstancia(\'' + row.IDSOLICITUD + '\');"><i class="icon-file-text" data-toggle="tooltip" data-placement="top" title="Imprimir Constancia."></i></button>';
                            vSele += '</div>'
                            return vSele;

                        }
                    },
                   

                ],
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true

        }

    );
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtDocSimple').DataTable();

}


function FnFormatoConstancia(vIdSolicitud) {
    window.open("../Solicitud/ImprimirConstancia/?vId=" + vIdSolicitud, '_blank');
}


function FnAdjuntarPago(vIdSolicitud, vAsunto, vCorrela, vMonto, vNroCta) {

    document.getElementById("HIdSolicitudPag").value = vIdSolicitud;
    document.getElementById("TxtAsuntoPag").value = vAsunto;
    document.getElementById("TxtCorrelaPag").value = vCorrela;
    document.getElementById("TxtMontoPagar").value = vMonto;
    document.getElementById("TxtNroCuenta").value = vNroCta;


    document.forms["FrmAdjuntarPago"].reset();
    //FnListaArcAdjuntoPago(vIdSolicitud)
    
    $('#ModAdjuntarPago').modal('show');

}

function FnListaArcAdjuntoPago(vIdSolicitud) {

    $("#TbArcAdjuntoPag").dataTable().fnDestroy()

    $('#TbArcAdjuntoPag').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Pagos/BuscarAdjArchivoPago",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    },
                    //{ data: 'TXTURLFOTO', name: "n3", orderable: false, autoWidth: false, width: "20%" },
                    {
                        orderable: false, name: "n4", width: '5%',
                        mRender: function (data, type, row) {
                            var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.DESCARCHIVO + ' ?';
                            var vconfirm = 'confirm';
                            var vtop = 'top';
                            var vtrue = 'true';

                            var vSele = '<span class="btn-group"> <a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="Delete"  onclick="FnConfirmaEliArcAdjuntoPago(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.CODARCHIVO + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash"></i></a> </span>';


                            return vSele;
                        }
                    }

                ]

        }

    );

    var oTable = $('#TbArcAdjuntoPag').DataTable();

    var info = oTable.page.info();

    var count = info.recordsTotal;

    if (count == "0") {
        //deshabilita
        document.getElementById("BtnEnviarPago").disabled = true;

    } else {
        //habilita
        document.getElementById("BtnEnviarPago").disabled = false;

    }


}

function FnConfirmaEliArcAdjuntoPago(text, type, layout, modal, CodArc, IdSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEliArcAdjuntoPago(CodArc, IdSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliArcAdjuntoPago(vIdArchivo, vIdSolicitud) {

    oTable = $('#TbArcAdjuntoPag').DataTable();

    $.ajax({
        type: 'POST',
        url: '../Pagos/EliminarAdjArchivoPago',
        data: { strIdArc: vIdArchivo },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

    FnListaArcAdjuntoPago(vIdSolicitud);

}

document.getElementById('BtnAdjuntarPago').onclick = function () {

    var IdSolicitud = document.getElementById("HIdSolicitudPag").value;
    var vFechDep = document.getElementById("txtFDeposito").value;


    if (vFechDep.length == 0 || IdSolicitud.length == 0) {
        alert("Ingrese la Fecha de Deposito");
        return false;
    }

    var data = new FormData();
    data.append("vTipo", "1");
    data.append("vIdSolicitud", IdSolicitud);
    data.append("vDescrip", vFechDep);

    var files = $("#UploadArchivoPag").get(0).files;
    if (files.length > 0) {
        data.append("MFiles", files[0]);
    }
    else {
        alert("Seleccione el Archivo a adjuntar al Pago");
        return false;
    }

    var fileName = document.getElementById("UploadArchivoPag").value;

    if (fileName.length > 0) {
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        if (extFile == "jpg" || extFile == "jpge" || extFile == "gif" || extFile == "png" || extFile == "pdf") { } else
        {
            alert("Seleccione el archivo de tipo: jpg,jpge,gif,png,pdf");
            return false;
        }
    }



    $.ajax({
        url: "../Pagos/AdjArchivoPago",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {

            document.forms["FrmAdjuntarPago"].reset();
            FnListaArcAdjuntoPago(IdSolicitud);
            //HABILITA BOTON
            document.getElementById("BtnEnviarPago").disabled = false;
        },
        error: function (er) {
            alert(er.message);
        }

    });

}

document.getElementById('BtnBuscar').onclick = function () {
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtDocSimple').DataTable();

    var TipValor = $('input[name="OptionRadio"]:checked').val();

    oTable.columns(0).search(TipValor);
    oTable.columns(1).search($('#TxtBuscar').val());
    oTable.columns(2).search('');
    ////hit search on server
    oTable.draw();

}

document.getElementById('BtnEnviarPago').onclick = function () {
    var IdSolicitud = document.getElementById("HIdSolicitudPag").value;
    FnEnviarPago(IdSolicitud);
}

//---------------------------------------------------

function FnEnviarPago(vIdSolicitud) {



    $.ajax({
        type: 'POST',
        url: '../Pagos/ActualizarEstPago',
        data: { strIdSolicitud: vIdSolicitud, strOpc: "2" },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });



}




//adjuntar archivos solo lectura

function FnAdjuntar(vIdSolicitud, vAsunto, vCorrela) {
    document.getElementById("HIdSolicitud").value = vIdSolicitud;
    document.getElementById("TxtAsunto").value = vAsunto;
    document.getElementById("TxtCorrela").value = vCorrela;

    // document.getElementById("DivArchivo").style.display = 'inline';
  
    document.getElementById("DivArchivo").style.display = 'none';
  
    document.forms["FrmAdjuntar"].reset();
    FnListaArcAdjunto(vIdSolicitud)

    $('#ModAdjuntar').modal('show');

}

function FnListaArcAdjunto(vIdSolicitud) {

    $("#TbArcAdjunto").dataTable().fnDestroy()

    $('#TbArcAdjunto').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarAdjArchivo",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    },
                    
                    {
                        orderable: false, name: "n4", width: '5%',
                        mRender: function (data, type, row) {
                            return '';
                        }
                    }

                ]

        }

    );

}

//fin adjuntar archivo


///observaciones solo lectura

function FnObservacion(vIdSolicitud, vAsunto, vCorrela) {
    document.getElementById("HIdSolicitudO").value = vIdSolicitud;
    document.getElementById("TxtAsuntoO").value = vAsunto;
    document.getElementById("TxtCorrelaO").value = vCorrela;

    document.getElementById("DivObserva").style.display = 'none';
    //document.forms["FrmObservacion"].reset();

    FnListaObservacion(vIdSolicitud)

    $('#ModObservacion').modal('show');

}

function FnListaObservacion(vIdSolicitud) {

    $("#TbListObservacion").dataTable().fnDestroy()

    $('#TbListObservacion').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarObservacion",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'TXTOBSERVACION', name: "n2", width: '90%' },

                     {
                         orderable: false, name: "n7", autoWidth: false, width: "10%", "render": function (data, type, row, meta) {

                             var vSele = '<div class="btn-toolbar">';
                             vSele += '<button class="btn btn-xs" onclick="FnRespuestaObs(\'' + row.IDSOLICITUD + '\',\'' + row.IDOBSERVACION + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.CORRELA + '\');"><i class="icon-list-ul" style="color:GREEN" data-toggle="tooltip" data-placement="top" title="Responder Observación"></i></button>';
                                vSele += '</div>';
                             return vSele;

                         }
                     },

                ]

        }

    );

}

///fin observaciones


///respuestas

function FnRespuestaObs(vIdSolicitud, vIdObservacion, vObservacion,vCorrela)
{

    document.getElementById("HIdSolicitudR").value = vIdSolicitud;
    document.getElementById("HIdObservacionR").value = vIdObservacion;
    document.getElementById("TxtCorrelaR").value = vCorrela;
    document.getElementById("TxtObservacion").value = vObservacion;

    document.getElementById("DivRespuesta").style.display = 'inline';
    
    document.forms["FrmRespuesta"].reset();

    FnListaRespuestaObs(vIdSolicitud, vIdObservacion)

    $('#ModRespuestaObs').modal('show');

}

function FnListaRespuestaObs(vIdSolicitud, vIdObservacion) {

    $("#TbRespuestaObs").dataTable().fnDestroy()

    $('#TbRespuestaObs').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarRespuestaObs",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud, IdObservacion: vIdObservacion }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'TXTRESPUESTA', name: "n2", width: '90%' },
                      {
                          orderable: false, name: "n3", width: '4%',
                          mRender: function (data, type, row) {

                              var vSele = "";
                              var icono = "../Imagenes/Iconos/pdf.jpg";
                              if (row.TXTURLARCHIVO != "") {
                                  vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                              }
                              return vSele;

                          }
                      },

                   {
                       orderable: false, name: "n4", width: '9%',
                       mRender: function (data, type, row) {
                           var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.TXTRESPUESTA + ' ?';
                           var vconfirm = 'confirm';
                           var vtop = 'top';
                           var vtrue = 'true';

                           var vSele = '<div class="btn-toolbar">';
                           vSele += '<button class="btn btn-xs" onclick="FnConfirmaEliRespuesta(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDOBSERVACION + '\',\'' + row.IDRESPUESTAOBS + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Respuesta"></i></button>';
                           vSele += '</div>';
                           return vSele;
                       }
                   }

                ]

        }

    );

}

document.getElementById('BtnAdjuntarR').onclick = function () {

    var IdSolicitud = document.getElementById("HIdSolicitudR").value;
    var IdObservacion = document.getElementById("HIdObservacionR").value;
    var Respuesta = document.getElementById("TxtRespuesta").value;


    if (Respuesta.length == 0 || IdObservacion.length == 0 || IdSolicitud.length == 0) {
        alert("Ingrese la Respuesta de la Observación");
        return false;
    }

    var data = new FormData();
    data.append("vTipo", "1");
    data.append("vIdSolicitud", IdSolicitud);
    data.append("vIdObservacion", IdObservacion);
    data.append("vRespuesta", Respuesta);

    var files = $("#UploadArchivoR").get(0).files;
    if (files.length > 0) {
        data.append("MFiles", files[0]);
    }
   // else {
   //     alert("Seleccione el Archivo a adjuntar a la Observación");
    //    return false;
   // }

    var fileName = document.getElementById("UploadArchivoR").value;

    if (fileName.length > 0) {
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        if (extFile == "jpg" || extFile == "jpge" || extFile == "gif" || extFile == "png" || extFile == "pdf") { } else
        {
            alert("Seleccione el archivo de tipo: jpg,jpge,gif,png,pdf");
            return false;
        }
    }



    $.ajax({
        url: "../Solicitud/RegistraRespuestaObs",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            //var vTxtId = document.getElementById("TxtCodDoc").value;
            document.forms["FrmRespuesta"].reset();
            FnListaRespuestaObs(IdSolicitud, IdObservacion);
        },
        error: function (er) {
            alert(er.message);
        }

    });




}

function FnConfirmaEliRespuesta(text, type, layout, modal, IdObservacion, IdRespuestaObs,IdSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEliRespuestaObs(IdObservacion, IdRespuestaObs,IdSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliRespuestaObs(vIdObservacion, vIdRespuestaObs,vIdSolicitud) {

    oTable = $('#TbRespuestaObs').DataTable();

    $.ajax({
        type: 'POST',
        url: '../Solicitud/EliminarRespuestaObs',
        data: { strIdObservacion: vIdObservacion, strIdRespuestaObs: vIdRespuestaObs },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

    FnListaRespuestaObs(vIdSolicitud,vIdObservacion);

}

///fin respuestas