﻿window.onload = function ()
{
    RadioSelect(null);
    FnListaDocSimple("2");
    listarCuenta();
    document.getElementById("TxtMonto").value = "0.00";
};

function listarCuenta() {
    $.ajax({
        type: "POST",
        url: "/Pagos/Pd_ListarNROCUENTA",
        cache: false,
        success: function (data) {
            $('#ddlCuenta').html("");
            $('#ddlCuenta').append('<option value="0" selected="selected">Seleccione Cuenta</option>');
            $.each(data, function (index, value) {
                $('#ddlCuenta').append('<option value="' + value.ID_CUENTA + '">' + value.NROCTA + '</option>');
                // $('#ddlCuenta').selectpicker('refresh');
            });
            //$('#ddlCalle').selectpicker('refresh');
            //$('#ddlCalle').addClass("selectpicker").selectpicker('refresh');
        }
    });
}

function RadioSelect(myRadio)
{

    
    document.getElementById("LbSeleccionado").innerHTML = $('input[name="OptionRadio"]:checked').parent().text();
    $('#TxtBuscar').val("");
    $('#TxtBuscar').focus();
    //alert('New value: ' + myRadio.value);

}

function FnListaDocSimple(StrCodigo) {
    var vCodigo = StrCodigo;
    //alert(vCodigo);
    $("#DtDocSimple").dataTable().fnDestroy()

    $('#DtDocSimple').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
            lengthChange: false,
            ajax:
                {
                    "url": "../DocSimple/BuscarDocSimple",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strTipo: vCodigo }
                },

            columns:
                [
                    { data: 'NROSOLICITUD', name: "n1", orderable: true, autoWidth: false, width: "5%" },
                    { data: 'FECHA', name: "n3", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'ADMINISTRADO', name: "n3", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'TXTASUNTO', name: "n4", orderable: true, autoWidth: false, width: "30%" },
                    { data: 'TXTFOLIOS', name: "n6", orderable: false, autoWidth: false, width: "5%" },



                    {
                        orderable: false, name: "n7", autoWidth: false, width: "4%", "render": function (data, type, row, meta) {

                            var vSele = '';

                            if (row.ID_CONDICION == '2') {
                                vSele = '<span class="label label-success">Enviado</span>';
                            }
                            else if (row.ID_CONDICION == '3') {
                                vSele = '<span class="label label-warning">Recepción</span>';
                            }
                            else if (row.ID_CONDICION == '4') {
                                vSele = '<span class="label label-danger">Observado</span>';
                            }
                            else if (row.ID_CONDICION == '5') {
                                vSele = '<span class="label label-default">Generado</span>';
                            }
                            return vSele;

                        }
                    },



                    {
                        orderable: false, name: "n8", autoWidth: false, width: "10%", "render": function (data, type, row, meta) {
                            //Eliminar
                           // var vmensaje = 'Desea eliminar la Solicitud ' + row.NROSOLICITUD + ' ?';
                            //var vconfirm = 'confirm';
                           // var vtop = 'top';
                           // var vtrue = 'true';
                            //Procesar
                          //  var vmensaje2 = 'Al enviar la solicitud sera atendido por el área correspondiente, desea Enviar la Solicitud ' + row.NROSOLICITUD + ' ahora ?';

                            var vSele = '<div class="btn-toolbar">';

                            if (row.ID_CONDICION == '2')
                            {
                                vAccion = 'V';
                                vSele += '<button class="btn btn-xs" onclick="FnVerSolicitud(\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.FECHA + '\',\'' + row.DOCADMINISTRADO + '\',\'' + row.ADMINISTRADO + '\',\'' + row.TXTASUNTO + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.TXTFOLIOS + '\',\'' + row.NRO_DOCADJ + '\',\'' + vAccion + '\',\'' + row.CORREO + '\');"><i class="icon-eye-open" style="color:NAVY" data-toggle="tooltip" data-placement="top" title="Ver - Recepcionar"></i></button>';
                            }
                            else
                            {

                                vAccion = 'F';
                                vSele += '<button class="btn btn-xs" onclick="FnVerSolicitud(\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.FECHA + '\',\'' + row.DOCADMINISTRADO + '\',\'' + row.ADMINISTRADO + '\',\'' + row.TXTASUNTO + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.TXTFOLIOS + '\',\'' + row.NRO_DOCADJ + '\',\'' + vAccion + '\',\'' + row.CORREO + '\');"><i class="icon-eye-open" style="color:NAVY" data-toggle="tooltip" data-placement="top" title="Ver - Recepcionar"></i></button>';
                                vSele += '<button class="btn btn-xs" onclick="FnObservacion(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD +  '\',\'' + row.ID_CONDICION  + '\');"><i class="icon-calendar" style="color:NAVY" data-toggle="tooltip" data-placement="top" title="Generar Observación"></i></button>';
                                vSele += '<button class="btn btn-xs" onclick="FnGenerarPago(\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.ASUN_chDESASU + '\',\'' + row.TXTASUNTO + '\',\'' + row.MONTO + '\',\'' + row.FEPAGO + '\',\'' + row.ID_CUENTA + '\',\'' + row.FLAGPAGO + '\',\'' + row.ID_CONDICION + '\');"><i class="icon-usd" data-toggle="tooltip" data-placement="top" title="Generar Pago"></i></button>';
                                if (row.ID_CONDICION != '5') {
                                    // vSele += '<button class="btn btn-xs" onclick="FnGenerarExpediente(\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.IDTIPOSOLICITUD + '\',\'' + row.TXTFOLIOS + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.IDTUPA + '\',\'' + row.OFIC_P_inCODOFI + '\',\'' + row.ASUN_chDESASU + '\',\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.ADMINISTRADO + '\',\'' + row.FIJO + '\',\'' + row.CORREO + '\',\'' + row.CELULAR + '\',\'' + row.NRODOCIDE + '\',\'' + row.TXTASUNTO + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.DIRECCION + '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Generar Expediente"></i></button>';
                                    vSele += '<button class="btn btn-xs" onclick="FnGenerarExpediente(\'' + row.IDREGISTRO + '\',\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.IDTIPOSOLICITUD + '\',\'' + row.TXTFOLIOS + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.IDTUPA + '\',\'' + row.OFIC_P_inCODOFI + '\',\'' + row.ASUN_chDESASU + '\',\'' + row.IDSOLICITUD + '\',\'' + row.NROSOLICITUD + '\',\'' + row.ADMINISTRADO + '\',\'' + row.FIJO + '\',\'' + row.CORREO + '\',\'' + row.CELULAR + '\',\'' + row.NRODOCIDE + '\',\'' + row.TXTASUNTO + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.PERS_P_inCODPER + '\',\'' + row.DIRE_P_inCODDIR + '\',\'' + row.DIRECCION + '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Generar Expediente"></i></button>';
                                }

                                
                            }

                            vSele += '<button class="btn btn-xs" onclick="FnFormatoConstancia(\'' + row.IDSOLICITUD + '\');"><i class="icon-file-text" data-toggle="tooltip" data-placement="top" title="Imprimir Constancia."></i></button>';
                           // vSele += '<button class="btn btn-xs" onclick="FnAdjuntar(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\');"><i class="icon-paper-clip" style="color:GREEN" data-toggle="tooltip" data-placement="top" title="Documentos Adjuntos"></i></button>';
                            //vSele += '<button class="btn btn-xs" onclick="FnConfirmaProcSolicitud(\'' + vmensaje2 + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Enviar Solicitud"></i></button>';
                 
                            return vSele;

                        }
                    },

                ],
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true

        }

    );
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtDocSimple').DataTable();

}

document.getElementById('BtnBuscar').onclick = function () {
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtDocSimple').DataTable();

    var TipValor = $('input[name="OptionRadio"]:checked').val();

    oTable.columns(0).search(TipValor);
    oTable.columns(1).search($('#TxtBuscar').val());
    oTable.columns(2).search('');
    ////hit search on server
    oTable.draw();

}


function FnFormatoConstancia(vIdSolicitud) {
    window.open("../Solicitud/ImprimirConstancia/?vId=" + vIdSolicitud, '_blank');
}

//visualizar
function FnVerSolicitud(vIdSolicitud, vNroSoli, vFecEnvio, vDocIde, vAdministrado, vAsunto, vObserva, vFolio, vCDocAdj, vAccion, vCorreo) {
    alert("zsas");
    document.getElementById("HIdSolicitud").value = vIdSolicitud;
    document.getElementById("TxtNroSolicitud").value = vNroSoli;
    document.getElementById("TxtFecEnvio").value = vFecEnvio;
    document.getElementById("TxtDocIdentidad").value = vDocIde;
    document.getElementById("TxtAdministrado").value = vAdministrado;
    document.getElementById("TxtAsunto").value = vAsunto;
    document.getElementById("TxtObservacion").value = vObserva;
    document.getElementById("TxtFolios").value = vFolio;
    document.getElementById("TxtCorreo").value = vCorreo;
    document.getElementById("SArcAdj").innerHTML = vCDocAdj;

    $('.nav-tabs a[href="#tab_1_1"]').tab('show');
    //alert(vAccion);
    if (vAccion == 'F')
    {
        document.getElementById("DivBtnAceptar").style.display = 'none';
      
      
    } else {
        document.getElementById("DivBtnAceptar").style.display = 'inline';
    }



    //alert(vIdSolicitud);
    // document.getElementById("DivArchivo").style.display = 'inline';
    //document.getElementById("DivArchivo").style.display = 'none';

    FnListaArcAdjunto(vIdSolicitud)

    $('#ModVerSolicitud').modal('show');

}

function FnListaArcAdjunto(vIdSolicitud) {

    $("#TbArcAdjunto").dataTable().fnDestroy()

    $('#TbArcAdjunto').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarAdjArchivo",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    }

                ]

        }

    );

}

//fIN visualizar

//recepcionar ds

document.getElementById('BtnAceptar').onclick = function () {
    var vIdSolicitud = document.getElementById("HIdSolicitud").value;

    var vmensaje = 'Desea Aceptar la Solicitud para continuar ?';
    var vconfirm = 'confirm';
    var vtop = 'top';
    var vtrue = 'true';
    FnConfirmaRecpSolicitud(vmensaje, vconfirm, vtop, vtrue, vIdSolicitud);
}



function FnConfirmaRecpSolicitud(text, type, layout, modal, CodSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnRecpSolicitud(CodSolicitud);
                c.close();
                $('#ModVerSolicitud').modal('hide');
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnRecpSolicitud(vIdSolicitud) {

    $.ajax({
        type: 'POST',
        url: '../Solicitud/RecepcionSolicitud',
        data: { StrSolicitud: vIdSolicitud },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable = $('#DtDocSimple').DataTable();
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });


}

//
//procesar

document.getElementById('BtnProcesarDatos').onclick = function () {
    
    var vNRODOC = document.getElementById("HIdNRODOCP").value;
    var vIDREGISTRO = document.getElementById("HIDREGISTROP").value;
    var vDIRECCION = document.getElementById("TxtDireccionP").value;
    var vCODPER = document.getElementById("HCODPER").value;
    var vCODDIR = document.getElementById("HCODDIR").value;


    var data = new FormData();
    data.append("iIDREGISTRO", vIDREGISTRO);
    data.append("iNRODOC", vNRODOC);
    data.append("iDIRECCION", vDIRECCION);
    data.append("iCODPER", vCODPER);
    data.append("iCODDIR", vCODDIR);

    $.ajax({
        url: "../Solicitud/RegistroDatos",
        cache: false,
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            //$.LoadingOverlay("show");
        },
        complete: function () {
            //$.LoadingOverlay("hide");
        },
        success: function (Data) {
            //$.LoadingOverlay("hide");

            if (Data.proValor != "0") {
                document.getElementById('HIdP').value = Data.proValor;

                FnListaDocSimple("2");
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //$.LoadingOverlay("hide");
            //alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}












//function FnGenerarExpediente(vPERS_P_inCODPER, vDIRE_P_inCODDIR, vTIPO_P_inCODTIP, vFOLIO, vOBS, vIDASUNTO, vOFICINA, vASUNTO, vIDSOLICITUD, vNROSOLICITUD, vADMINISTRADO, vFIJO, vCORREO, vCELULAR, vNRODOCIDE, vPROCEDIMIENTO, vOBSERVACION) {
function FnGenerarExpediente(vIDREGISTRO , vPERS_P_inCODPER, vDIRE_P_inCODDIR, vTIPO_P_inCODTIP, vFOLIO, vOBS, vIDASUNTO, vOFICINA, vASUNTO, vIDSOLICITUD, vNROSOLICITUD, vADMINISTRADO, vFIJO, vCORREO, vCELULAR, vNRODOCIDE, vPROCEDIMIENTO, vOBSERVACION, vPERS_P_inCODPER, vDIRE_P_inCODDIR, vDIRECCION) {
    if (vPERS_P_inCODPER == "0" || vDIRE_P_inCODDIR == "0") {
    
        document.getElementById("TxtAdministradoP").value = vADMINISTRADO;
        document.getElementById("TxtDireccionP").value = vDIRECCION;

        document.getElementById("HIdNRODOCP").value = vNRODOCIDE;
        document.getElementById("HIdSolicitudP").value = vIDSOLICITUD;
        document.getElementById("HIDREGISTROP").value = vIDREGISTRO;

        document.getElementById("HCODPER").value = vPERS_P_inCODPER;
        document.getElementById("HCODDIR").value = vDIRE_P_inCODDIR;


        $('#ModDatosPersona').modal('show');
    } else {
        document.getElementById("TxtCorrelaT").value = vNROSOLICITUD;

        document.getElementById("HIdSolicitudT").value = vIDSOLICITUD;
        document.getElementById("TxtAsuntoT").value = vPROCEDIMIENTO;
        document.getElementById("HIdPersonaT").value = vPERS_P_inCODPER;
        document.getElementById("HIdDireccionT").value = vDIRE_P_inCODDIR;
        document.getElementById("HIdTipdocT").value = vTIPO_P_inCODTIP;
        document.getElementById("HFolioT").value = vFOLIO;
        document.getElementById("HIdTupaT").value = vIDASUNTO;
        document.getElementById("HIdOficinaT").value = vOFICINA;
        document.getElementById("HAdministradoT").value = vADMINISTRADO;
        document.getElementById("HFIJOT").value = vFIJO;
        document.getElementById("HCORREOT").value = vCORREO;
        document.getElementById("HCELULART").value = vCELULAR;
        document.getElementById("HNRODOCIDET").value = vNRODOCIDE;

        document.getElementById("TxtProcedimientoT").value = vASUNTO;
        document.getElementById("HOBSSOLIT").value = vOBSERVACION;

        document.forms["FrmGenerarSid"].reset();


        $('#ModAdjuntarSid').modal('show');
    }

}

document.getElementById('BtnGenerarTrans').onclick = function () {



    var vOBS = document.getElementById("TxtObservacionT").value;
    var vIDSOLICITUD = document.getElementById("HIdSolicitudT").value;
    var vASUNTO = document.getElementById("TxtAsuntoT").value;
    var vPROCEDIMIENTO = document.getElementById("TxtProcedimientoT").value;
    var vPERS_P_inCODPER = document.getElementById("HIdPersonaT").value;
    var vDIRE_P_inCODDIR = document.getElementById("HIdDireccionT").value;
    var vTIPO_P_inCODTIP = document.getElementById("HIdTipdocT").value;
    var vFOLIO = document.getElementById("HFolioT").value;
    var vIDASUNTO = document.getElementById("HIdTupaT").value;
    var vOFICINA = document.getElementById("HIdOficinaT").value;
    var vADMINISTRADO = document.getElementById("HAdministradoT").value;
    var vFIJO = document.getElementById("HFIJOT").value;
    var vCORREO = document.getElementById("HCORREOT").value;
    var vCELULAR = document.getElementById("HCELULART").value;
    var vNRODOCIDE = document.getElementById("HNRODOCIDET").value;
    var vOBSSOLI = document.getElementById("HOBSSOLIT").value;


    var data = new FormData();
    data.append("iPERS_P_inCODPER", vPERS_P_inCODPER);
    data.append("iDIRE_P_inCODDIR", vDIRE_P_inCODDIR);
    data.append("iTIPO_P_inCODTIP", vTIPO_P_inCODTIP);
    data.append("iFOLIO", vFOLIO);
    data.append("iOBS", vOBS);
    data.append("iIDASUNTO", vIDASUNTO);
    data.append("iOFICINA", vOFICINA);
    data.append("iASUNTO", vASUNTO);
    data.append("iIDSOLICITUD", vIDSOLICITUD);
    data.append("iADMINISTRADO", vADMINISTRADO);
    data.append("iFIJO", vFIJO);
    data.append("iCORREO", vCORREO);
    data.append("iCELULAR", vCELULAR);
    data.append("iNRODOCIDE", vNRODOCIDE);
    data.append("iPROCEDIMIENTO", vPROCEDIMIENTO);
    data.append("iOBSSOLI", vOBSSOLI);


    $.ajax({
        url: "../Solicitud/ProcesarSID",
        cache: false,
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            //$.LoadingOverlay("show");
        },
        complete: function () {
            //$.LoadingOverlay("hide");
        },
        success: function (Data) {
            //$.LoadingOverlay("hide");

            if (Data.proValor != "0") {
                document.getElementById('HIdT').value = Data.proValor;
  
                FnListaDocSimple("2");
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //$.LoadingOverlay("hide");
            //alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });


}


document.getElementById('BtnConfirmarPago').onclick = function () {
    var IdSolicitud = document.getElementById("HIdSolicitud1").value;
    FnEnviarPago(IdSolicitud);
}

//---------------------------------------------------

function FnEnviarPago(vIdSolicitud) {



    $.ajax({
        type: 'POST',
        url: '../Pagos/ActualizarEstPago',
        data: { strIdSolicitud: vIdSolicitud, strOpc: "3" },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });



}

function FnGenerarPago(vIDSOLICITUD, vNROSOLICITUD, vPROCEDIMIENTO, vASUNTO, vMONTO, vFEMSION, vIDCUENTA, vESTPAGO, vESTADO) {
    document.getElementById("BtnGenerarPago").disabled = false;
    document.getElementById("BtnConfirmarPago").disabled = false;

    if (vESTPAGO == "2") {
        document.getElementById("BtnGenerarPago").disabled = true;
    }
    if (vESTPAGO == "1") {
        document.getElementById("BtnGenerarPago").disabled = false;
    }
    if (vESTPAGO == "3") {
        document.getElementById("BtnGenerarPago").disabled = true;
        document.getElementById("BtnConfirmarPago").disabled = true;
    }
    if (vESTADO == "5") {
        document.getElementById("BtnGenerarPago").disabled = true;
        document.getElementById("BtnConfirmarPago").disabled = true;
    }
    document.getElementById("TxtCorrela1").value = vNROSOLICITUD;
    document.getElementById("HIdSolicitud1").value = vIDSOLICITUD;
    document.getElementById("TxtAsunto1").value = vPROCEDIMIENTO;
    document.getElementById("TxtProcedimiento1").value = vASUNTO;

    document.getElementById("TxtFechaEmi").value = vFEMSION;

    document.getElementById("TxtMonto").value = vMONTO;
    document.getElementById("ddlCuenta").value = vIDCUENTA;

    FnListaArcAdjuntoPago(vIDSOLICITUD);
    //document.forms["FrmGenerarPago"].reset();
    $('#ModPago').modal('show');

}

document.getElementById('BtnGenerarPago').onclick = function () {
    
    var vIDSOLICITUD = document.getElementById("HIdSolicitud1").value;
    var vFECHAEMI = document.getElementById("TxtFechaEmi").value;
    var vMONTO = document.getElementById("TxtMonto").value;
    var vIDCUENTA = document.getElementById("ddlCuenta").value;


    var data = new FormData();
    data.append("iIDSOLICITUD", vIDSOLICITUD);
    data.append("iFECHAEMI", vFECHAEMI);
    data.append("iMONTO", vMONTO);
    data.append("iID_CUENTA", vIDCUENTA);

    $.ajax({
        url: "../Pagos/GenerarPago",
        cache: false,
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function () {
            //$.LoadingOverlay("show");
        },
        complete: function () {
            //$.LoadingOverlay("hide");
        },
        success: function (Data) {
            //$.LoadingOverlay("hide");

            if (Data.proValor != "0") {
                document.getElementById('HId1').value = Data.proValor;
                var IdRegistro = 2;
                FnListaDocSimple(IdRegistro);
                //FnListaSolicitud(IdRegistro);
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //$.LoadingOverlay("hide");
            //alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function FnListaArcAdjuntoPago(vIdSolicitud) {

    $("#DtPagos").dataTable().fnDestroy()

    $('#DtPagos').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Pagos/BuscarAdjArchivoPago",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    }

                    //{
                    //    orderable: false, name: "n4", width: '5%',
                    //    mRender: function (data, type, row) {
                    //        var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.DESCARCHIVO + ' ?';
                    //        var vconfirm = 'confirm';
                    //        var vtop = 'top';
                    //        var vtrue = 'true';

                    //        var vSele = '<span class="btn-group"> <a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="Delete"  onclick="FnConfirmaEliArcAdjuntoPago(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.CODARCHIVO + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash"></i></a> </span>';


                    //        return vSele;
                    //    }
                    //}

                ]

        }

    );

}





//function FnAdjuntar(vIdSolicitud, vAsunto, vCorrela) {
 
//    document.getElementById("HIdSolicitud").value = vIdSolicitud;
//    document.getElementById("TxtAsunto").value = vAsunto;
//    document.getElementById("TxtCorrela").value = vCorrela;

//    // document.getElementById("DivArchivo").style.display = 'inline';

//    document.getElementById("DivArchivo").style.display = 'none';

//    document.forms["FrmAdjuntar"].reset();
//    FnListaArcAdjunto(vIdSolicitud)

//    $('#ModAdjuntar').modal('show');

//}

//function FnListaArcAdjunto(vIdSolicitud) {

//    $("#TbArcAdjunto").dataTable().fnDestroy()

//    $('#TbArcAdjunto').dataTable(
//        {
//            processing: true, // control the processing indicator.
//            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
//            info: false,   // control table information display field
//            stateSave: false,  //si esta activo hay problemas conpaginacion
//            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
//            pageLength: 5,
//            searching: false,
//            dom: 'Bfrtip',
//            destroy: true,
//            ajax:
//                {
//                    "url": "../Solicitud/BuscarAdjArchivo",
//                    "type": "POST",
//                    "datatype": "json",
//                    "data": { IdSolicitud: vIdSolicitud }
//                },
//            autoWidth: false,
//            columns:
//                [
//                    { data: 'CORRELA', name: "n2", width: '1%' },
//                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
//                    {
//                        orderable: false, name: "n3", width: '4%',
//                        mRender: function (data, type, row) {

//                            var vSele = "";
//                            var icono = "../Imagenes/Iconos/pdf.jpg";
//                            if (row.TXTURLARCHIVO != "") {
//                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
//                            }
//                            return vSele;

//                        }
//                    },

//                    {
//                        orderable: false, name: "n4", width: '5%',
//                        mRender: function (data, type, row) {
//                            return '';
//                        }
//                    }

//                ]

//        }

//    );

//}

//fin adjuntar archivo








///observaciones

function FnObservacion(vIdSolicitud, vAsunto, vCorrela, vEstado)
{
    document.getElementById("HIdSolicitudO").value = vIdSolicitud;
    document.getElementById("TxtAsuntoO").value = vAsunto;
    document.getElementById("TxtCorrelaO").value = vCorrela;

    if (vEstado == "5") {
        document.getElementById("BtnObservacion").disabled = true;
    } else {
        document.getElementById("BtnObservacion").disabled = false;
    }

    document.getElementById("DivObserva").style.display = 'inline';

    document.forms["FrmObservacion"].reset();
    FnListaObservacion(vIdSolicitud)

    $('#ModObservacion').modal('show');

}

function FnListaObservacion(vIdSolicitud) {

    $("#TbListObservacion").dataTable().fnDestroy()

    $('#TbListObservacion').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarObservacion",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'TXTOBSERVACION', name: "n2", width: '90%' },
                 
                    {
                        orderable: false, name: "n4", width: '9%',
                        mRender: function (data, type, row) {
                            var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.TXTOBSERVACION + ' ?';
                            var vconfirm = 'confirm';
                            var vtop = 'top';
                            var vtrue = 'true';

                            var vSele = '<div class="btn-toolbar">';
                            vSele += '<button class="btn btn-xs" onclick="FnRespuestaObs(\'' + row.IDSOLICITUD + '\',\'' + row.IDOBSERVACION + '\',\'' + row.TXTOBSERVACION + '\',\'' + row.CORRELA + '\');"><i class="icon-list-ul" style="color:GREEN" data-toggle="tooltip" data-placement="top" title="Revisar Observación"></i></button>';
                            if (row.ID_CONDICION != '5') {
                                vSele += '<button class="btn btn-xs" onclick="FnConfirmaEliObservacion(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDOBSERVACION + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Observación"></i></button>';
                            }
                            vSele += '</div>';
                            return vSele;
                        }
                    }

                ]

        }

    );

}

document.getElementById('BtnObservacion').onclick = function ()
{
    var IdSolicitud = document.getElementById("HIdSolicitudO").value;
    var vDescObs = document.getElementById("TxtDescripO").value;

    if (vDescObs.length == 0 || IdSolicitud.length == 0) {
        alert("Ingrese la Descripcion de la Observacion");
        return false;
    }

    var data = new FormData();
    data.append("vTipo", "1");
    data.append("vIdSolicitud", IdSolicitud);
    data.append("vDescrip", vDescObs);

    $.ajax({
        url: "../Solicitud/RegistraObservacion",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            document.forms["FrmObservacion"].reset();
            FnListaObservacion(IdSolicitud);
        },
        error: function (er) {
            alert(er.message);
        }

    });




}

function FnConfirmaEliObservacion(text, type, layout, modal, CodObs, IdSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEliObservacion(CodObs, IdSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliObservacion(vIdObs, vIdSolicitud) {

    oTable = $('#TbArcAdjunto').DataTable();

    $.ajax({
        type: 'POST',
        url: '../Solicitud/EliminarObservacion',
        data: { strIdObservacion: vIdObs},
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

    FnListaObservacion(vIdSolicitud);

}


///fin observaciones


///Respuesta

///respuestas

function FnRespuestaObs(vIdSolicitud, vIdObservacion, vObservacion, vCorrela) {

    document.getElementById("HIdSolicitudR").value = vIdSolicitud;
    document.getElementById("HIdObservacionR").value = vIdObservacion;
    document.getElementById("TxtCorrelaR").value = vCorrela;
    document.getElementById("TxtObservacion").value = vObservacion;

    document.getElementById("DivRespuesta").style.display = 'none';

    document.forms["FrmRespuesta"].reset();

    FnListaRespuestaObs(vIdSolicitud, vIdObservacion)

    $('#ModRespuestaObs').modal('show');

}

function FnListaRespuestaObs(vIdSolicitud, vIdObservacion) {

    $("#TbRespuestaObs").dataTable().fnDestroy()

    $('#TbRespuestaObs').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarRespuestaObs",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud, IdObservacion: vIdObservacion }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'TXTRESPUESTA', name: "n2", width: '90%' },
                      {
                          orderable: false, name: "n3", width: '4%',
                          mRender: function (data, type, row) {

                              var vSele = "";
                              var icono = "../Imagenes/Iconos/pdf.jpg";
                              if (row.TXTURLARCHIVO != "") {
                                  vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                              }
                              return vSele;

                          }
                      },

                   {
                       orderable: false, name: "n4", width: '9%',
                       mRender: function (data, type, row) {
                           var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.TXTRESPUESTA + ' ?';
                           var vconfirm = 'confirm';
                           var vtop = 'top';
                           var vtrue = 'true';

                           var vSele = '<div class="btn-toolbar">';
                           if (row.ID_CONDICION != '5') {
                               vSele += '<button class="btn btn-xs" onclick="FnConfirmaEliRespuesta(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDOBSERVACION + '\',\'' + row.IDRESPUESTAOBS + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Respuesta"></i></button>';
                           }
                           vSele += '</div>';
                           return vSele;
                       }
                   }

                ]

        }

    );

}


///fin observaciones