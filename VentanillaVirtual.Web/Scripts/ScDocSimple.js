﻿window.onload = function ()
{

    FnInicio("Inicio");

};

////////////***configuracion Inicio
function FnInicio(activar)
{

    if (activar == "Inicio") {
        document.getElementById("Div01").style.display = 'inline';
        document.getElementById("Div02").style.display = 'none';
    }
    else {
        document.getElementById("Div01").style.display = 'none';
        document.getElementById("Div02").style.display = 'inline';
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }

}

function FnValidarFormulario()
{
    var info = "";
    var vSolicitud = document.getElementById('txtSolicitud').value;
    var vObs = document.getElementById('txtObs').value;
    var vFolio = document.getElementById('txtFolio').value;
    var vHCodRegi = document.getElementById('HCodRegi').value;


    ////Test campo obligatorio
    if (vHCodRegi == null || vHCodRegi.length == 0) {
        info = "No existe el usuario de envio de datos, vuelve a iniciar sesión !";
    }
    if (vSolicitud == null || vSolicitud.length == 0) {
        info = "Ingrese el Asunto de lo solicitado !";
    }

    else if (vFolio == null || vFolio.length == 0) {
        info = "Ingrese el número de folios !";
    }

    return info;

}

function FnConfirmaGuardar()
{

    var vValidado = FnValidarFormulario();

    if (vValidado != "")
    {
        alert(vValidado);
        return false;
    }
    else
    {
        var type = "confirm";

        noty({
            text: "Desea Guardar el Registro ?",
            type: "confirm",
            layout: "top",
            timeout: 2000,
            modal: "true",
            buttons: (type != "confirm") ? false : [{
                addClass: "btn btn-primary",
                text: "Aceptar",
                onClick: function (c) {
                    FnEnviarDatos();
                    c.close();
                }
            }, {
                addClass: "btn btn-danger",
                text: "Cancelar",
                onClick: function (c) {
                    c.close();

                }
            }]
        });

    }

    return false;

}

function FnEnviarDatos()
{

    var vSolicitud = document.getElementById('txtSolicitud').value;
    var vObs = document.getElementById('txtObs').value;
    var vFolio = document.getElementById('txtFolio').value;
    var vHCodRegi = document.getElementById('HCodRegi').value;
   
    $.ajax({

        // contentType: "application/json; charset=utf-8",
        url: '../DocSimple/RegistraDocSimple',
        cache: false,
        type: 'POST',
        dataType: 'json',
        //data: decodeURIComponent($.param(formData)),
        data: { StrSolicitud: vSolicitud, StrObs: vObs, StrFolio: vFolio,  StrHCodRegi: vHCodRegi },
        //  cache: false,
        beforeSend: function () {
          //  $.LoadingOverlay("show");
        },
        complete: function () {
          //  $.LoadingOverlay("hide");
        },
        success: function (Data) {
          //  $.LoadingOverlay("hide");

            if (Data.proValor != "0")
            {
                document.getElementById('H2NroSolicitud').innerHTML = "Nro de Solicitud :  <strong>" + Data.proValor + "</strong>";
                FnInicio("Fin");
            }
            else {
                alert(Data.proMensaje);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
           // $.LoadingOverlay("hide");
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }

    });

}


