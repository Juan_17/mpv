﻿window.onload = function ()
{

    ValidarTipoDoc();
};

function ValidarTipoDoc()
{
    var vTipoDoc = document.getElementById('ddlTipoDoc').value;

    document.getElementById('TxtNroDoc').value = "";
    document.getElementById('TxtApePatP').value = "";
    document.getElementById('TxtApeMatP').value = "";
    document.getElementById('TxtNombreP').value = "";
    document.getElementById('TxtApePatP').readOnly = true;
    document.getElementById('TxtApeMatP').readOnly = true;
    document.getElementById('TxtNombreP').readOnly = true;
    document.getElementById('btnBusquedad').disabled = false;


    if (vTipoDoc == "01")
    {
        document.getElementById("DDni").style.display = 'inline';
    }
    else if (vTipoDoc == "02") {
        document.getElementById("DDni").style.display = 'none';
    }
    else
    {
        document.getElementById("DDni").style.display = 'inline';
        document.getElementById('TxtApePatP').readOnly = false;
        document.getElementById('TxtApeMatP').readOnly = false;
        document.getElementById('TxtNombreP').readOnly = false;
        document.getElementById('btnBusquedad').disabled = true;
    }
}

function FnBusquedad()
{
    var stTipoDoc = document.getElementById('ddlTipoDoc').value;
    var stNroDoc = document.getElementById('TxtNroDoc').value;
    var stTipo = "3";

    if (stTipoDoc == "01")
    {
        stTipo = "1";
        if (stNroDoc.length != 8)
        {
            alert("Ingrese un numero de DNI de 8 Numeros");
            return;
        }
    }

    if (stTipoDoc == "02")
    {
        stTipo = "2";
        if (stNroDoc.length != 11) {
            alert("Ingrese un numero de RUC de 11 Numeros");
            return;
        }
    }

            $.ajax({
                type: "POST",
                url: "../Login/ConsultaExt",
                data: { strNum: stNroDoc, strTipo: stTipo },
                cache: false,
                beforeSend: function () {
                    $.LoadingOverlay("show");
                },
                complete: function () {
                    $.LoadingOverlay("hide");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.LoadingOverlay("hide");
                    alert("No esta disponible los servicios de Datos");
                   // alert(textStatus + ":2 " + XMLHttpRequest.responseText);
                },
                success: function (data)
                {
                    if (data == null)
                    {
                        alert("No se se encuentra datos para visualizar");
                    }
                    else
                    {
                        if (stTipo == "1")
                        {
                            document.getElementById('TxtApePatP').value = data.proAPPRIMER;
                            document.getElementById('TxtApeMatP').value = data.proAPSEGUNDO;
                            document.getElementById('TxtNombreP').value = data.proPRENOMBRES;
                        }

                        if (stTipo == "2")
                        {
                            if (data.proDDP_NOMBRE == "")
                            {
                                alert("No se encuentra datos para visualizar con el RUC ingresado");
                            }
                            document.getElementById('TxtNombreP').value = data.proDDP_NOMBRE;
                        }
                        FnBuscarPersona();

                    }
                    

                    $.LoadingOverlay("hide");

                }
            });

}

function validarFormulario()
{
    var info = "";


        var sAux = "";
        var frm = document.getElementById("FrmDatos");
        for (i = 0; i < frm.elements.length; i++)
        {
            if (frm.elements[i].type == "text" || frm.elements[i].type == "select-one")
            {
                if (frm.elements[i].value=="" || frm.elements[i].value=="-1")
                {
                    if (document.getElementById('ddlTipoDoc').value == "02" && (frm.elements[i].name == "TxtApePatP" || frm.elements[i].name == "TxtApeMatP"))
                    {

                    }
                    else
                    {
                        if (frm.elements[i].name == "ddlOp1" || frm.elements[i].name == "ddlOp2" || frm.elements[i].name == "ddlOp3" || frm.elements[i].name == "txtOp1" || frm.elements[i].name == "txtOp2" || frm.elements[i].name == "txtOp3") {

                        } else {
                            sAux += "Falta registrar : " + frm.elements[i].title + "\n";
                        }
                       
                    }
                     
                }

                //sAux += "NOMBRE: " + frm.elements[i].name + " ";
                // sAux += "TIPO :  " + frm.elements[i].type + " ";
                //sAux += "title :  " + frm.elements[i].title + " ";
               // sAux += "VALOR: " + frm.elements[i].value + "\n";
            }
           
        }

        var sCorreo1 = document.getElementById('txtEmailA').value;
        var sCorreo2 = document.getElementById('txtEmailB').value;

        if (sCorreo1 != sCorreo2)
        {
            sAux += "Los correos ingresados no son iguales" + "\n";
        }

        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(sCorreo1))
        { }
        else
        {
            sAux += "La dirección de email es incorrecta!." + "\n";

        }


        if (sAux == "")
        {
            return true
        }
        else
        {
            alert(sAux);
            return false
        }
        
 





}

function FnEnviarDatos()
{
   
 

        if (validarFormulario()) {

            var formData = $('#FrmDatos').serializeObject();

            $.ajax({
                // contentType: "application/json; charset=utf-8",
                url: '../Login/EnviarDatos',
                cache: false,
                type: 'POST',
                dataType: 'json',
                data: decodeURIComponent($.param(formData)),
                //  cache: false,
                beforeSend: function () {
                    $.LoadingOverlay("show");
                },
                complete: function () {
                    $.LoadingOverlay("hide");
                },
                success: function (Data) {
                    alert(Data.TXTMENSAJE);
                    if (Data.IDREGISTRO > 0) {
                        location.href = '../Login/';
                    }
                    else {
                        document.getElementById("FrmDatos").reset();;
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.LoadingOverlay("hide");
                    alert(textStatus + ": " + XMLHttpRequest.responseText);
                }
            });

        }
    

}



function FnBuscarPersona() {

        var dato = 0;

        var formData = $('#FrmDatos').serializeObject();

        $.ajax({
            // contentType: "application/json; charset=utf-8",
            url: '../Login/BuscarPersona',
            cache: false,
            type: 'POST',
            dataType: 'json',
            data: decodeURIComponent($.param(formData)),
            //  cache: false,
            beforeSend: function () {
                //$.LoadingOverlay("show");
            },
            complete: function () {
                //$.LoadingOverlay("hide");
            },
            success: function (Data) {
         
                if (Data.IDREGISTRO ==0) {
                    //dato = "2";
                    document.getElementById('BtnGuardar').disabled = false;


                } else {
                    alert(Data.TXTMENSAJE);
                    document.getElementById('BtnGuardar').disabled = true;
                    //dato = "1";

                }


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //$.LoadingOverlay("hide");
                alert(textStatus + ": " + XMLHttpRequest.responseText);
            }
        });


        //if (dato == "2") {
        //    return true
        //}
        //else {
        //    alert(sAux);
        //    return false
        //}


}

//envio formulario
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

