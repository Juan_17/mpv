﻿window.onload = function ()
{
    var IdRegistro = document.getElementById("HCodRegi").value;
    FnListaSolicitud(IdRegistro);
};

function FnListaSolicitud(StrCodigo) {
    var vCodigo = StrCodigo;
    //alert(vCodigo);
    $("#DtSolicitudes").dataTable().fnDestroy()

    $('#DtSolicitudes').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            dom: '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
            destroy: true,
            lengthChange: false,
            ajax:
                {
                    "url": "../Solicitud/BuscarSolicitud",
                    "type": "POST",
                    "datatype": "json",
                    "data": { strCodigo: vCodigo }
                },

            columns:
                [
                      {
                          orderable: false, name: "n2", autoWidth: false, width: "1%", "render": function (data, type, row, meta) {
                              if (row.IDTIPOSOLICITUD == "2") {
                                  return '<center><i class="icon-copy"  data-toggle="tooltip" data-placement="top" title="Documento Simple" ></i></center>';
                              }
                              else {
                                  return '<center><i class="icon-paste"  data-toggle="tooltip" data-placement="top" title="Expediente" ></i></center>';
                              }
                          }
                      },
                    { data: 'NROSOLICITUD', name: "n1", orderable: true, autoWidth: false, width: "5%" },

                    { data: 'FECHA', name: "n3", orderable: false, autoWidth: false, width: "10%" },
                    { data: 'TXTASUNTO', name: "n4", orderable: true, autoWidth: false, width: "30%" },
                    { data: 'ASUN_chDESASU', name: "n5", orderable: false, autoWidth: false, width: "30%" },
                    { data: 'TXTFOLIOS', name: "n6", orderable: false, autoWidth: false, width: "3%" },


                      {
                          orderable: false, name: "n7", autoWidth: false, width: "5%", "render": function (data, type, row, meta) {
                              if (row.ID_CONDICION == "1") {
                                  return '<center><span class="label label-info">Pendiente</span></center>';
                              }
                              else if (row.ID_CONDICION == "2")
                              {
                                  return '<center><span class="label label-success">Enviado</span></center>';
                              }
                              else {
                                  return '<center>ninguno</center>';
                              }
                          }
                      },



                    {
                        orderable: false, name: "n7", autoWidth: false, width: "10%", "render": function (data, type, row, meta)
                        {
                            //Eliminar
                             var vmensaje = 'Desea eliminar la Solicitud ' + row.NROSOLICITUD + ' ?';
                             var vconfirm = 'confirm';
                             var vtop = 'top';
                             var vtrue = 'true';
                            //Procesar
                             var vmensaje2 = 'Al enviar la solicitud sera atendido por el área correspondiente, desea Enviar la Solicitud ' + row.NROSOLICITUD + ' ahora ?';

                             var vSele = '<div class="btn-toolbar">';
                        
                             

                             if (row.ID_CONDICION == 1)
                             {
                                 vSele += '<button class="btn btn-xs" onclick="FnAdjuntar(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\');"><i class="icon-paper-clip" data-toggle="tooltip" data-placement="top" title="Adjuntar Doc."></i></button>';
                                 vSele += '<button class="btn btn-xs" onclick="FnConfirmaProcSolicitud(\'' + vmensaje2 + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-exchange" data-toggle="tooltip" data-placement="top" title="Enviar Solicitud"></i></button>';
                                 vSele += '<button class="btn btn-xs" onclick="FnConfirmaEliSolicitud(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Solicitud"></i></button>';
                             }
                             else
                             {
                                 vSele += '<button class="btn btn-xs" onclick="FnFormatoConstancia(\'' + row.IDSOLICITUD + '\');"><i class="icon-file-text" data-toggle="tooltip" data-placement="top" title="Imprimir Constancia."></i></button>';
                                 
                             }
                             vSele += '</div>';
                            
                             //if (row.FLAGPAGO == 1)
                             //{
                             //    vSele += '<button class="btn btn-xs" onclick="FnAdjuntarPago(\'' + row.IDSOLICITUD + '\',\'' + row.TXTASUNTO + '\',\'' + row.NROSOLICITUD + '\',\'' + row.MONTO + '\',\'' + row.NROCTA + '\');"><i class="icon-usd" data-toggle="tooltip" data-placement="top" title="Cancelar Tramite"></i></button>';
                             //}
                              return vSele;

                         }
                     },

                ],
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true

        }

    );
    //Apply Custom search on jQuery DataTables here
    oTable = $('#DtSolicitudes').DataTable();

}

function FnConfirmaEliSolicitud(text, type, layout, modal, CodSolicitud)
{
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEliminaSolicitud(CodSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliminaSolicitud(vIdSolicitud)
{

    $.ajax({
        type: 'POST',
        url: '../Solicitud/EliminarSolicitud',
        data: { StrSolicitud: vIdSolicitud },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro)
        {
            oTable = $('#DtSolicitudes').DataTable();
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });


}




function FnConfirmaProcSolicitud(text, type, layout, modal, CodSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEnviarSolicitud(CodSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEnviarSolicitud(vIdSolicitud) {

    $.ajax({
        type: 'POST',
        url: '../Solicitud/EnviarSolicitud',
        data: { StrSolicitud: vIdSolicitud },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable = $('#DtSolicitudes').DataTable();
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });


}

function FnAdjuntarPago(vIdSolicitud, vAsunto, vCorrela, vMonto, vNroCta)
{
      
    document.getElementById("HIdSolicitudPag").value = vIdSolicitud;
    document.getElementById("TxtAsuntoPag").value = vAsunto;
    document.getElementById("TxtCorrelaPag").value = vCorrela;
    document.getElementById("TxtMontoPagar").value = vMonto;
    document.getElementById("TxtNroCuenta").value = vNroCta;


    document.forms["FrmAdjuntarPago"].reset();
    FnListaArcAdjuntoPago(vIdSolicitud)

    $('#ModAdjuntarPago').modal('show');

}

document.getElementById('BtnEnviarPago').onclick = function () {
    var IdSolicitud = document.getElementById("HIdSolicitudPag").value;
    FnEnviarPago(IdSolicitud);
}

//---------------------------------------------------

function FnEnviarPago(vIdSolicitud) {



    $.ajax({
        type: 'POST',
        url: '../Pagos/ActualizarEstPago',
        data: { strIdSolicitud: vIdSolicitud, strOpc:"2" },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

 

}

//----------------------------------------------------








document.getElementById('BtnAdjuntarPago').onclick = function () {

    var IdSolicitud = document.getElementById("HIdSolicitudPag").value;
    var vFechDep = document.getElementById("txtFDeposito").value;


    if (vFechDep.length == 0 || IdSolicitud.length == 0) {
        alert("Ingrese la Fecha de Deposito");
        return false;
    }

    var data = new FormData();
    data.append("vTipo", "1");
    data.append("vIdSolicitud", IdSolicitud);
    data.append("vDescrip", vFechDep);

    var files = $("#UploadArchivoPag").get(0).files;
    if (files.length > 0) {
        data.append("MFiles", files[0]);
    }
    else {
        alert("Seleccione el Archivo a adjuntar al Pago");
        return false;
    }

    var fileName = document.getElementById("UploadArchivoPag").value;

    if (fileName.length > 0) {
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        if (extFile == "jpg" || extFile == "jpge" || extFile == "gif" || extFile == "png" || extFile == "pdf") { } else
        {
            alert("Seleccione el archivo de tipo: jpg,jpge,gif,png,pdf");
            return false;
        }
    }



    $.ajax({
        url: "../Pagos/AdjArchivoPago",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
           
            document.forms["FrmAdjuntarPago"].reset();
            FnListaArcAdjuntoPago(IdSolicitud);
        },
        error: function (er) {
            alert(er.message);
        }

    });




}

function FnListaArcAdjuntoPago(vIdSolicitud) {

    $("#TbArcAdjuntoPag").dataTable().fnDestroy()

    $('#TbArcAdjuntoPag').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Pagos/BuscarAdjArchivoPago",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "") {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    },
                    //{ data: 'TXTURLFOTO', name: "n3", orderable: false, autoWidth: false, width: "20%" },
                    {
                        orderable: false, name: "n4", width: '5%',
                        mRender: function (data, type, row) {
                            var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.DESCARCHIVO + ' ?';
                            var vconfirm = 'confirm';
                            var vtop = 'top';
                            var vtrue = 'true';

                            var vSele = '<span class="btn-group"> <a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="Delete"  onclick="FnConfirmaEliArcAdjuntoPago(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.CODARCHIVO + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash"></i></a> </span>';


                            return vSele;
                        }
                    }

                ]

        }

    );

}

function FnConfirmaEliArcAdjuntoPago(text, type, layout, modal, CodArc, IdSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c) {
                FnEliArcAdjuntoPago(CodArc, IdSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliArcAdjuntoPago(vIdArchivo, vIdSolicitud) {
 
    oTable = $('#TbArcAdjuntoPag').DataTable();

    $.ajax({
        type: 'POST',
        url: '../Pagos/EliminarAdjArchivoPago',
        data: { strIdArc: vIdArchivo },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

    FnListaArcAdjuntoPago(vIdSolicitud);

}

function FnAdjuntar(vIdSolicitud, vAsunto, vCorrela)
{
    document.getElementById("HIdSolicitud").value = vIdSolicitud;
    document.getElementById("TxtAsunto").value = vAsunto;
    document.getElementById("TxtCorrela").value = vCorrela;
    
    document.getElementById("DivArchivo").style.display = 'inline';

    document.forms["FrmAdjuntar"].reset();
    FnListaArcAdjunto(vIdSolicitud)

    $('#ModAdjuntar').modal('show');

}

function FnListaArcAdjunto(vIdSolicitud)
{

    $("#TbArcAdjunto").dataTable().fnDestroy()

    $('#TbArcAdjunto').dataTable(
        {
            processing: true, // control the processing indicator.
            serverSide: true, // recommended to use serverSide when data is more than 10000 rows for performance reasons
            info: false,   // control table information display field
            stateSave: false,  //si esta activo hay problemas conpaginacion
            //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            pageLength: 5,
            searching: false,
            dom: 'Bfrtip',
            destroy: true,
            ajax:
                {
                    "url": "../Solicitud/BuscarAdjArchivo",
                    "type": "POST",
                    "datatype": "json",
                    "data": { IdSolicitud: vIdSolicitud }
                },
            autoWidth: false,
            columns:
                [
                    { data: 'CORRELA', name: "n2", width: '1%' },
                    { data: 'DESCARCHIVO', name: "n2", width: '90%' },
                    {
                        orderable: false, name: "n3", width: '4%',
                        mRender: function (data, type, row) {

                            var vSele = "";
                            var icono = "../Imagenes/Iconos/pdf.jpg";
                            if (row.TXTURLARCHIVO != "")
                            {
                                vSele = '<center><a href="' + row.TXTURLARCHIVO + '" target="_blank"><img src="' + icono + '" alt="" width="20" height="30"></a></center>';
                            }
                            return vSele;

                        }
                    },
                    //{ data: 'TXTURLFOTO', name: "n3", orderable: false, autoWidth: false, width: "20%" },
                    {
                        orderable: false, name: "n4", width: '5%',
                        mRender: function (data, type, row) {
                            var vmensaje = 'Desea eliminar el Registro ' + row.CORRELA + ', con descripción: ' + row.DESCARCHIVO + ' ?';
                            var vconfirm = 'confirm';
                            var vtop = 'top';
                            var vtrue = 'true';

                            var vSele = '<span class="btn-group"> <a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="Delete"  onclick="FnConfirmaEliArcAdjunto(\'' + vmensaje + '\',\'' + vconfirm + '\',\'' + vtop + '\',\'' + vtrue + '\',\'' + row.CODARCHIVO + '\',\'' + row.IDSOLICITUD + '\');"><i class="icon-trash"></i></a> </span>';


                            return vSele;
                        }
                    }

                ]

        }

    );

}




document.getElementById('BtnAdjuntar').onclick = function ()
{

    var IdSolicitud = document.getElementById("HIdSolicitud").value;
    var vDescArc = document.getElementById("TxtDescrip").value;
   

    if (vDescArc.length == 0 || IdSolicitud.length == 0)
    {
        alert("Ingrese la Descripcion del Archivo");
        return false;
    }

    var data = new FormData();
    data.append("vTipo", "1");
    data.append("vIdSolicitud", IdSolicitud);
    data.append("vDescrip", vDescArc);

    var files = $("#UploadArchivo").get(0).files;
    if (files.length > 0)
    {
        data.append("MFiles", files[0]);
    }
    else
    {
        alert("Seleccione el Archivo a adjuntar a la solicitud");
        return false;
    }
   
    var fileName = document.getElementById("UploadArchivo").value;

    if (fileName.length > 0) {
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        if (extFile == "jpg" || extFile == "jpge" || extFile == "gif" || extFile == "png" || extFile == "pdf") { } else
        {
            alert("Seleccione el archivo de tipo: jpg,jpge,gif,png,pdf");
            return false;
        }
    }



    $.ajax({
        url: "../Solicitud/AdjArchivo",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response)
        {
            //var vTxtId = document.getElementById("TxtCodDoc").value;
            document.forms["FrmAdjuntar"].reset();
            FnListaArcAdjunto(IdSolicitud);
        },
        error: function (er)
        {
            alert(er.message);
        }

    });




}








function FnConfirmaEliArcAdjunto(text, type, layout, modal, CodArc, IdSolicitud) {
    noty({
        text: text,
        type: type,
        layout: layout,
        timeout: 2000,
        modal: modal,
        buttons: (type != "confirm") ? false : [{
            addClass: "btn btn-primary",
            text: "Ok",
            onClick: function (c)
            {
                FnEliArcAdjunto(CodArc, IdSolicitud);
                c.close();
            }
        }, {
            addClass: "btn btn-danger",
            text: "Cancelar",
            onClick: function (c) {
                c.close();

            }
        }]
    });
    return false
}

function FnEliArcAdjunto(vIdArchivo, vIdSolicitud)
{

    oTable = $('#TbArcAdjunto').DataTable();

    $.ajax({
        type: 'POST',
        url: '../Solicitud/EliminarAdjArchivo',
        data: { strIdArc: vIdArchivo },
        cache: false,
        datatype: 'json',
        beforeSend: function () {
            // $.LoadingOverlay("show");
        },
        complete: function () {
            // $.LoadingOverlay("hide");
        },
        success: function (Registro) {
            oTable.draw();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

    FnListaArcAdjunto(vIdSolicitud);

}





function FnFormatoConstancia(vIdSolicitud)
{
    window.open("../Solicitud/ImprimirConstancia/?vId=" + vIdSolicitud, '_blank');
}
