﻿using System;
using System.Web;
using System.Web.Mvc;
using VentanillaVirtual.Web.Models;
using VentanillaVirtual.LogicaNegocio;
using VentanillaVirtual.Entidad;
using System.Data;
using System.Collections.Generic;
using Utils;
using System.Linq;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace VentanillaVirtual.Web.Controllers
{
    [CheckAuthorization]
    public class SolicitudController : Controller
    {
        // GET: Solicitud
        public ActionResult Index()
        {
            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja";
            ViewBag.OpHijo01 = "Solicitudes";
            ViewBag.OpHijo02 = "";

            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();
            ViewData["iIdRegi"] = Session["SIdUsuario"];
            return View();
        }


   
        [HttpPost]
        public ActionResult BuscarSolicitud(int strCodigo)
        {


            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            //var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            //var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;


            List<SOLICITUD_Be> list = new List<SOLICITUD_Be>();

            list = fbd_BuscarSolicitud(strCodigo);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        private static List<SOLICITUD_Be> fbd_BuscarSolicitud(int strCodigo)
        {

            List<SOLICITUD_Be> lsGenerado = null;
            try
            {

                SOLICITUD_Br SOLICITUDBr = new SOLICITUD_Br();
                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();

                SOLICITUDBe.IDREGISTRO = strCodigo;

                lsGenerado = SOLICITUDBr.Ps_SOLICITUD(SOLICITUDBe);

                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return lsGenerado;
        }


        [HttpPost]
        public ActionResult EliminarSolicitud(string StrSolicitud)
        {

            try
            {
                //OPC=1 REGISTRAR //OPC=2 EDITAR //OPC=3 ELIMINAR

                RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();

                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();
                SOLICITUDBe.OPC = 0;
                SOLICITUDBe.IDSOLICITUD = (StrSolicitud != "") ? Convert.ToInt32(StrSolicitud.ToString().Trim()) : 0;
                SOLICITUDBe.IDREGISTRO = 0;
                SOLICITUDBe.TXTASUNTO = "";
                SOLICITUDBe.TXTOBSERVACION = "";
                SOLICITUDBe.TXTFOLIOS = "";
                SOLICITUDBe.IDTIPOSOLICITUD = 2;
                //SOLICITUDBe.TIPO_P_inCODTIP = (StrHCodTipdoc != "") ? Convert.ToInt32(StrHCodTipdoc.ToString().Trim()) : 0;
                SOLICITUDBe.IDTUPA = 0;
                SOLICITUDBe.ESTADO = "0";
                RESPUESTABe.proValor = new SOLICITUD_Br().Pid_SOLICITUD(SOLICITUDBe);

                if (RESPUESTABe.proValor != "0")
                {
                    RESPUESTABe.proMensaje = "Se Registro Correctamente la solicitud.";
                }
                else
                {
                    RESPUESTABe.proMensaje = "Error no se registro la solicitud, Vuelve a intentar.";
                }

                return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw;
            }


        }





        [HttpPost]
        public ActionResult BuscarAdjArchivo(string IdSolicitud)
        {

            int recordsTotal = 0;
            List<ARCHIVOADJ_Be> list = new List<ARCHIVOADJ_Be>();
            ARCHIVOADJ_Be ARCHIVOADJBe = new ARCHIVOADJ_Be();
            ARCHIVOADJ_Br ARCHIVOADJBr = new ARCHIVOADJ_Br();

            list = (List<ARCHIVOADJ_Be>)ARCHIVOADJBr.Ps_ARCHIVOADJ(IdSolicitud);
            recordsTotal = list.Count();

            var start = Convert.ToInt16(Request.Form.GetValues("start").FirstOrDefault());
            var length = Convert.ToInt32(Request.Form.GetValues("length").FirstOrDefault());

            var draw = Request.Form.GetValues("draw").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;


            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = recordsTotal,
                recordsTotal = recordsTotal,
                data = data
            }, JsonRequestBehavior.AllowGet);


        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdjArchivo()
        {

            ARCHIVOADJ_Be ARCHIVOADJBe = new ARCHIVOADJ_Be();

            ARCHIVOADJBe.TIPOPROCESO = Request["vTipo"].ToString().Trim();
            ARCHIVOADJBe.IDSOLICITUD = Convert.ToInt32(Request["vIdSolicitud"].ToString().Trim());
            ARCHIVOADJBe.DESCARCHIVO = Request["vDescrip"].ToString().Trim();
            
            var pic = System.Web.HttpContext.Current.Request.Files["MFiles"];
            if (pic != null)
            {
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    // var _ext = Path.GetExtension(pic.FileName);
                    string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName;

                    string CarpetaMes = DateTime.Now.Month.ToString();
                    string CarpetaAnio = DateTime.Now.Year.ToString();
                    //string CarpetaBase =  + "FotoPapeleta\\Multa\\";
                    string CarpetaBase = Server.MapPath("~/File_Doc/Documento/");
                    string BusCarpeta = CarpetaBase + CarpetaAnio;
                    bool exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                       Directory.CreateDirectory(BusCarpeta);
                    }

                    BusCarpeta = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes;
                    exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                        Directory.CreateDirectory(BusCarpeta);
                    }

                    var _comPath = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes + "\\" + adjunto;

                    pic.SaveAs(_comPath.ToString());
                    ARCHIVOADJBe.TXTURLARCHIVO = "../File_Doc/Documento/" + CarpetaAnio + "/" + CarpetaMes + '/' + adjunto;

                }
                else
                {
                    ARCHIVOADJBe.TXTURLARCHIVO = "";
                }
            }
            else
            {
                ARCHIVOADJBe.TXTURLARCHIVO = "";
            }

            string lstNUMDOCUMENTO = fbd_ArchivoAdjPid(ARCHIVOADJBe);
            return Json(lstNUMDOCUMENTO, JsonRequestBehavior.AllowGet);

        }

        private string fbd_ArchivoAdjPid(ARCHIVOADJ_Be ARCHIVOADJBe)
        {

            return new ARCHIVOADJ_Br().Pid_ARCHIVOADJ(ARCHIVOADJBe);

        }


        [HttpPost]
        public ActionResult EliminarAdjArchivo(string strIdArc)
        {

            Int32 iCod = Convert.ToInt32(strIdArc.ToString());

            ARCHIVOADJ_Be ARCHIVOADJBe = new ARCHIVOADJ_Be();

            ARCHIVOADJBe.CODARCHIVO = iCod;
            ARCHIVOADJBe.DESCARCHIVO = "";
            ARCHIVOADJBe.TXTIP = "";
            ARCHIVOADJBe.TIPOPROCESO = "0";

            string iResultado = fbd_ArchivoAdjPid(ARCHIVOADJBe);

            return Json(iResultado, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public ActionResult EnviarSolicitud(string StrSolicitud)
        {

            try
            {

                //OPC=1 REGISTRAR //OPC=2 EDITAR //OPC=3 ELIMINAR

                RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();

                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();
                SOLICITUDBe.OPC = 2;
                SOLICITUDBe.IDSOLICITUD = (StrSolicitud != "") ? Convert.ToInt32(StrSolicitud.ToString().Trim()) : 0;
                SOLICITUDBe.IDREGISTRO = 0;
                SOLICITUDBe.TXTASUNTO = "";
                SOLICITUDBe.TXTOBSERVACION = "";
                SOLICITUDBe.TXTFOLIOS = "";
                SOLICITUDBe.IDTIPOSOLICITUD = 2;
                //SOLICITUDBe.TIPO_P_inCODTIP = (StrHCodTipdoc != "") ? Convert.ToInt32(StrHCodTipdoc.ToString().Trim()) : 0;
                SOLICITUDBe.IDTUPA = 0;
                //SOLICITUDBe.ESTADO = "0";
                RESPUESTABe.proValor = new SOLICITUD_Br().Pid_SOLICITUD(SOLICITUDBe);

                if (RESPUESTABe.proValor != "0")
                {
                    RESPUESTABe.proMensaje = "Se Envio Correctamente la solicitud.";
                }
                else
                {
                    RESPUESTABe.proMensaje = "Error no se envio la solicitud, Vuelve a intentar.";
                }

                return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw;
            }


        }


        [HttpPost]
        public ActionResult RecepcionSolicitud(string StrSolicitud)
        {

            try
            {

                RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();
                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();
                SOLICITUDBe.OPC = 3;
                SOLICITUDBe.IDSOLICITUD = (StrSolicitud != "") ? Convert.ToInt32(StrSolicitud.ToString().Trim()) : 0;
                SOLICITUDBe.IDREGISTRO = 0;
                SOLICITUDBe.TXTASUNTO = "";
                SOLICITUDBe.TXTOBSERVACION = "";
                SOLICITUDBe.TXTFOLIOS = "";
                SOLICITUDBe.IDTIPOSOLICITUD = 2;
                SOLICITUDBe.IDTUPA = 0;
                RESPUESTABe.proValor = new SOLICITUD_Br().Pid_SOLICITUD(SOLICITUDBe);

                if (RESPUESTABe.proValor != "0")
                {
                    RESPUESTABe.proMensaje = "Se Recepcionó Correctamente la solicitud.";
                }
                else
                {
                    RESPUESTABe.proMensaje = "Error no se Recepcionó la solicitud, Vuelve a intentar.";
                }

                return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw;
            }


        }






        public ActionResult ImprimirConstancia(string vId)
        {

            ReportClass rpth = new ReportClass();
            rpth.FileName = Server.MapPath("~/Reportes/RptSolicitud.rpt");
            rpth.Load();
            rpth.SetDataSource(fbd_ImpConstancia(vId));
            Stream stream = rpth.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");

  

        }

        public DataTable fbd_ImpConstancia(string StrSolicitud)
        {
            SOLICITUD_Br SOLICITUDBr;
            DataTable dt = null;

            SOLICITUDBr = new SOLICITUD_Br();
            dt = SOLICITUDBr.Ps_IMPCONSTANCIA(StrSolicitud);
            return dt;

        }





        ///observaciones

       [HttpPost]
        public ActionResult BuscarObservacion(string IdSolicitud)
        {

            int recordsTotal = 0;
            List<OBSERVACION_Be> list = new List<OBSERVACION_Be>();
            OBSERVACION_Be OBSERVACIONBe = new OBSERVACION_Be();
            OBSERVACION_Br OBSERVACIONBr = new OBSERVACION_Br();

            list = (List<OBSERVACION_Be>)OBSERVACIONBr.Ps_OBSERVACION(IdSolicitud);
            recordsTotal = list.Count();

            var start = Convert.ToInt16(Request.Form.GetValues("start").FirstOrDefault());
            var length = Convert.ToInt32(Request.Form.GetValues("length").FirstOrDefault());

            var draw = Request.Form.GetValues("draw").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;


            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = recordsTotal,
                recordsTotal = recordsTotal,
                data = data
            }, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegistraObservacion()
        {

            OBSERVACION_Be OBSERVACIONBe = new OBSERVACION_Be();

            OBSERVACIONBe.TIPOPROCESO = Request["vTipo"].ToString().Trim();
            OBSERVACIONBe.IDSOLICITUD = Convert.ToInt32(Request["vIdSolicitud"].ToString().Trim());
            OBSERVACIONBe.TXTOBSERVACION = Request["vDescrip"].ToString().Trim();

            string lstNUMDOCUMENTO = new OBSERVACION_Br().Pid_OBSERVACION(OBSERVACIONBe);
            return Json(lstNUMDOCUMENTO, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EliminarObservacion(string strIdObservacion)
        {

            Int32 iCod = Convert.ToInt32(strIdObservacion.ToString());

            OBSERVACION_Be OBSERVACIONBe = new OBSERVACION_Be();

            OBSERVACIONBe.IDOBSERVACION = iCod;
            OBSERVACIONBe.TXTOBSERVACION = "";
            OBSERVACIONBe.TXTIP = "";
            OBSERVACIONBe.TIPOPROCESO = "0";

            string iResultado = new OBSERVACION_Br().Pid_OBSERVACION(OBSERVACIONBe);

            return Json(iResultado, JsonRequestBehavior.AllowGet);

        }

        ///


        ///Respuesta
        [HttpPost]
        public ActionResult BuscarRespuestaObs(string IdSolicitud, string IdObservacion)
        {

            int recordsTotal = 0;
            List<RESPUESTAOBS_Be> list = new List<RESPUESTAOBS_Be>();
            RESPUESTAOBS_Be RESPUESTAOBSBe = new RESPUESTAOBS_Be();
            RESPUESTAOBS_Br RESPUESTAOBSBr = new RESPUESTAOBS_Br();

            list = (List<RESPUESTAOBS_Be>)RESPUESTAOBSBr.Ps_RESPUESTAOBS(IdSolicitud, IdObservacion);
            recordsTotal = list.Count();

            var start = Convert.ToInt16(Request.Form.GetValues("start").FirstOrDefault());
            var length = Convert.ToInt32(Request.Form.GetValues("length").FirstOrDefault());

            var draw = Request.Form.GetValues("draw").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;


            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = recordsTotal,
                recordsTotal = recordsTotal,
                data = data
            }, JsonRequestBehavior.AllowGet);

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegistraRespuestaObs()
        {

            RESPUESTAOBS_Be RESPUESTAOBSBe = new RESPUESTAOBS_Be();

            RESPUESTAOBSBe.TIPOPROCESO = Request["vTipo"].ToString().Trim();
            RESPUESTAOBSBe.IDSOLICITUD = Convert.ToInt32(Request["vIdSolicitud"].ToString().Trim());
            RESPUESTAOBSBe.IDOBSERVACION = Convert.ToInt32(Request["vIdObservacion"].ToString().Trim());
            RESPUESTAOBSBe.TXTRESPUESTA = Request["vRespuesta"].ToString().Trim();

            var pic = System.Web.HttpContext.Current.Request.Files["MFiles"];
            if (pic != null)
            {
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    // var _ext = Path.GetExtension(pic.FileName);
                    string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName;

                    string CarpetaMes = DateTime.Now.Month.ToString();
                    string CarpetaAnio = DateTime.Now.Year.ToString();
                    //string CarpetaBase =  + "FotoPapeleta\\Multa\\";
                    string CarpetaBase = Server.MapPath("~/File_Doc/Documento/");
                    string BusCarpeta = CarpetaBase + CarpetaAnio;
                    bool exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                        Directory.CreateDirectory(BusCarpeta);
                    }

                    BusCarpeta = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes;
                    exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                        Directory.CreateDirectory(BusCarpeta);
                    }

                    var _comPath = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes + "\\" + adjunto;

                    pic.SaveAs(_comPath.ToString());
                    RESPUESTAOBSBe.TXTURLARCHIVO = "../File_Doc/Documento/" + CarpetaAnio + "/" + CarpetaMes + '/' + adjunto;

                }
                else
                {
                    RESPUESTAOBSBe.TXTURLARCHIVO = "";
                }
            }
            else
            {
                RESPUESTAOBSBe.TXTURLARCHIVO = "";
            }

            string lstNUMDOCUMENTO = new RESPUESTAOBS_Br().Pid_RESPUESTAOBS(RESPUESTAOBSBe);
            return Json(lstNUMDOCUMENTO, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EliminarRespuestaObs(string strIdObservacion, string strIdRespuestaObs)
        {

            Int32 IdObservacion = Convert.ToInt32(strIdObservacion.ToString());
            Int32 IdRespuestaObs = Convert.ToInt32(strIdRespuestaObs.ToString());

            RESPUESTAOBS_Be RESPUESTAOBSBe = new RESPUESTAOBS_Be();

            RESPUESTAOBSBe.IDOBSERVACION = IdObservacion;
            RESPUESTAOBSBe.IDRESPUESTAOBS = IdRespuestaObs;
            RESPUESTAOBSBe.TXTRESPUESTA = "";
            RESPUESTAOBSBe.TXTIP = "";
            RESPUESTAOBSBe.TIPOPROCESO = "0";

            string iResultado = new RESPUESTAOBS_Br().Pid_RESPUESTAOBS(RESPUESTAOBSBe);

            return Json(iResultado, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public ActionResult ProcesarSID(string iPERS_P_inCODPER, string iDIRE_P_inCODDIR, string iTIPO_P_inCODTIP, string iFOLIO, string iOBS, string iIDASUNTO, string iOFICINA, string iASUNTO, string iIDSOLICITUD, string iADMINISTRADO, string iFIJO, string iCORREO, string iCELULAR, string iNRODOCIDE, string iPROCEDIMIENTO, string iOBSSOLI)

        {

            int vPERS_P_inCODPER = Convert.ToInt32(iPERS_P_inCODPER);
            int vDIRE_P_inCODDIR = Convert.ToInt32(iDIRE_P_inCODDIR);
            int vTIPO_P_inCODTIP = Convert.ToInt32(iTIPO_P_inCODTIP);
            int vFOLIO = Convert.ToInt32(iFOLIO);
            string vOBS = iOBS.ToString().Trim();
            int vASUNTO = Convert.ToInt32(iIDASUNTO.ToString().Trim());
            int vOFICINA = Convert.ToInt32(iOFICINA);
            int vIDSOLICITUD = Convert.ToInt32(iIDSOLICITUD);

            int vSOficinatra = 0;
            int vSCodigotra = 0;
            string vSUsuariotra = "";
            string mes = DateTime.Now.Month.ToString();
            string dia = DateTime.Now.Day.ToString();
            string hora = DateTime.Now.Hour.ToString();
            string min = DateTime.Now.Minute.ToString();
            string seg = DateTime.Now.Second.ToString();

            if (mes.Length < 2)
                mes = "0" + mes;
            if (dia.Length < 2)
                dia = "0" + dia;
            if (hora.Length < 2)
                hora = "0" + hora;
            if (min.Length < 2)
                min = "0" + min;
            if (seg.Length < 2)
                seg = "0" + seg;
            string fechHora;
            fechHora = dia + "/" + mes + "/" + DateTime.Now.Year.ToString() + ' ' + DateTime.Now.ToString("hh:mm:ss tt");

            /* Insertar el documento en el sistema de Tramite documentario */

            M_MAES_DOCU_Br M_MAES_DOCULn = new M_MAES_DOCU_Br();
            M_MAES_DOCU_Be M_MAES_DOCUBe = new M_MAES_DOCU_Be();
            M_MAES_DOCUBe.DOCU_P_inCODDOC = 0;
            M_MAES_DOCUBe.OFIC_P_inCODOFI = vOFICINA;
            M_MAES_DOCUBe.TIPO_P_inCODTIP = vTIPO_P_inCODTIP;
            M_MAES_DOCUBe.DOCU_chANODOC = DateTime.Now.Year.ToString();
            M_MAES_DOCUBe.DOCU_chNRODOC = "";
            M_MAES_DOCUBe.DOCU_chFECDOC = DateTime.Now.Year.ToString() + mes + dia;
            M_MAES_DOCUBe.DOCU_inNROFOL = vFOLIO;
            M_MAES_DOCUBe.ASUN_P_inCODASU = vASUNTO;
            M_MAES_DOCUBe.PERS_P_inCODPER = vPERS_P_inCODPER;
            M_MAES_DOCUBe.DIRE_P_inCODDIR = vDIRE_P_inCODDIR;
            M_MAES_DOCUBe.DOCU_chCODCON = "";
            M_MAES_DOCUBe.DOCU_chOBSDOC = vOBS;
            M_MAES_DOCUBe.ESTA_P_inCODEST = 1;
            M_MAES_DOCUBe.SEGU_P_chCODUSU = vSUsuariotra; //"USU10-000064";
            M_MAES_DOCUBe.SEGU_chFECINI = DateTime.Now.Year.ToString() + mes + dia;
            M_MAES_DOCUBe.SEGU_chHORA = "";
            M_MAES_DOCUBe.SEGU_chFECMOD = M_MAES_DOCUBe.SEGU_chFECINI;
            M_MAES_DOCUBe.SEGU_btFLAELI = 1;
            M_MAES_DOCUBe.MED_chCOD = "";
            M_MAES_DOCUBe.SEGU_inCREUSU = vSCodigotra;
            M_MAES_DOCUBe.SEGU_inMODUSU = vSCodigotra;
            M_MAES_DOCUBe.SEGU_chCREFEC = fechHora;
            M_MAES_DOCUBe.SEGU_chMODFEC = fechHora;
            M_MAES_DOCUBe.BAND_chCODEST = "0";
            M_MAES_DOCUBe.DOCU_chReg = "EXTERNO";
            M_MAES_DOCUBe.DOCU_chSIGJER = ""; //"SG";
            M_MAES_DOCUBe.DOCU_chSIGOFI = ""; //"SGTDyA";
            M_MAES_DOCUBe.DOCU_chDESASU = iASUNTO + " " + iOBSSOLI;

            string iddocumento = fbd_DocumentoTransf(M_MAES_DOCUBe);

            RESPUESTA_Be RPTBASEBe = new RESPUESTA_Be();


            if (iddocumento != "0")
            {
                RPTBASEBe.proMensaje = "Se Genero en el Sistema de Tramite Documentario.";

                SOLICITUD_Be PASEBe = new SOLICITUD_Be();
                PASEBe.IDSOLICITUD = vIDSOLICITUD;
                PASEBe.DOCU_P_inCODDOC = Convert.ToInt32(iddocumento);
                RPTBASEBe.proValor = new SOLICITUD_Br().Pu_SOLICITUD_SID(PASEBe);

                FcorreoRespuesta(iADMINISTRADO.ToString(), iCORREO.ToString(), dia, mes, DateTime.Now.Year.ToString(), iFIJO.ToString(), iCELULAR.ToString(), iNRODOCIDE.ToString(), RPTBASEBe.proValor, iPROCEDIMIENTO, iASUNTO, vOBS, Convert.ToString(vTIPO_P_inCODTIP));


            }
            else
            {
                RPTBASEBe.proMensaje = "Error no se registro el Pase, Vuelve a intentar.";
            }

            return Json(RPTBASEBe, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult RegistroDatos(int iIDREGISTRO, string iNRODOC, string iDIRECCION, int iCODPER, int iCODDIR)
        {
            REGISTRO_Be PERSONASBe = new REGISTRO_Be();
            RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();
            REGISTRO_Br PERSONASLn = new REGISTRO_Br();

            //Administrado de Tramite documentario
            List<REGISTRO_Be> listPersonas = new List<REGISTRO_Be>();
            PERSONASBe.NRODOCIDE = iNRODOC;
            listPersonas = (List<REGISTRO_Be>)PERSONASLn.listarAdministrado(PERSONASBe);
            int vPERS_P_inCODPER = 0;
            int vDIRE_P_inCODDIR = 0;

            if (listPersonas.Count > 0)
            {

                //Agregar al Administrado de Tramite Documentario 
                M_PERS_Br M_PERSLn = new M_PERS_Br();
                M_PERS_Be M_PERSBe = new M_PERS_Be();

                M_PERSBe.vPERS_P_inCODPER = Convert.ToInt32(listPersonas[0].PERS_P_inCODPER.ToString()); 
                M_PERSBe.vPERS_chNOMPER = listPersonas[0].TXTNOMRAZONSOCIAL.ToString();
                M_PERSBe.vPERS_chAPEPAT = listPersonas[0].TXTAPEPAT.ToString();
                M_PERSBe.vPERS_chAPEMAT = listPersonas[0].TXTAPEMAT.ToString();
                M_PERSBe.vPERS_chRAZSOC = listPersonas[0].TXTNOMRAZONSOCIAL.ToString();
                M_PERSBe.vVARI_P_chCODTIP_TIPDOC = "";
                M_PERSBe.vVARI_P_chCODCAR_TIPDOC = "";
                M_PERSBe.vPERS_chNRODOC = iNRODOC;
                M_PERSBe.vPERS_chTELPER = listPersonas[0].TXTFIJOFONO.ToString();
                M_PERSBe.vPERS_chCELPER = listPersonas[0].TXTCELULAR.ToString();
                M_PERSBe.vPERS_chMEIPER = listPersonas[0].TXTCORREO.ToString();
                M_PERSBe.vSEGU_btFLAELI = 1;
                M_PERSBe.vPERS_chCONTRIB = "";
                M_PERSBe.vPERS_chCONTRIB_COD = "";
                M_PERSBe.vPERS_chNOMCOMP = "";
                M_PERSBe.vPERS_chFECNAC = "";
                M_PERSBe.vPERS_chSEX = "H";
                M_PERSBe.vSEGU_chUSUPC = "";
                M_PERSBe.vSEGU_chUSUMAC = "";
                M_PERSBe.vSEGU_chUSUIP = "";
                //M_PERSBe.vSEGU_inUSUCRE = vSCodigotra;
                //M_PERSBe.vSEGU_inUSUMOD = vSCodigotra;
                //M_PERSBe.vSEGU_chCREUSU = "";
                //M_PERSBe.vSEGU_chMODUSU = "";
                M_PERSBe.vPERS_CIVIL = "";
                M_PERSBe.vPERS_OCUPACION = "";
                M_PERSBe.vPERS_NACIONALIDAD = "";
                M_PERSBe.vPERS_LUGNAC = "";



                if (listPersonas[0].IDTIPODOCIDE.ToString() == "1")
                {
                    M_PERSBe.vVARI_P_chCODTIP_TIPPER = "001";
                    M_PERSBe.vVARI_P_chCODCAR_TIPPER = "001";
                    M_PERSBe.vPERS_chRAZSOC = "";
                    M_PERSBe.vPERS_chNOMCOMP = M_PERSBe.vPERS_chAPEPAT + " " + M_PERSBe.vPERS_chAPEMAT + " " + M_PERSBe.vPERS_chNOMPER;
                }
                if (listPersonas[0].IDTIPODOCIDE.ToString() == "8")
                {
                    M_PERSBe.vVARI_P_chCODTIP_TIPPER = "001";
                    M_PERSBe.vVARI_P_chCODCAR_TIPPER = "003";
                    M_PERSBe.vPERS_chRAZSOC = "";
                    M_PERSBe.vPERS_chNOMCOMP = M_PERSBe.vPERS_chAPEPAT + " " + M_PERSBe.vPERS_chAPEMAT + " " + M_PERSBe.vPERS_chNOMPER;
                }
                else if (listPersonas[0].IDTIPODOCIDE.ToString() == "2")
                {
                    M_PERSBe.vVARI_P_chCODTIP_TIPPER = "001";
                    M_PERSBe.vVARI_P_chCODCAR_TIPPER = "002";
                    M_PERSBe.vPERS_chRAZSOC = M_PERSBe.vPERS_chRAZSOC;
                    M_PERSBe.vPERS_chNOMCOMP = M_PERSBe.vPERS_chRAZSOC;
                }
                else if (listPersonas[0].IDTIPODOCIDE.ToString() == "9")
                {
                    M_PERSBe.vVARI_P_chCODTIP_TIPPER = "001";
                    M_PERSBe.vVARI_P_chCODCAR_TIPPER = "005";
                    M_PERSBe.vPERS_chRAZSOC = "";
                    M_PERSBe.vPERS_chNOMCOMP = M_PERSBe.vPERS_chAPEPAT + " " + M_PERSBe.vPERS_chNOMPER;
                }

                if (M_PERSBe.vPERS_P_inCODPER == 0)
                {
                    vPERS_P_inCODPER = fbd_M_PERSPiud(M_PERSBe);
                }
                else
                {
                    vPERS_P_inCODPER = Convert.ToInt32(listPersonas[0].PERS_P_inCODPER.ToString());
                }


              

                M_MAES_DIRE_Br M_MAES_DIRELn = new M_MAES_DIRE_Br();
                M_MAES_DIRE_Be M_MAES_DIREBe = new M_MAES_DIRE_Be();
                M_MAES_DIREBe.vDIRE_P_inCODDIR = 0;
                M_MAES_DIREBe.vPERS_P_inCODPER = vPERS_P_inCODPER;
                M_MAES_DIREBe.vUBIG_P_inCODUBI = Convert.ToInt32(listPersonas[0].CODDISTDIR.ToString());
                M_MAES_DIREBe.vDIRE_chDESDIR = iDIRECCION.ToString();
                M_MAES_DIREBe.vDIRE_btVIGDIR = 0;
                M_MAES_DIREBe.vSEGU_P_chCODUSU = ""; //"USU10-000064";
                M_MAES_DIREBe.vSEGU_chFECINI = "";
                M_MAES_DIREBe.vSEGU_chFECMOD = "";
                M_MAES_DIREBe.vSEGU_btFLAELI = 1;
                M_MAES_DIREBe.vCALLE = "";
                M_MAES_DIREBe.vVIA = "";
                M_MAES_DIREBe.vDIRE_chNRO = "";
                M_MAES_DIREBe.vDIRE_chINT = "";
                M_MAES_DIREBe.vDIRE_chMAN = "";
                M_MAES_DIREBe.vDIRE_chLOTE = "";
                M_MAES_DIREBe.vDIRE_chURB = "";
                M_MAES_DIREBe.vDIRE_chREFE = "";
                M_MAES_DIREBe.vSEGU_inUSUCRE = 0;
                M_MAES_DIREBe.vSEGU_inUSUMOD = 0;
                M_MAES_DIREBe.vSEGU_chMODFEC = "";
                M_MAES_DIREBe.vSEGU_chCREFEC = "";
                M_MAES_DIREBe.vSEGU_chUSUPC = "";
                M_MAES_DIREBe.vSEGU_chUSUMAC = "";
                M_MAES_DIREBe.vSEGU_chUSUIP = "";
                vDIRE_P_inCODDIR = fbd_M_MAES_DIREPiud(M_MAES_DIREBe);
               
                RESPUESTABe.proValor = new SOLICITUD_Br().PU_DATOSREGISTRO(iIDREGISTRO, vPERS_P_inCODPER, vDIRE_P_inCODDIR);

            }



            if (RESPUESTABe.proValor != "0")
            {
                RESPUESTABe.proMensaje = "Se Envio Correctamente los Datos.";
            }
            else
            {
                RESPUESTABe.proMensaje = "Error generar los Datos, Vuelve a intentar.";
            }

            return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            //string this_id = "2020-001";
            //// do here some operation  
            //return Json(new { id = this_id }, JsonRequestBehavior.AllowGet);
        }

        private int fbd_M_PERSPiud(M_PERS_Be M_PERSBe)
        {
            M_PERS_Br M_PERSLn = new M_PERS_Br();
            return M_PERSLn.Piud_M_PERS(M_PERSBe);
        }

        private int fbd_M_MAES_DIREPiud(M_MAES_DIRE_Be M_MAES_DIREBe)
        {
            M_MAES_DIRE_Br M_MAES_DIRELn = new M_MAES_DIRE_Br();
            return M_MAES_DIRELn.Piud_M_MAES_DIRE(M_MAES_DIREBe);
        }

        private string fbd_DocumentoTransf(M_MAES_DOCU_Be M_MAES_DOCUBe)
        {
            M_MAES_DOCU_Br M_MAES_DOCULn = new M_MAES_DOCU_Br();
            return M_MAES_DOCULn.Piud_M_Maes_Docu(M_MAES_DOCUBe);
        }

        private void FcorreoRespuesta(string strNombre, string strCorreo, string strDia, string strMes, string strAini, string strFIJO, string strCELULAR, string strNRODOCIDE, string strNrExp, string strPROCEDIMIENTO, string strASUNTO, string strObservacion, string strTipoDoc)
        {

            string DescripDoc = "";
            if (strTipoDoc == "1") {
                DescripDoc = "EXPEDIENTE";
            }
            else
            {
                DescripDoc = "DOCUMENTO SIMPLE";
            }
                
            WsCorreo.wsmail mySender = new WsCorreo.wsmail();

            string sendermail = "mesadepartes@munijesusmaria.gob.pe";

            string VHtml = @"
                        <html>
                        <head>
	                        <title>HOJA DE CONSULTA</title>
	                        <style type='text/css'>
	                        .txt12_ciam {font-size: 18px;font-weight: bold;color: #227BBF;text-transform: none; padding-right:15px;}
	                        .tdCampoTextoCabecera{background-color:#f7ffd6; font-size:14px; font-family:Arial; color:Black; font-weight:600;}
	                        .tdCampoTextoNumero{background-color:#f7ffd6; font-size:14px; font-family:Arial; color:black; font-weight:600;}
	                        .tdCampoTextoCabecera2{background-color:White; font-size:12px; font-family:Arial; color:Black;}
	                        .tdCampoTextoCorreo{background-color:White; font-size:14px; font-family:Arial; color:Black;}
	                        </style>
                        </head>

                        <body>
                        <table width='100%' cellpadding='0' cellspacing='0' bgcolor='white'>
	                    <tr>
		                <td align='center' bgcolor=''>
                        <img src='https://www.munijesusmaria.gob.pe/wp-content/uploads/2020/06/cabecera-mdjm-junio-2020.jpg' alt='img'>
                        </td>
                        </tr>
                        <tr>
                        <td align = 'left' width = '100%' class='tdCampoTextoCorreo'>Estimado ciudadano:<br />Se registró su "+ DescripDoc  + @" con los siguientes datos:</td>
	                    </tr>
	                    <tr>
		                <td><br /></td>
	                    </tr>
	                    <tr>
		                <td align = 'center' width='100%' bgcolor='white'>
		                <table cellpadding = '2' cellspacing='0' width='600' border='2' bgcolor='white'>
				        <tr>
					    <td colspan = '5' class='txt12_ciam' align='center' style='background-color:#003165'><p style = 'color:#FFF' > REGISTRO DE " + DescripDoc + @"</p></td>
				        </tr>
	                    <tr>
	                    <td class='tdCampoTextoNumero' align='center' rowspan='2' width='25%'>FECHA</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='25%'>DÍA</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='19%'>MES</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='31%'>AÑO</td>
	                    </tr>
	                    <tr>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strDia + @"</td>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strMes + @"</td>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strAini + @"</td>
	                    </tr>
	                    <tr>
	                      <td class='tdCampoTextoNumero' align='center'>Nro.</td>
	                      <td class='tdCampoTextoCabecera2' align='center'>" + strAini+ "-" + strNrExp + @"</td>
	                      <td colspan = '2' align='center' class='tdCampoTextoCabecera2'>&nbsp;</td>
	                    </tr>
	                    <tr>
	                      <td height = '47' align='center' class='tdCampoTextoNumero'>Tipo de Procedimiento:</td>
	                      <td colspan ='3' align='center' class='tdCampoTextoCabecera2'>" + strPROCEDIMIENTO + @"</td>
	                      </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style ='color:#FFF' > Indentificación del usuario</p></td>
	                    </tr>
	                    <tr>
		                <td class='tdCampoTextoCabecera2' colspan='5' align='left'>NOMBRE:" + strNombre + @"</td>
	                    </tr>
                        <tr>
                        <td height = '44' colspan='2' align='center' ><span class='tdCampoTextoCabecera2'>NRO.DOCUMENTO:" + strNRODOCIDE + @"</span></td>
                        <td colspan = '2' align='center' class='tdCampoTextoCabecera2'>E-MAIL: " + strCorreo + @"</td>
                        </tr>
                        <tr>
                        <td height = '44' colspan='2' align='center' >Teléfono: <span class='tdCampoTextoCabecera2'>" + strFIJO + @"</span></td>
                        <td colspan = '2' align='center' >Celular:" + strCELULAR + @"</td>
                        </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style = 'color:#FFF' >Asunto</ p ></ td >
                        </tr>
                        <tr>
                        <td height='70' colspan='5' align='left' class='tdCampoTextoCabecera2'>" + strASUNTO + @"</td>
	                    </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style = 'color:#FFF' >Observación</ p ></ td >
                        </tr>
                        <tr>
                        <td height='70' colspan='5' align='left' class='tdCampoTextoCabecera2'>" + strObservacion + @"</td>
	                    </tr>

                    </table>
                    </td>
                    </tr>
                    <tr>
	                <td><br /></td></tr>
	                <tr>
		                <td align = 'left' width='100%' class='tdCampoTextoCorreo'>
                        <p>Para hacer seguimiento de su Expediente hacer clic en siguiente enlace</p>
                        <br /><a href='http://tramites.munijesusmaria.gob.pe/'> Estado del Tramite <a/></p>

                          <p>Agradecemos su participación en el proceso de mejora de la institución</p>
                          <p>      Atentamente,<br />Municipalidad de Jesús María<br />Central telefónica 614-1212<br />Central de Serenazgo 634-0100</p>          </p></td></tr></table>
	                </body></html>";


            mySender.EnviaEmailFormatHTML(strCorreo, sendermail, "Acceso Personal - Ventanilla Virtual", VHtml);



        }



    }
}