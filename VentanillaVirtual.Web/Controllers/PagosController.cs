﻿using System;
using System.Web;
using System.Web.Mvc;
using VentanillaVirtual.Web.Models;
using VentanillaVirtual.LogicaNegocio;
using VentanillaVirtual.Entidad;
using System.Data;
using System.Collections.Generic;
using Utils;
using System.Linq;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace VentanillaVirtual.Web.Controllers
{
    public class PagosController : Controller
    {
        // GET: Pagos
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Pd_ListarNROCUENTA()
        {

            List<CUENTA_Be> lsGenerado = null;
            CUENTA_Br CUENTALn = new CUENTA_Br();
            CUENTA_Be CUENTABe = new CUENTA_Be();
            lsGenerado = CUENTALn.listarNroCuenta(CUENTABe);
            CUENTABe.ID_CUENTA = 0;
            //CUENTABe.NROCTA = "Seleccione ...";
            //lsGenerado.Add(CUENTABe);
            //ViewData["VDCalles"] = new SelectList(lsGenerado, "NVIA_P_inID", "UBI1_chDESC", 0);
            return Json(lsGenerado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerarPago(string iIDSOLICITUD, string iFECHAEMI, string iMONTO, string iID_CUENTA)

        {

            PAGO_Br PAGOLn = new PAGO_Br();
            PAGO_Be PAGOBe = new PAGO_Be();
            PAGOBe.IDSOLICITUD = Convert.ToInt32(iIDSOLICITUD);
            PAGOBe.FEPAGO= iFECHAEMI.ToString();
            PAGOBe.MONTO = Convert.ToDecimal(iMONTO.ToString());
            PAGOBe.ID_CUENTA = Convert.ToInt32(iID_CUENTA);
            string iddocumento = fbd_PagoPiud(PAGOBe);

            RESPUESTA_Be RPTBASEBe = new RESPUESTA_Be();


            if (iddocumento != "0")
            {
                RPTBASEBe.proMensaje = "Se Genero el Pago.";
                RPTBASEBe.proValor = "1";
            }
            else
            {
                RPTBASEBe.proMensaje = "Error no se registro el Pago";
            }
            return Json(RPTBASEBe, JsonRequestBehavior.AllowGet);
        }

        private string fbd_PagoPiud(PAGO_Be PAGOBe)
        {
            PAGO_Br PAGOLn = new PAGO_Br();
            return PAGOLn.Pid_PAGOS(PAGOBe);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdjArchivoPago()
        {

            ARCHIVOADJPAGO_Be ARCHIVOADJBe = new ARCHIVOADJPAGO_Be();

            ARCHIVOADJBe.TIPOPROCESO = Request["vTipo"].ToString().Trim();
            ARCHIVOADJBe.IDSOLICITUD = Convert.ToInt32(Request["vIdSolicitud"].ToString().Trim());
            ARCHIVOADJBe.DESCARCHIVO = Request["vDescrip"].ToString().Trim();

            var pic = System.Web.HttpContext.Current.Request.Files["MFiles"];
            if (pic != null)
            {
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    // var _ext = Path.GetExtension(pic.FileName);
                    string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName;

                    string CarpetaMes = DateTime.Now.Month.ToString();
                    string CarpetaAnio = DateTime.Now.Year.ToString();
                    //string CarpetaBase =  + "FotoPapeleta\\Multa\\";
                    string CarpetaBase = Server.MapPath("~/File_Doc/Documento/");
                    string BusCarpeta = CarpetaBase + CarpetaAnio;
                    bool exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                        Directory.CreateDirectory(BusCarpeta);
                    }

                    BusCarpeta = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes;
                    exists = Directory.Exists(BusCarpeta);
                    if (!exists)
                    {
                        Directory.CreateDirectory(BusCarpeta);
                    }

                    var _comPath = CarpetaBase + CarpetaAnio + "\\" + CarpetaMes + "\\" + adjunto;

                    pic.SaveAs(_comPath.ToString());
                    ARCHIVOADJBe.TXTURLARCHIVO = "../File_Doc/Documento/" + CarpetaAnio + "/" + CarpetaMes + '/' + adjunto;

                }
                else
                {
                    ARCHIVOADJBe.TXTURLARCHIVO = "";
                }
            }
            else
            {
                ARCHIVOADJBe.TXTURLARCHIVO = "";
            }

            string lstNUMDOCUMENTO = fbd_ArchivoAdjPid(ARCHIVOADJBe);
            return Json(lstNUMDOCUMENTO, JsonRequestBehavior.AllowGet);

        }

        private string fbd_ArchivoAdjPid(ARCHIVOADJPAGO_Be ARCHIVOADJBe)
        {

            return new ARCHIVODJPAGO_Br().Pid_ARCHIVOADJ(ARCHIVOADJBe);

        }

        [HttpPost]
        public ActionResult BuscarAdjArchivoPago(string IdSolicitud)
        {

            int recordsTotal = 0;
            List<ARCHIVOADJPAGO_Be> list = new List<ARCHIVOADJPAGO_Be>();
            ARCHIVOADJPAGO_Be ARCHIVOADJBe = new ARCHIVOADJPAGO_Be();
            ARCHIVODJPAGO_Br ARCHIVOADJBr = new ARCHIVODJPAGO_Br();

            list = (List<ARCHIVOADJPAGO_Be>)ARCHIVOADJBr.Ps_ARCHIVOADJ(IdSolicitud);
            recordsTotal = list.Count();

            var start = Convert.ToInt16(Request.Form.GetValues("start").FirstOrDefault());
            var length = Convert.ToInt32(Request.Form.GetValues("length").FirstOrDefault());

            var draw = Request.Form.GetValues("draw").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;


            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = recordsTotal,
                recordsTotal = recordsTotal,
                data = data
            }, JsonRequestBehavior.AllowGet);


        }



        [HttpPost]
        public ActionResult EliminarAdjArchivoPago(string strIdArc)
        {

            Int32 iCod = Convert.ToInt32(strIdArc.ToString());

            ARCHIVOADJPAGO_Be ARCHIVOADJBe = new ARCHIVOADJPAGO_Be();

            ARCHIVOADJBe.CODARCHIVO = iCod;
            ARCHIVOADJBe.DESCARCHIVO = "";
            ARCHIVOADJBe.TXTIP = "";
            ARCHIVOADJBe.TIPOPROCESO = "3";

            string iResultado = fbd_ArchivoAdjPid(ARCHIVOADJBe);

            return Json(iResultado, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult ActualizarEstPago(string strIdSolicitud,string strOpc)
        {

            Int32 iCod = Convert.ToInt32(strIdSolicitud.ToString());

            ARCHIVOADJPAGO_Be ARCHIVOADJBe = new ARCHIVOADJPAGO_Be();

            ARCHIVOADJBe.IDSOLICITUD= iCod;
            ARCHIVOADJBe.DESCARCHIVO = "";
            ARCHIVOADJBe.TXTIP = "";
            ARCHIVOADJBe.TIPOPROCESO = strOpc ;

            string iResultado = fbd_ActualizaPago(ARCHIVOADJBe);

            return Json(iResultado, JsonRequestBehavior.AllowGet);

        }

        private string fbd_ActualizaPago(ARCHIVOADJPAGO_Be ARCHIVOADJBe)
        {

            return new ARCHIVODJPAGO_Br().Pid_ACTUALIZAPAGO(ARCHIVOADJBe);

        }

    }
}