﻿using System;
using System.Web;
using System.Web.Mvc;
using VentanillaVirtual.Web.Models;
using VentanillaVirtual.LogicaNegocio;
using VentanillaVirtual.Entidad;
using System.Data;
using System.Collections.Generic;
using Utils;
using System.Linq;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace VentanillaVirtual.Web.Controllers
{
    public class SolicitudExpController : Controller
    {
        // GET: SolicitudExp
        public ActionResult Index()
        {
            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();
            return View();
        }

        [HttpPost]
        public ActionResult BuscarSolicitud(int strTIPOSOLICITUD)
        {


            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            //var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            //var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;

            int strOFIC_P_inCODOFI = Convert.ToInt32( Session["SIdOficina"].ToString());

            List<SOLICITUD_Be> list = new List<SOLICITUD_Be>();


            list = fbd_BuscarSolicitud(strTIPOSOLICITUD, strOFIC_P_inCODOFI);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        private static List<SOLICITUD_Be> fbd_BuscarSolicitud(int strTIPOSOLICITUD, int strOFIC_P_inCODOFI)
        {

            List<SOLICITUD_Be> lsGenerado = null;
            try
            {

                SOLICITUD_Br SOLICITUDBr = new SOLICITUD_Br();
                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();

                SOLICITUDBe.IDTIPOSOLICITUD = strTIPOSOLICITUD;
                SOLICITUDBe.OFIC_P_inCODOFI = strOFIC_P_inCODOFI;

                lsGenerado = SOLICITUDBr.Ps_SOLICITUD_TIPO_AREA(SOLICITUDBe);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return lsGenerado;
        }

        [HttpPost]
        public ActionResult ProcesarSID(string iPERS_P_inCODPER, string iDIRE_P_inCODDIR, string iTIPO_P_inCODTIP, string iFOLIO, string iOBS, string iIDASUNTO, string iOFICINA, string iASUNTO, string iIDSOLICITUD,string iADMINISTRADO,string iFIJO, string  iCORREO, string  iCELULAR, string iNRODOCIDE,string iPROCEDIMIENTO,string iOBSSOLI)

        {


            

            int vPERS_P_inCODPER = Convert.ToInt32(iPERS_P_inCODPER);
            int vDIRE_P_inCODDIR = Convert.ToInt32(iDIRE_P_inCODDIR);
            int vTIPO_P_inCODTIP = Convert.ToInt32(iTIPO_P_inCODTIP);
            int vFOLIO           = Convert.ToInt32(iFOLIO);
            string vOBS          = iOBS.ToString().Trim();
            int vASUNTO          = Convert.ToInt32(iIDASUNTO.ToString().Trim());
            int vOFICINA         = Convert.ToInt32(iOFICINA);
            int vIDSOLICITUD     = Convert.ToInt32(iIDSOLICITUD);

            int vSOficinatra     = 0;
            int vSCodigotra      = 0;
            string vSUsuariotra  = "";
            string mes = DateTime.Now.Month.ToString();
            string dia = DateTime.Now.Day.ToString();
            string hora = DateTime.Now.Hour.ToString();
            string min = DateTime.Now.Minute.ToString();
            string seg = DateTime.Now.Second.ToString();

            if (mes.Length < 2)
                mes = "0" + mes;
            if (dia.Length < 2)
                dia = "0" + dia;
            if (hora.Length < 2)
                hora = "0" + hora;
            if (min.Length < 2)
                min = "0" + min;
            if (seg.Length < 2)
                seg = "0" + seg;
            string fechHora;
            fechHora = dia + "/" + mes + "/" + DateTime.Now.Year.ToString() + ' ' + DateTime.Now.ToString("hh:mm:ss tt");

            /* Insertar el documento en el sistema de Tramite documentario */

            M_MAES_DOCU_Br M_MAES_DOCULn = new M_MAES_DOCU_Br();
            M_MAES_DOCU_Be M_MAES_DOCUBe = new M_MAES_DOCU_Be();
            M_MAES_DOCUBe.DOCU_P_inCODDOC = 0;
            M_MAES_DOCUBe.OFIC_P_inCODOFI = vOFICINA;
            M_MAES_DOCUBe.TIPO_P_inCODTIP = vTIPO_P_inCODTIP;
            M_MAES_DOCUBe.DOCU_chANODOC = DateTime.Now.Year.ToString();
            M_MAES_DOCUBe.DOCU_chNRODOC = "";
            M_MAES_DOCUBe.DOCU_chFECDOC = DateTime.Now.Year.ToString() + mes + dia;
            M_MAES_DOCUBe.DOCU_inNROFOL = vFOLIO;
            M_MAES_DOCUBe.ASUN_P_inCODASU = vASUNTO;
            M_MAES_DOCUBe.PERS_P_inCODPER = vPERS_P_inCODPER;
            M_MAES_DOCUBe.DIRE_P_inCODDIR = vDIRE_P_inCODDIR;
            M_MAES_DOCUBe.DOCU_chCODCON = "";
            M_MAES_DOCUBe.DOCU_chOBSDOC = vOBS;
            M_MAES_DOCUBe.ESTA_P_inCODEST = 1;
            M_MAES_DOCUBe.SEGU_P_chCODUSU = vSUsuariotra; //"USU10-000064";
            M_MAES_DOCUBe.SEGU_chFECINI = DateTime.Now.Year.ToString() + mes + dia;
            M_MAES_DOCUBe.SEGU_chHORA = "";
            M_MAES_DOCUBe.SEGU_chFECMOD = M_MAES_DOCUBe.SEGU_chFECINI;
            M_MAES_DOCUBe.SEGU_btFLAELI = 1;
            M_MAES_DOCUBe.MED_chCOD = "";
            M_MAES_DOCUBe.SEGU_inCREUSU = vSCodigotra;
            M_MAES_DOCUBe.SEGU_inMODUSU = vSCodigotra;
            M_MAES_DOCUBe.SEGU_chCREFEC = fechHora;
            M_MAES_DOCUBe.SEGU_chMODFEC = fechHora;
            M_MAES_DOCUBe.BAND_chCODEST = "0";
            M_MAES_DOCUBe.DOCU_chReg = "EXTERNO";
            M_MAES_DOCUBe.DOCU_chSIGJER = ""; //"SG";
            M_MAES_DOCUBe.DOCU_chSIGOFI = ""; //"SGTDyA";
            M_MAES_DOCUBe.DOCU_chDESASU = iASUNTO+ " "+ iOBSSOLI;

            string iddocumento = fbd_DocumentoTransf(M_MAES_DOCUBe);

            RESPUESTA_Be RPTBASEBe = new RESPUESTA_Be();


            if (iddocumento != "0")
            {
                RPTBASEBe.proMensaje = "Se Genero en el Sistema de Tramite Documentario.";

                SOLICITUD_Be PASEBe = new SOLICITUD_Be();
                PASEBe.IDSOLICITUD = vIDSOLICITUD;
                PASEBe.DOCU_P_inCODDOC = Convert.ToInt32(iddocumento);
                RPTBASEBe.proValor = new SOLICITUD_Br().Pu_SOLICITUD_SID(PASEBe);

                FcorreoRespuesta(iADMINISTRADO.ToString(), iCORREO.ToString(), dia, mes, DateTime.Now.Year.ToString(), iFIJO.ToString(), iCELULAR.ToString(), iNRODOCIDE.ToString(), iddocumento,  iPROCEDIMIENTO, iASUNTO, vOBS);


            }
            else
            {
                RPTBASEBe.proMensaje = "Error no se registro el Pase, Vuelve a intentar.";
            }

            return Json(RPTBASEBe, JsonRequestBehavior.AllowGet);
        }

        private string fbd_DocumentoTransf(M_MAES_DOCU_Be M_MAES_DOCUBe)
        {
            M_MAES_DOCU_Br M_MAES_DOCULn = new M_MAES_DOCU_Br();
            return M_MAES_DOCULn.Piud_M_Maes_Docu(M_MAES_DOCUBe);
        }

        private void FcorreoRespuesta(string strNombre, string strCorreo,string strDia,string strMes, string strAini, string strFIJO, string strCELULAR, string strNRODOCIDE,string strNrExp,string strPROCEDIMIENTO, string strASUNTO,string strObservacion)
        {

            WsCorreo.wsmail mySender = new WsCorreo.wsmail();

            string sendermail = "mesadepartes@munijesusmaria.gob.pe";

            string VHtml = @"
                        <html>
                        <head>
	                        <title>HOJA DE CONSULTA</title>
	                        <style type='text/css'>
	                        .txt12_ciam {font-size: 18px;font-weight: bold;color: #227BBF;text-transform: none; padding-right:15px;}
	                        .tdCampoTextoCabecera{background-color:#f7ffd6; font-size:14px; font-family:Arial; color:Black; font-weight:600;}
	                        .tdCampoTextoNumero{background-color:#f7ffd6; font-size:14px; font-family:Arial; color:black; font-weight:600;}
	                        .tdCampoTextoCabecera2{background-color:White; font-size:12px; font-family:Arial; color:Black;}
	                        .tdCampoTextoCorreo{background-color:White; font-size:14px; font-family:Arial; color:Black;}
	                        </style>
                        </head>

                        <body>
                        <table width='100%' cellpadding='0' cellspacing='0' bgcolor='white'>
	                    <tr>
		                <td align='center' bgcolor=''>
                        <img src='https://www.munijesusmaria.gob.pe/wp-content/uploads/2020/06/cabecera-mdjm-junio-2020.jpg' alt='img'>
                        </td>
                        </tr>
                        <tr>
                        <td align = 'left' width = '100%' class='tdCampoTextoCorreo'>Estimado ciudadano:<br />Se registró su Expediente con los siguientes datos:</td>
	                    </tr>
	                    <tr>
		                <td><br /></td>
	                    </tr>
	                    <tr>
		                <td align = 'center' width='100%' bgcolor='white'>
		                <table cellpadding = '2' cellspacing='0' width='600' border='2' bgcolor='white'>
				        <tr>
					    <td colspan = '5' class='txt12_ciam' align='center' style='background-color:#003165'><p style = 'color:#FFF' > REGISTRO DE EXPEDIENTE</p></td>
				        </tr>
	                    <tr>
	                    <td class='tdCampoTextoNumero' align='center' rowspan='2' width='25%'>FECHA</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='25%'>DÍA</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='19%'>MES</td>
	                    <td class='tdCampoTextoCabecera2' align='center' width='31%'>AÑO</td>
	                    </tr>
	                    <tr>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strDia + @"</td>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strMes + @"</td>
		                    <td class='tdCampoTextoCabecera2' align='center'>" + strAini + @"</td>
	                    </tr>
	                    <tr>
	                      <td class='tdCampoTextoNumero' align='center'>Nro.</td>
	                      <td class='tdCampoTextoCabecera2' align='center'>" + strAini + "-"+ strNrExp + @"</td>
	                      <td colspan = '2' align='center' class='tdCampoTextoCabecera2'>&nbsp;</td>
	                    </tr>
	                    <tr>
	                      <td height = '47' align='center' class='tdCampoTextoNumero'>Tipo de Procedimiento:</td>
	                      <td colspan ='3' align='center' class='tdCampoTextoCabecera2'>" + strPROCEDIMIENTO + @"</td>
	                      </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style ='color:#FFF' > Indentificación del usuario</p></td>
	                    </tr>
	                    <tr>
		                <td class='tdCampoTextoCabecera2' colspan='5' align='left'>NOMBRE:" + strNombre + @"</td>
	                    </tr>
                        <tr>
                        <td height = '44' colspan='2' align='center' ><span class='tdCampoTextoCabecera2'>NRO.DOCUMENTO:" + strNRODOCIDE +@"</span></td>
                        <td colspan = '2' align='center' class='tdCampoTextoCabecera2'>E-MAIL: " + strCorreo + @"</td>
                        </tr>
                        <tr>
                        <td height = '44' colspan='2' align='center' >Teléfono: <span class='tdCampoTextoCabecera2'>" + strFIJO + @"</span></td>
                        <td colspan = '2' align='center' >Celular:" + strCELULAR  + @"</td>
                        </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style = 'color:#FFF' >Asunto</ p ></ td >
                        </tr>
                        <tr>
                        <td height='70' colspan='5' align='left' class='tdCampoTextoCabecera2'>"+ strASUNTO + @"</td>
	                    </tr>
	                    <tr>
	                    <td class='tdCampoTextoCabecera' colspan='5' align='center' style='background-color:#003165'><p style = 'color:#FFF' >Observación</ p ></ td >
                        </tr>
                        <tr>
                        <td height='70' colspan='5' align='left' class='tdCampoTextoCabecera2'>" + strObservacion + @"</td>
	                    </tr>

                    </table>
                    </td>
                    </tr>
                    <tr>
	                <td><br /></td></tr>
	                <tr>
		                <td align = 'left' width='100%' class='tdCampoTextoCorreo'>
                        <p>Se brindará a su caso en un plazo máximo de 72 horas, en caso contrario, favor de comunicarse al siguiente correo:
                        <br /><span style = 'color:#03C' > mesadepartes@munijesusmaria.gob.pe</span><br /></p>
                        <p>Para hacer seguimiento de su Expediente hacer clic en siguiente enlace</p>
                        <br /><a href='http://tramites.munijesusmaria.gob.pe/'> Estado del Tramite <a/></p>

                          <p>Agradecemos su participación en el proceso de mejora de la institución</p>
                          <p>      Atentamente,<br />Municipalidad de Jesús María<br />Central telefónica 614-1212<br />Central de Serenazgo 634-0100</p>          </p></td></tr></table>
	                </body></html>";


            mySender.EnviaEmailFormatHTML(strCorreo, sendermail, "Acceso Personal - Ventanilla Virtual", VHtml);



        }




    }
}