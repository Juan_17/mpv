﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagoWeb.Web.Models;
using Utils;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using CrystalDecisions.Shared;


namespace PagoWeb.Web.Controllers
{
    [CheckAuthorization]
    public class RptMastController : Controller
    {
        // GET: RptMast
        public ActionResult Index(FormCollection form)
        {

            FVarGlobal();
            WsPagoWeb.PWServicioSoapClient WsContri = new WsPagoWeb.PWServicioSoapClient();
            WsPagoWeb.CONTRIBUYENTE_Be CONTRIBUYENTEBe = new WsPagoWeb.CONTRIBUYENTE_Be();
            CONTRIBUYENTEBe = (WsPagoWeb.CONTRIBUYENTE_Be)Session["Slogin"];

            string strResultado = "";
            string pstResultado = "";
            string pstCodAutorizacion= "";
            string pstNroReferencia= "";
            string pstMoneda = "";
            string pstMonto = "";
            string pstOrden = "";
            string pstCodRespuesta = "";
            string pstTarjeta = "";
            string pstCodCliente = "";
            string pstCodPais = "";
            string strHora = "";
            string strFecha = "";
            string strFechaHora = "";
            string strEstiloMensaje = "";
            string pstSecureCode = "";
            string pstTxtRespuesta = "";
            string hashPuntoWeb = "";

            if (!string.IsNullOrEmpty(Request.Form["O1"]))
                {

                    pstResultado = Request.Form["O1"].ToString();
                    pstCodAutorizacion = Request.Form["O2"].ToString();
                    pstNroReferencia = Request.Form["O3"].ToString();
                    pstMoneda = Request.Form["O8"].ToString();
                    pstMonto = Request.Form["O9"].ToString();
                    pstOrden = Request.Form["O10"].ToString();
                    pstCodRespuesta = Request.Form["O13"].ToString();
                    pstTarjeta = Request.Form["O15"].ToString();
                    pstCodCliente = Request.Form["O18"].ToString();
                    pstCodPais = Request.Form["O19"].ToString();
                    pstSecureCode = Request.Form["O16"].ToString();
                    pstTxtRespuesta = Request.Form["O17"].ToString();

                }


            if (pstResultado == "A")
                {

                    strResultado = "Aceptada";
                    //Key Merchant
                    string strKey = System.Configuration.ConfigurationManager.AppSettings["P_keyM"].ToString();
                    hashPuntoWeb = Server.UrlDecode(Request.Form["O20"].ToString());

                    string strCadena;
                    Signature sign = new Signature();
                    string strFirma;
                    string[] arrDatos = new string[11];

                    arrDatos[0] = pstResultado;
                    arrDatos[1] = pstCodAutorizacion;
                    arrDatos[2] = pstNroReferencia;
                    arrDatos[3] = pstMoneda;
                    arrDatos[4] = pstMonto;
                    arrDatos[5] = pstOrden;
                    arrDatos[6] = pstCodRespuesta;
                    arrDatos[7] = pstTarjeta;
                    arrDatos[8] = pstCodCliente;
                    arrDatos[9] = pstCodPais;
                    arrDatos[10] = strKey;
                    //Concatenando Inputs
                    strCadena = string.Join("", arrDatos);
                    //Generando MAC
                    strFirma = sign.GenerateSignature(strCadena, strKey);

                    string strFecha1 = Request.Form["O11"].ToString().Substring(0, 4);
                    string strFecha2 = Request.Form["O11"].ToString().Substring(4, 2);
                    string strFecha3 = Request.Form["O11"].ToString().Substring(6, 2);


                    string strHora1 = Request.Form["O12"].ToString().Substring(0, 2);
                    string strHora2 = Request.Form["O12"].ToString().Substring(2, 2);
                    string strHora3 = Request.Form["O12"].ToString().Substring(4, 2);

                    strHora = strHora1 + ":" + strHora2 + ":" + strHora3;
                    strFecha = strFecha1 + "/" + strFecha2 + "/" + strFecha3;
                    strFechaHora = strFecha1 + "/" + strFecha2 + "/" + strFecha3 + " " + strHora;

                    strEstiloMensaje = "alert alert-info";

                }
                else
                {
                    if (pstResultado == "D")
                        {
                            strResultado = "Denegada";
                            strEstiloMensaje = "alert alert-danger";
                            pstMonto = "0.00";
                        }
                    else
                        {
                            if (pstResultado == "E")
                                {
                                    strResultado = "Error";
                                    strEstiloMensaje = "alert alert-danger";
                                    pstMonto = "0.00";
                                }
                            else
                                {
                                    strResultado = "No Valido";
                                    strEstiloMensaje = "alert alert-danger";
                                    pstMonto = "0.00";
                                }
                        }
                }


            //GUARDAR LOS DATOS EN MASTER
            WsPagoWeb.RESULTADO_MASTER_Be RESULTADOMASTERBe = new WsPagoWeb.RESULTADO_MASTER_Be();

            RESULTADOMASTERBe.vIDTARJETA = "MC";
            RESULTADOMASTERBe.vTXTRESPUESTA = strResultado;
            RESULTADOMASTERBe.vTXTCODAUTORIZA = pstCodAutorizacion;
            RESULTADOMASTERBe.vIDPAGO = pstOrden;
            RESULTADOMASTERBe.vTXTNROREFERENCIA = pstNroReferencia;
            RESULTADOMASTERBe.vNROCUOTA = "";
            RESULTADOMASTERBe.vFECCUOTA = "";
            RESULTADOMASTERBe.vTXTMONEDACUOTA = "";
            RESULTADOMASTERBe.vMONTOCUOTA = "";
            RESULTADOMASTERBe.vTXTMONEDA = pstMoneda;
            RESULTADOMASTERBe.vTXTMONTO = pstMonto;
            RESULTADOMASTERBe.vTXTNROREF = pstOrden;
            RESULTADOMASTERBe.vTXTFECTRANSACCION = strFecha;
            RESULTADOMASTERBe.vTXTHORATRANSACCION = strHora;
            RESULTADOMASTERBe.vCODRESPUESTA = pstCodRespuesta;
            RESULTADOMASTERBe.vCODPAIS = pstCodPais;
            RESULTADOMASTERBe.vTXTNROTARJETA = pstTarjeta;
            RESULTADOMASTERBe.vTXTSECURECODE = pstSecureCode;
            RESULTADOMASTERBe.vTXTMENSRESPUESTA = pstTxtRespuesta;
            RESULTADOMASTERBe.vCODCLI = pstCodCliente;
            RESULTADOMASTERBe.vCODPAISTXN = pstCodPais;
            RESULTADOMASTERBe.vTXTFIRMA = hashPuntoWeb;

            int Guardar = WsContri.Master_Autorizacion("Localhost", RESULTADOMASTERBe);


            //TRAEMOS LOS DATOS DESPUES DE GUARDAR
            WsPagoWeb.RESULTADO_RECIBO_Be RESULTADORECIBOBe = new WsPagoWeb.RESULTADO_RECIBO_Be();
            List<WsPagoWeb.RESULTADO_RECIBO_Be> listaRESULTADORECIBOBe = new List<WsPagoWeb.RESULTADO_RECIBO_Be>(WsContri.Ps_RECIBO_MASTER(pstOrden)); //"24"

            DataTable DtRESULTADORECIBO = ucTablaLista<WsPagoWeb.RESULTADO_RECIBO_Be>.ListaATabla(listaRESULTADORECIBOBe);
            ViewBag.VbRESULTADORECIBO = DtRESULTADORECIBO.AsEnumerable();



            ViewBag.VBResultado = strResultado;
            ViewBag.NroPedido = pstOrden;
            ViewBag.FecProceso = strFecha;
            ViewBag.EstiloMensaje = strEstiloMensaje;
            ViewBag.EstadoPedido = pstTxtRespuesta;
            ViewBag.NroTarjeta = pstTarjeta;
            ViewBag.CodContri = CONTRIBUYENTEBe.IDCONTRIBUYENTE;
            ViewBag.FecHoraProceso = strFechaHora;
            ViewBag.RazonSocial = CONTRIBUYENTEBe.RAZONSOCIAL;
            ViewBag.Importe = pstMonto;
            ViewBag.NroDoc = CONTRIBUYENTEBe.NRODOCIDE;
            ViewBag.Moneda = pstMoneda;
            ViewBag.DirFiscal = CONTRIBUYENTEBe.DIREFISCAL;
            ViewBag.TarjetaHabiente = pstTarjeta;
            ViewBag.Telefono = CONTRIBUYENTEBe.TELEFONO;



            return View();

        }

        private void FVarGlobal()
        {

            //indicador de Menu
            //ViewBag.vbOPEN2 = "current open";
            ViewBag.vbSELECT2 = "current";
            ViewBag.vbMENU01 = "Panel";
            ViewBag.vbMENU02 = "Cuenta Corriente";
            ViewBag.vbMENU03 = "Pago en Línea";
            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();
            //fin indicador

            ViewBag.vbAnio = System.Configuration.ConfigurationManager.AppSettings["P_Anio"].ToString();
            ViewBag.vbEntidad = System.Configuration.ConfigurationManager.AppSettings["P_Municipalidad"].ToString();
            ViewBag.vbTitulo = System.Configuration.ConfigurationManager.AppSettings["P_TituloApp"].ToString();
            ViewBag.vbDir1 = System.Configuration.ConfigurationManager.AppSettings["P_DirMuni"].ToString();
            ViewBag.vbDir2 = System.Configuration.ConfigurationManager.AppSettings["P_Dir2Muni"].ToString();
            ViewBag.vbTelef = System.Configuration.ConfigurationManager.AppSettings["P_TelfMuni"].ToString();
            ViewBag.vbPagina = System.Configuration.ConfigurationManager.AppSettings["P_PaginaWeb"].ToString();
            ViewBag.vbCorreo = System.Configuration.ConfigurationManager.AppSettings["P_Correo"].ToString();

        }

    }





}