﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaVirtual.Entidad;
using VentanillaVirtual.LogicaNegocio;
using Utils;
using System.Data;

namespace VentanillaVirtual.Web.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {

            FVarGlobal();
            return View();

        }

        // GET: Login
        public ActionResult Registrar()
        {
            FVarGlobal();
            return View();

        }


        // POST: /Account/Login
        [HttpPost]
        public ActionResult ValidaLogin(FormCollection form)
        {

            FVarGlobal();

            int RptSesion;
            string lstNroDoc = form["Txtuser"].ToString().Trim();
            //string lstclave = Funciones.Encriptar(form["Txtpass"].ToString().Trim());
            string lstclave = form["Txtpass"].ToString().Trim();
            string lstAnio = System.Configuration.ConfigurationManager.AppSettings["P_Anio"].ToString();

            SESION_Be Ssesion = new SESION_Be();

            Ssesion.TXTLOGIN = lstNroDoc;
            Ssesion.TXTCLAVE = lstclave;

            Ssesion.FECREGISTRO = DateTime.Today.ToString("yyyyMMddHHmmss");

            if (Request.ServerVariables["REMOTE_ADDR"] != null)
            {
                Ssesion.IPLOCAL = Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                Ssesion.IPLOCAL = Request.ServerVariables["REMOTE_HOST"];
            }


            USUARIO_Be USUARIOBe = new USUARIO_Be();

            USUARIOBe = new USUARIO_Br().Ps_VALIDARLOGIN(lstNroDoc, lstclave);

            if (USUARIOBe == null)
            {

                Ssesion.TXTESTADO = "El nombre de usuario o la contraseña son incorrectos. Comprueba que no esté activada la tecla Bloq Mayús y vuelve a introducir el nombre de usuario y contraseña.";
                RptSesion = new SESION_Ln().Pi_SESION(Ssesion);

                return RedirectToAction("Index", "Login");

            }
            else
            {

                Ssesion.TXTESTADO = "Login Correcto";
                Ssesion.IDACCESO = USUARIOBe.IDUSUARIO;
                RptSesion = new SESION_Ln().Pi_SESION(Ssesion);

                Session["Slogin"] = USUARIOBe;
                Session["SRazonSocial"] = USUARIOBe.RAZONSOCIAL;
                if (USUARIOBe.RAZONSOCIAL.Length > 9)
                {
                    Session["LRazonSocial"] = "9";
                }
                else
                {
                    Session["LRazonSocial"] = USUARIOBe.RAZONSOCIAL.Length.ToString();
                }

                Session["SIdUsuario"] = USUARIOBe.IDUSUARIO;
                Session["STituloWeb"] = ViewBag.vEntidad + " | " + ViewBag.vDesc01;
                Session["SAnio"] = System.Configuration.ConfigurationManager.AppSettings["P_Anio"].ToString();

                Session["SIdOficina"] = USUARIOBe.IDOFICINA;
                Session["SNombOficina"] = USUARIOBe.OFICINA;

                Session["STipoAcceso"] = USUARIOBe.TIPOACCESO;
                Session["SIp"] = Ssesion.IPLOCAL;

                FnLOGIN(lstNroDoc, false);

                //activamos para contar en linea
                Ssesion.IDSESION = RptSesion;
                Ssesion.TXTESTADOFINAL = "Abierto";
                new SESION_Ln().Pu_SESION(Ssesion);
                //return RedirectToAction("Index", "Panel");
                return RedirectToAction("Index", "Panel");

            }

        }

        private void FnLOGIN(string strLOGIN, bool isPersistent = false)
        {
            // Clear any lingering authencation data
            FormsAuthentication.SignOut();
            FormsAuthentication.SetAuthCookie(strLOGIN, isPersistent);
            // Write the authentication cookie

        }

        public ActionResult CerrarSesion(string Id)
        {
            Session["Slogin"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");

        }


        private void FVarGlobal()
        {

            ViewBag.vEntidad = System.Configuration.ConfigurationManager.AppSettings["P_Municipalidad"].ToString();
            ViewBag.vDesc01 = System.Configuration.ConfigurationManager.AppSettings["P_TituloApp"].ToString();
            ViewBag.vDesc02 = System.Configuration.ConfigurationManager.AppSettings["P_DirMuni"].ToString();
            ViewBag.vDesc03 = System.Configuration.ConfigurationManager.AppSettings["P_Dir2Muni"].ToString();
            ViewBag.vDesc04 = System.Configuration.ConfigurationManager.AppSettings["P_TelfMuni"].ToString();
            ViewBag.vbPagina = System.Configuration.ConfigurationManager.AppSettings["P_PaginaWeb"].ToString();

        }

        [HttpPost]
        public ActionResult ConsultaExt(string strNum,string strTipo)
        {
            try
           {

            if (strTipo=="1")
            {
                WsRecSun.WswPIDE WSreniec = new WsRecSun.WswPIDE();
                WsRecSun.RENIECV2_BE RENIECV2BE = new WsRecSun.RENIECV2_BE();
                RENIECV2BE = WSreniec.RENIECv2(strNum, "40986086");
                return Json(RENIECV2BE, JsonRequestBehavior.AllowGet);
            }

            if (strTipo == "2")
            {
                WsRecSun.WswPIDE WSSunat = new WsRecSun.WswPIDE();
                WsRecSun.DATOSPRINRUC_BE DATOSPRINRUCBE = new WsRecSun.DATOSPRINRUC_BE();
                DATOSPRINRUCBE = WSSunat.RUC(strNum);
                return Json(DATOSPRINRUCBE, JsonRequestBehavior.AllowGet);
            }


            return Json(null, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }

        [HttpPost]
        public ActionResult EnviarDatos(FormCollection form)
        {

            try
            {
                REGISTRO_Be REGISTROBe = new REGISTRO_Be();

                if (form.Count == 0)
                {
                    return View("Index");
                }
                else
                {

                    REGISTROBe.IDTIPODOCIDE = Convert.ToInt32(form["ddlTipoDoc"].ToString().Trim().ToUpper());
                    REGISTROBe.NRODOCIDE = form["TxtNroDoc"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTAPEPAT = form["TxtApePatP"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTAPEMAT = form["TxtApeMatP"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTNOMRAZONSOCIAL = form["TxtNombreP"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTFIJOFONO = form["txtCasa"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTCELULAR = form["txtCelular"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTFECNAC = form["txtFnac"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTCORREO = form["txtEmailA"].ToString().Trim().ToUpper();
                    //REGISTROBe.IDTIPODOCIDE = form["txtEmailB
                    REGISTROBe.CODDEPDIR = form["ddlDep"].ToString().Trim().ToUpper();
                    REGISTROBe.CODPROVDIR = form["ddlProv"].ToString().Trim().ToUpper();
                    REGISTROBe.CODDISTDIR = form["ddlDist"].ToString().Trim().ToUpper();
                    REGISTROBe.CODVIADIR = form["ddlVia"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTVIA = form["txtVia"].ToString().Trim().ToUpper();
                    REGISTROBe.CODOP1DIR = form["ddlOp1"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTOP1DIR = form["txtOp1"].ToString().Trim().ToUpper();
                    REGISTROBe.CODOP2DIR = form["ddlOp2"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTOP2DIR = form["txtOp2"].ToString().Trim().ToUpper();
                    REGISTROBe.CODOP3DIR = form["ddlOp3"].ToString().Trim().ToUpper();
                    REGISTROBe.TXTOP3DIR = form["txtOp3"].ToString().Trim().ToUpper();

                    REGISTRO_Br REGISTROBr = new REGISTRO_Br();

           

                    REGISTROBe.IDREGISTRO = REGISTROBr.Pi_REGISTRO(REGISTROBe);
                     if (REGISTROBe.IDREGISTRO != 0)
                        {
                        REGISTROBe.TXTMENSAJE = "Se Registro Correctamente el Acceso, verifique su correo.";
                        Fcorreo(REGISTROBe.TXTNOMRAZONSOCIAL, REGISTROBe.TXTCORREO, REGISTROBe.NRODOCIDE, REGISTROBe.IDREGISTRO.ToString());
                        }
                        else
                        {
                        REGISTROBe.TXTMENSAJE = "Error no se registro el Acceso, Vuelve a intentar.";
                        }

                    }

                    return Json(REGISTROBe, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }


        }


        [HttpPost]
        public ActionResult BuscarPersona(FormCollection form)
        {

            try
            {
                REGISTRO_Be REGISTROBe = new REGISTRO_Be();

                if (form.Count == 0)
                {
                    return View("Index");
                }
                else
                {

                    REGISTROBe.NRODOCIDE = form["TxtNroDoc"].ToString().Trim().ToUpper();
        

                    REGISTRO_Br REGISTROBr = new REGISTRO_Br();

                    REGISTROBe.IDREGISTRO = REGISTROBr.PS_REGISTRO(REGISTROBe);
                    //if (REGISTROBe.IDREGISTRO != 0)
                    //{
                        REGISTROBe.TXTMENSAJE = "El Usuario ya se Encuentra Registrado !!!";
                       
                    //}
                    //else
                    //{
                    //    REGISTROBe.TXTMENSAJE = "Error no se registro el Acceso, Vuelve a intentar.";
                    //}

                }

                return Json(REGISTROBe, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }


        }

        private void Fcorreo(string strNombre,string strCorreo,string strUsuario,string strClave)
        {
           
            WsCorreo.wsmail mySender = new WsCorreo.wsmail();

            string sendermail = "mesadepartes@munijesusmaria.gob.pe";

            string VHtml= @"
                        <html>
                        <head>
                        </head>
                        <body>
                        <table>
                        <tr>
                        <td >
                        <table bgcolor='#1D447E'>
                        <tr>
                        <td style='width: 550px; height: 34px; text-align: center;'>
                        <img src='https://www.munijesusmaria.gob.pe/wp-content/uploads/2019/03/logo-mdjm-alternativo.png'  width='290' height='71' />
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr style='height: 356px;'>
                        <td style='width: 597px; height: 356px;'>&nbsp;Registro exitoso
                        <h2>&iexcl;Te damos la bienvenida!</h2>
                        <h2>Hola,"+ strNombre + @"</h2>
                        <p>Detallamos&nbsp;Informaci&oacute;n de tu cuenta:</p>
                        <ul>
                        <li>Usuario:&nbsp;" + strUsuario + @"</li>
                        <li>Clave:&nbsp;" + strClave + @"</li>
                        </ul>
                        <p>&nbsp;</p>
                        <p>Si tienes alguna consulta referente  al usuario o clave escr&iacute;banos a&nbsp;<a href='mailto:mesadepartes@munijesusmaria.gob.pe' target='_blank' rel='noopener noreferrer' data-auth='NotApplicable'>mesadepartes@munijesusmaria.gob.pe</a>.<br /><br />Cordialmente,</p>
                        <p>Municipalidad distrital Jesus Mar&iacute;a.</p>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>";


            mySender.EnviaEmailFormatHTML(strCorreo, sendermail, "Acceso Personal - Ventanilla Virtual", VHtml);



        }









        [HttpGet]
        public JsonResult ListaDepartamento()
        {
            List<UBIGEO_Be> lsGenerado = null;
            DataSet ds;
            string lstRuta = Server.MapPath("~/Xml/GRAL_UBIGEO.xml");
            ds = new DataSet();
            ds.ReadXml(lstRuta);

            DataTable dt = ds.Tables[0];
            DataView dvDataFiltro = new DataView(dt);
            dvDataFiltro.RowFilter = "CODUBIGEO like '%0000'";
            dvDataFiltro.Sort = "TXTDEPARTAMENTO";

            DataTable dtResult;
            dtResult = new DataTable();
            dtResult = dvDataFiltro.ToTable();

            lsGenerado = dtResult.AsEnumerable().Select(x => new UBIGEO_Be
            {
                proCODUBIGEO = (string)(x["CODUBIGEO"]),
                proTXTDEPARTAMENTO = (string)(x["TXTDEPARTAMENTO"] ?? "")

            }).ToList();

            return Json(lsGenerado, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult ListaProvincia(string stDepartamento)
        {
            List<UBIGEO_Be> lsGenerado = null;
            DataSet ds;
            string lstRuta = Server.MapPath("~/Xml/GRAL_UBIGEO.xml");
            ds = new DataSet();
            ds.ReadXml(lstRuta);

            DataTable dt = ds.Tables[0];
            DataView dvDataFiltro = new DataView(dt);

            string lstProv = stDepartamento.Substring(0, 2);

            dvDataFiltro.RowFilter = "CODDEP = '" + lstProv + "' AND CODPRO<> '00' AND CODDIS = '00' "; //'" + stDepartamento.Substring(2) + "%00'";
            dvDataFiltro.Sort = "TXTPROVINCIA";

            DataTable dtResult;
            dtResult = new DataTable();
            dtResult = dvDataFiltro.ToTable();



            lsGenerado = dtResult.AsEnumerable().Select(x => new UBIGEO_Be
            {
                proCODUBIGEO = (string)(x["CODUBIGEO"]),
                proTXTPROVINCIA = (string)(x["TXTPROVINCIA"] ?? "")

            }).ToList();

            //lbeMILITANTE = GetListOfDiscoverableDomainControllers();
            //var lDepartamento = new SelectList(lsGenerado, "proCODUBIGEO", "proTXTPROVINCIA");
            // ViewData["VDProvincia"] = lDepartamento;
            //return lsGenerado;
            return Json(lsGenerado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ListaDistrito(string stDepartamento, string stProvincia)
        {
            List<UBIGEO_Be> lsGenerado = null;
            DataSet ds;
            string lstRuta = Server.MapPath("~/Xml/GRAL_UBIGEO.xml");
            ds = new DataSet();
            ds.ReadXml(lstRuta);

            DataTable dt = ds.Tables[0];
            DataView dvDataFiltro = new DataView(dt);

            string lstDep = stDepartamento.Substring(0, 2);
            string lstProv = stProvincia.Substring(2, 2);
            dvDataFiltro.RowFilter = "CODDEP = '" + lstDep + "' AND CODDIS <> '00' AND CODPRO='" + lstProv + "' "; //'" + stDepartamento.Substring(2) + "%00'";
            dvDataFiltro.Sort = "TXTDISTRITO";

            DataTable dtResult;
            dtResult = new DataTable();
            dtResult = dvDataFiltro.ToTable();

            lsGenerado = dtResult.AsEnumerable().Select(x => new UBIGEO_Be
            {
                proCODUBIGEO = (string)(x["CODUBIGEO"]),
                proTXTDISTRITO = (string)(x["TXTDISTRITO"] ?? "")

            }).ToList();

            return Json(lsGenerado, JsonRequestBehavior.AllowGet);
        }




    }
}



