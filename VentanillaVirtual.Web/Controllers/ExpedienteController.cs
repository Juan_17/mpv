﻿using System;
using System.Web;
using System.Web.Mvc;
using VentanillaVirtual.Web.Models;
using VentanillaVirtual.LogicaNegocio;
using VentanillaVirtual.Entidad;
using System.Data;
using System.Collections.Generic;
using Utils;
using System.Linq;

namespace VentanillaVirtual.Web.Controllers
{
    [CheckAuthorization]
    public class ExpedienteController : Controller
    {
        // GET: Expediente
        public ActionResult Index()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Expediente TUPA";

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be)Session["Slogin"];


            ViewData["VIdRegi"] = USUARIOBe.IDUSUARIO;
            ViewData["VTipDocIde"] = USUARIOBe.TIPDOCIDE;

            if (USUARIOBe.TIPDOCIDE == "DNI")
            {
                ViewData["VEtiNombre"] = "Razón Social";
            }
            else
            {
                ViewData["VEtiNombre"] = "Nombre y Apellidos";
            }

            ViewData["VNroDocIde"] = USUARIOBe.NRODOCIDE;
            ViewData["VDatos"] = USUARIOBe.RAZONSOCIAL;

            CargarOficina();

            return View();
        }

        private void CargarOficina()
        {

            List<OFICINA_Be> lsGenerado = null;
            OFICINA_Br OFICINALn = new OFICINA_Br();
            OFICINA_Be OFICINABe = new OFICINA_Be();
            OFICINABe.OFIC_P_inCODOFI = 0;
            OFICINABe.SEGU_btFLAELI = "2";

            lsGenerado = OFICINALn.Ps_LISTAOFICINA(OFICINABe);
            //VIASBe1.VIA_P_inID = 0;
            //VIASBe1.VIA_chDESC = "Seleccione ...";
            lsGenerado.Add(OFICINABe);
            ViewData["VDOfic"] = new SelectList(lsGenerado, "OFIC_P_inCODOFI", "OFIC_chDESOFI", 0);

        }


        [HttpPost]
        public ActionResult BuscarTUPA(string strNproc, string strAsunto, int strOficina)
        {


            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;


            List<TUPA_Be> list = new List<TUPA_Be>();

            list = fbd_BuscarTupa(strNproc, strAsunto, strOficina);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        private static List<TUPA_Be> fbd_BuscarTupa(string strNproc, string strAsunto, int strOficina)
        {

            DataTable dt = null;
            List<TUPA_Be> lsGenerado = null;
            try
            {
                TUPA_Br TUPABr= new TUPA_Br();

                TUPA_Be TUPABe = new TUPA_Be();
                TUPABe.ASUN_chTUPASUBP = strNproc;
                TUPABe.BUS_ASUN_chDESASU = strAsunto;
                TUPABe.OFIC_P_inCODOFI = strOficina;

                dt = TUPABr.Ps_LISTATUPA(TUPABe);

                lsGenerado = dt.AsEnumerable().Select(x => new TUPA_Be
                {
                    ASUN_P_inCODASU = (int)(x.IsNull("ASUN_P_inCODASU") ? 0 : x["ASUN_P_inCODASU"]),
                    ASUN_chTUPAPART = (string)(x.IsNull("ASUN_chTUPAPART") ? "" : x["ASUN_chTUPAPART"]),
                    BUS_ASUN_chDESASU = (string)(x.IsNull("BUS_ASUN_chDESASU") ? "" : x["BUS_ASUN_chDESASU"]),
                    ASUN_inTIEDIA = (int)(x.IsNull("ASUN_inTIEDIA") ? "" : x["ASUN_inTIEDIA"]),
                    //ASUN_reTUPADERPAG = (decimal)(x.IsNull("ASUN_reTUPADERPAG") ? 0 : x["ASUN_reTUPADERPAG"]),
                    CALI_chNOMCOR = (string)(x.IsNull("CALI_chNOMCOR") ? "" : x["CALI_chNOMCOR"]),
                    OFIC_P_inCODOFI = (int)(x.IsNull("OFIC_P_inCODOFI") ? 0 : x["OFIC_P_inCODOFI"]),
                    OFIC_chDESOFISEC = (string)(x.IsNull("OFIC_chDESOFISEC") ? "" : x["OFIC_chDESOFISEC"]),
                    ASUN_chTUPASUBP = (string)(x.IsNull("ASUN_chTUPASUBP") ? "" : x["ASUN_chTUPASUBP"])


                }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return lsGenerado;
        }


        [HttpPost]
        public ActionResult BuscarRequisitoTupa(int strCodigo)
        {


            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            //var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            //var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;


            List<TUPA_REQUISITO_Be> list = new List<TUPA_REQUISITO_Be>();

            list = fbd_BuscarRequisitoTupa(strCodigo);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        private static List<TUPA_REQUISITO_Be> fbd_BuscarRequisitoTupa(int strCodigo)
        {

            DataTable dt = null;
            List<TUPA_REQUISITO_Be> lsGenerado = null;
            try
            {
               
                TUPA_REQUISITO_Br TUPA_REQUISITOBr = new TUPA_REQUISITO_Br();

                TUPA_REQUISITO_Be TUPA_REQUISITOBe = new TUPA_REQUISITO_Be();

                TUPA_REQUISITOBe.ASUN_P_inCODASU = strCodigo;


                dt = TUPA_REQUISITOBr.PS_LISTATUPA_REQUISITO(TUPA_REQUISITOBe);

                lsGenerado = dt.AsEnumerable().Select(x => new TUPA_REQUISITO_Be
                {
                    ASUN_P_inCODASU = (int)(x.IsNull("ASUN_P_inCODASU") ? 0 : x["ASUN_P_inCODASU"]),
                    ASUN_chTUPAPART = (string)(x.IsNull("ASUN_chTUPAPART") ? "" : x["ASUN_chTUPAPART"]),
                    ASUN_chDESASU = (string)(x.IsNull("ASUN_chDESASU") ? "" : x["ASUN_chDESASU"]),
                    CALI_chNOMCOM = (string)(x.IsNull("CALI_chNOMCOM") ? "" : x["CALI_chNOMCOM"]),
                    ASUN_inTIEDIA = (int)(x.IsNull("ASUN_inTIEDIA") ? 0 : x["ASUN_inTIEDIA"]),
                    OFIC_chDESOFI = (string)(x.IsNull("OFIC_chDESOFI") ? "" : x["OFIC_chDESOFI"]),
                    ASUN_chTUPACONC = (string)(x.IsNull("ASUN_chTUPACONC") ? "" : x["ASUN_chTUPACONC"]),
                    DPLA_chDESC = (string)(x.IsNull("DPLA_chDESC") ? "" : x["DPLA_chDESC"]),
                    DPLA_chDESCOR = (string)(x.IsNull("DPLA_chDESCOR") ? "" : x["DPLA_chDESCOR"]),
                    //ASUN_reTUPADERPAG = (decimal)(x.IsNull("ASUN_reTUPADERPAG") ? 0 : x["ASUN_reTUPADERPAG"]),
                    REQ_chDES = (string)(x.IsNull("REQ_chDES") ? "" : x["REQ_chDES"]),
                    // REQ_inDERPAG = (decimal)(x.IsNull("REQ_inDERPAG") ? 0 : x["REQ_inDERPAG"]),


                }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return lsGenerado;
        }

        [HttpPost]
        public ActionResult RegistrarExpediente(string StrSolicitud, string StrObs, string StrFolio, string StrHCodTupa, string StrHCodRegi)
        {

            try
            {

                RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();

                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();
                SOLICITUDBe.OPC =1;
                SOLICITUDBe.IDSOLICITUD = 0; //(StrCodDocu != "") ? Convert.ToInt32(StrCodDocu.ToString().Trim()) : 0;
                SOLICITUDBe.IDREGISTRO = (StrHCodRegi != "") ? Convert.ToInt32(StrHCodRegi.ToString().Trim()) : 0;


                SOLICITUDBe.TXTASUNTO = StrSolicitud.ToString().Trim().ToUpper();
                SOLICITUDBe.TXTOBSERVACION = StrObs.ToString().Trim();
                SOLICITUDBe.TXTFOLIOS = StrFolio.ToString().Trim().ToUpper();
                SOLICITUDBe.IDTUPA = (StrHCodTupa != "") ? Convert.ToInt32(StrHCodTupa.ToString().Trim()) : 0;

                SOLICITUDBe.IDTIPOSOLICITUD = 1;
                // SOLICITUDBe.TIPO_P_inCODTIP = 1;
                //SOLICITUDBe.ESTADO = "1";

                RESPUESTABe.proValor = new SOLICITUD_Br().Pid_SOLICITUD(SOLICITUDBe);

                if (RESPUESTABe.proValor != "0")
                {
                    RESPUESTABe.proMensaje = "Se Registro Correctamente el Pre-Expediente.";

                }
                else
                {
                    RESPUESTABe.proMensaje = "Error no se registro el Pre-Expediente, Vuelve a intentar.";
                }

                return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            }
            // }
            catch (Exception ex)
            {
                throw;
            }


        }



        [HttpPost]
        public ActionResult BuscarSExpediente(string strTipo, string strIdRegistro, string strOrigen)
        {
            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();

            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;

            //int strOFIC_P_inCODOFI = Convert.ToInt32(Session["SIdOficina"].ToString());

            List<SOLICITUD_Be> list = new List<SOLICITUD_Be>();

            list = new SOLICITUD_Br().Ps_SEXPEDIENTE(strTipo, strIdRegistro, strTipoBusq, strValorBusq, strOrigen, Session["SIdOficina"].ToString());

            //list = fbd_BuscarDocSimple(strTipo, strTipoBusq, strValorBusq);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }



        // PANEL USUARIO MUNI

        public ActionResult RecepcionSEx()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja de Usuario";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Expediente";
            ViewBag.OpHijo03 = Session["SNombOficina"];

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be)Session["Slogin"];

            return View();

        }

        // FIN PANEL USUARIO MUNI



        // PANEL CONTRIBUYENTE

        public ActionResult RecepcionSExC()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja de Usuario";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Expediente";
            ViewBag.OpHijo03 = Session["SNombOficina"];

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be)Session["Slogin"];

            ViewData["iIdRegi"] = USUARIOBe.IDUSUARIO;

            return View();

        }

        // FIN CONTRIBUYENTE





    }
}