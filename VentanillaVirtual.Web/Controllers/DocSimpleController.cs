﻿using System;
using System.Web;
using System.Web.Mvc;
using VentanillaVirtual.Web.Models;
using VentanillaVirtual.LogicaNegocio;
using VentanillaVirtual.Entidad;
using System.Linq;
using System.Collections.Generic;

namespace VentanillaVirtual.Web.Controllers
{
    [CheckAuthorization]
    public class DocSimpleController : Controller
    {
        // GET: DocSimple
        public ActionResult Index()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Documento Simple";

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be) Session["Slogin"];


            ViewData["VIdRegi"] = USUARIOBe.IDUSUARIO;
            ViewData["VTipDocIde"] = USUARIOBe.TIPDOCIDE;

            if(USUARIOBe.TIPDOCIDE== "DNI")
            {
                ViewData["VEtiNombre"] = "Razón Social";
            }
            else
            {
                ViewData["VEtiNombre"] = "Nombre y Apellidos";
            }


            ViewData["VNroDocIde"] = USUARIOBe.NRODOCIDE;
            ViewData["VDatos"] = USUARIOBe.RAZONSOCIAL;

            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();

            return View();

        }

        [HttpPost]
        public ActionResult RegistraDocSimple(string StrSolicitud, string StrObs, string StrFolio, string StrHCodRegi)
        {

            try
            {
                //OPC=1 REGISTRAR //OPC=2 EDITAR //OPC=3 ELIMINAR

                RESPUESTA_Be RESPUESTABe = new RESPUESTA_Be();

                SOLICITUD_Be SOLICITUDBe = new SOLICITUD_Be();
                SOLICITUDBe.OPC = 1;
                //SOLICITUDBe.IDSOLICITUD = (StrCodDocu != "") ? Convert.ToInt32(StrCodDocu.ToString().Trim()) : 0;
                SOLICITUDBe.IDREGISTRO = (StrHCodRegi != "") ? Convert.ToInt32(StrHCodRegi.ToString().Trim()) : 0;
                SOLICITUDBe.TXTASUNTO = StrSolicitud.ToString().Trim().ToUpper();
                SOLICITUDBe.TXTOBSERVACION = StrObs.ToString().Trim();
                SOLICITUDBe.TXTFOLIOS = StrFolio.ToString().Trim().ToUpper();
                SOLICITUDBe.IDTIPOSOLICITUD = 2;
                //SOLICITUDBe.TIPO_P_inCODTIP = (StrHCodTipdoc != "") ? Convert.ToInt32(StrHCodTipdoc.ToString().Trim()) : 0;
                SOLICITUDBe.IDTUPA = 1326;
                //SOLICITUDBe.ESTADO = "1";
                RESPUESTABe.proValor = new SOLICITUD_Br().Pid_SOLICITUD(SOLICITUDBe);

                if (RESPUESTABe.proValor != "0")
                {
                    RESPUESTABe.proMensaje = "Se Registro Correctamente el Documento Simple.";
                }
                else
                {
                    RESPUESTABe.proMensaje = "Error no se registro el Documento Simple, Vuelve a intentar.";
                }

                return Json(RESPUESTABe, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw;
            }


        }










///BUSCAR DOC SIMPLE

        [HttpPost]
        public ActionResult BuscarDocSimple(string strTipo, string strIdRegistro, string strOrigen)
        {
            ViewBag.vbFECHA = System.DateTime.Today.ToShortDateString();

            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            var strTipoBusq = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var strValorBusq = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;

            //int strOFIC_P_inCODOFI = Convert.ToInt32(Session["SIdOficina"].ToString());

            List<SOLICITUD_Be> list = new List<SOLICITUD_Be>();

            list = new SOLICITUD_Br().Ps_DOCUMENTOSIMPLE(strTipo, strIdRegistro, strTipoBusq, strValorBusq, strOrigen, Session["SIdOficina"].ToString());

            //list = fbd_BuscarDocSimple(strTipo, strTipoBusq, strValorBusq);

            recordsTotal = list.Count();
            var data = list.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }
//FIN

        // PANEL USUARIO MUNI

        public ActionResult RecepcionDS()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja de Usuario";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Documento Simple";
            ViewBag.OpHijo03 = Session["SNombOficina"];

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be)Session["Slogin"];

            return View();

        }

        // FIN PANEL USUARIO MUNI


        // PANEL CONTRIBUYENTE

        public ActionResult RecepcionDSC()
        {

            ////indicador de Menu
            ViewBag.OpPadre = "Bandeja de Usuario";
            ViewBag.OpHijo01 = "Solicitud";
            ViewBag.OpHijo02 = "Documento Simple";
            ViewBag.OpHijo03 = Session["SNombOficina"];

            USUARIO_Be USUARIOBe = new USUARIO_Be();
            USUARIOBe = (USUARIO_Be)Session["Slogin"];

            ViewData["iIdRegi"] = USUARIOBe.IDUSUARIO;

            return View();

        }

        // FIN CONTRIBUYENTE




    }
}